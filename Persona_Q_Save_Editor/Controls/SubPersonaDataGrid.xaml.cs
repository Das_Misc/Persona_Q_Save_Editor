﻿using System.Windows;
using System.Windows.Controls;

namespace Persona_Q_Save_Editor.Controls
{
    /// <summary>
    /// Interaction logic for SubPersonaDataGrid.xaml
    /// </summary>
    public partial class SubPersonaDataGrid : UserControl
    {
        public SubPersonaDataGrid()
        {
            InitializeComponent();
        }

        private void DataGrid_Selected(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource.GetType() == typeof(DataGridCell))
                (sender as DataGrid).BeginEdit(e);
        }
    }
}
