﻿using System.Windows;
using System.Windows.Controls;

namespace Persona_Q_Save_Editor.Controls
{
    /// <summary>
    /// Interaction logic for InventoryDataGrid.xaml
    /// </summary>
    public partial class InventoryDataGrid : UserControl
    {
        public InventoryDataGrid()
        {
            InitializeComponent();
        }

        private void DataGrid_Selected(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource.GetType() == typeof(DataGridCell))
                (sender as DataGrid).BeginEdit(e);
        }
    }
}
