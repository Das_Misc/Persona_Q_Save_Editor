﻿using MahApps.Metro.Controls;
using Persona_Q_Save_Editor.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;

namespace Persona_Q_Save_Editor.Views
{
    /// <summary>
    /// Interaction logic for SubPersonaWindow.xaml
    /// </summary>
    public partial class SubPersonaWindow : MetroWindow
    {
        public SubPersonaWindow(MainWindow mw)
        {
            DataContext = mw.DataContext;
            (DataContext as SaveInfo).PersonaList = new ObservableCollection<Persona>();
            (DataContext as SaveInfo).PersonaList.Add((DataContext as SaveInfo).Character.SubPersona);

            InitializeComponent();
        }

        private void Arcana_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(e.OriginalSource is TabControl))
                return;

            KeyValuePair<int, Tuple<string, string>>[] personaData = null;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    personaData = (from x in Lists.PersonasChariot
                                   select x).ToArray();
                    break;

                case 1:
                    personaData = (from x in Lists.PersonasDeath
                                   select x).ToArray();
                    break;

                case 2:
                    personaData = (from x in Lists.PersonasDevil
                                   select x).ToArray();
                    break;

                case 3:
                    personaData = (from x in Lists.PersonasEmperor
                                   select x).ToArray();
                    break;

                case 4:
                    personaData = (from x in Lists.PersonasEmpress
                                   select x).ToArray();
                    break;

                case 5:
                    personaData = (from x in Lists.PersonasFool
                                   select x).ToArray();
                    break;

                case 6:
                    personaData = (from x in Lists.PersonasFortune
                                   select x).ToArray();
                    break;

                case 7:
                    personaData = (from x in Lists.PersonasHangedMan
                                   select x).ToArray();
                    break;

                case 8:
                    personaData = (from x in Lists.PersonasHermit
                                   select x).ToArray();
                    break;

                case 9:
                    personaData = (from x in Lists.PersonasHierophant
                                   select x).ToArray();
                    break;

                case 10:
                    personaData = (from x in Lists.PersonasJudgement
                                   select x).ToArray();
                    break;

                case 11:
                    personaData = (from x in Lists.PersonasJustice
                                   select x).ToArray();
                    break;

                case 12:
                    personaData = (from x in Lists.PersonasLovers
                                   select x).ToArray();
                    break;

                case 13:
                    personaData = (from x in Lists.PersonasMagician
                                   select x).ToArray();
                    break;

                case 14:
                    personaData = (from x in Lists.PersonasMoon
                                   select x).ToArray();
                    break;

                case 15:
                    personaData = (from x in Lists.PersonasPriestess
                                   select x).ToArray();
                    break;

                case 16:
                    personaData = (from x in Lists.PersonasStar
                                   select x).ToArray();
                    break;

                case 17:
                    personaData = (from x in Lists.PersonasStrength
                                   select x).ToArray();
                    break;

                case 18:
                    personaData = (from x in Lists.PersonasSun
                                   select x).ToArray();
                    break;

                case 19:
                    personaData = (from x in Lists.PersonasTemperance
                                   select x).ToArray();
                    break;

                case 20:
                    personaData = (from x in Lists.PersonasTower
                                   select x).ToArray();
                    break;

                case 21:
                    personaData = (from x in Lists.PersonasWorld
                                   select x).ToArray();
                    break;

                default:
                    return;
            }

            UpdatePersonaList(personaData);

            e.Handled = true;
        }

        private void Skills_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KeyValuePair<int, Tuple<string, string>>[] skillsData = null;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    skillsData = (from x in Lists.SkillsAlmighty
                                  select x).ToArray();
                    break;

                case 1:
                    skillsData = (from x in Lists.SkillsBash
                                  select x).ToArray();
                    break;

                case 2:
                    skillsData = (from x in Lists.SkillsBuff
                                  select x).ToArray();
                    break;

                case 3:
                    skillsData = (from x in Lists.SkillsCut
                                  select x).ToArray();
                    break;

                case 4:
                    skillsData = (from x in Lists.SkillsDebuff
                                  select x).ToArray();
                    break;

                case 5:
                    skillsData = (from x in Lists.SkillsMagic
                                  select x).ToArray();
                    break;

                case 6:
                    skillsData = (from x in Lists.SkillsNavi
                                  select x).ToArray();
                    break;

                case 7:
                    skillsData = (from x in Lists.SkillsRecovery
                                  select x).ToArray();
                    break;

                case 8:
                    skillsData = (from x in Lists.SkillsStab
                                  select x).ToArray();
                    break;
            }

            UpdateSkillList(skillsData);

            e.Handled = true;
        }

        private void UpdatePersonaList(KeyValuePair<int, Tuple<string, string>>[] personaData)
        {
            var personas = new Persona[personaData.Length - 1];

            for (int i = 1; i <= personas.Length; i++) // Drop the EMPTY entry
            {
                personas[i - 1] = new Persona();
                personas[i - 1].Id = (short)personaData[i].Key;
            }

            Persona.List = personas.OrderBy(x => x.Name).ToArray(); // Alphabetical order

            for (int i = (DataContext as SaveInfo).PersonaList.Count - 1; i > 0; i--)
                (DataContext as SaveInfo).PersonaList.RemoveAt(i);

            foreach (Persona item in Persona.List)
                (DataContext as SaveInfo).PersonaList.Add(item);
        }

        private void UpdateSkillList(KeyValuePair<int, Tuple<string, string>>[] skillsData)
        {
            var skills = new Skill[skillsData.Length];

            for (int i = 0; i < skills.Length; i++)
            {
                skills[i] = new Skill();
                skills[i].ID = (short)skillsData[i].Key;
            }

            Skill.List = skills.OrderBy(x => x.ID != 0) // Set EMPTY as the first entry
                               .ThenBy(x => x.Name).ToArray(); // Then sort the others
        }
    }
}
