﻿using MahApps.Metro.Controls.Dialogs;
using Microsoft.Win32;
using Persona_Q_Save_Editor.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Persona_Q_Save_Editor.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        internal SaveInfo si;

        public MainWindow()
        {
            si = new SaveInfo();

            DataContext = si;

            InitializeComponent();

            //DebugLoad();
            //QuickFix();
        }

        [Conditional("DEBUG")]
        private void DebugLoad()
        {
            string saveFile = @"C:\Users\Dasanko\AppData\Roaming\Citra\sdmc\Nintendo 3DS\00000000000000000000000000000000\00000000000000000000000000000000\title\00040000\00149f00\data\00000001\001GAME";

            si.SaveFile = saveFile;

            si.SaveData = File.ReadAllBytes(saveFile);

            si.KeyItems = LoadKeyItemsData();

            si.SummonedPersonas = LoadSummonedPersonasData();

            si.Characters = LoadCharacterData();

            si.Inventory = LoadInventoryData();

            LoadGeneralData();

            si.Character = si.MainCharIsP4 ? si.Characters[0] : si.Characters[1];

            si.SaveLoaded = true;
        }

        [Conditional("DEBUG")]
        private void DebugSave()
        {
            SaveGeneralData();

            SaveKeyItemsData();

            SaveSummonedPersonasData();

            SaveCharacterData();

            SaveInventoryData();

            FixChecksum(si.SaveData, 0x10, 0x4C);

            FixChecksum(si.SaveData, 0xC, 0x4C);

            FixChecksum(si.SaveData, 0x54, si.SaveData.Length);

            File.WriteAllBytes(si.SaveFile, si.SaveData);
        }

        [Conditional("DEBUG")]
        private void QuickFix()
        {
            DebugLoad();

            DebugSave();

            Application.Current.Shutdown();
        }

        #region CanExecute

        private void CheckUpdates_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = File.Exists("Updater.exe");
        }

        private void Save_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = !string.IsNullOrEmpty(si.SaveFile);
        }

        #endregion CanExecute

        #region Executed

        private void About_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly();

            string message = string.Format(Properties.Resources.AboutMessage, Properties.Resources.Title, thisAssembly.GetName().Version, FileVersionInfo.GetVersionInfo(Assembly.GetEntryAssembly().Location).CompanyName);

            DialogManager.ShowModalMessageExternal(this, Properties.Resources.Title, message, MessageDialogStyle.Affirmative);
        }

        private void BestEquipment_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            int[] weapIds = { 0x19, 0x1E7, 0x56, 0x96, 0xC6, 0x2FE, 0xF6, 0x126, 0x156, 0x216, 0x1A, 0x2A6, 0x246, 0x2FD, 0x186, 0x276, 0x57, 0x2D6, 0x1B7, 0x1B7 };
            int[] armourIds = { 0x339, 0x339, 0x339, 0x35E, 0x35E, 0x36E, 0x339, 0x35E, 0x339, 0x35E, 0x339, 0x339, 0x35E, 0x36D, 0x388, 0x339, 0x3B5, 0x339, 0x3E8, 0x3E8 };

            for (int i = 0; i < si.Characters.Length; i++)
            {
                var data = Lists.GetItemData(weapIds[i]);
                si.Characters[i].EquippedWeapon = new Item(weapIds[i], data.Item1, data.Item2);

                data = Lists.GetItemData(armourIds[i]);
                si.Characters[i].EquippedArmour = new Item(armourIds[i], data.Item1, data.Item2);
            }

            DialogManager.ShowModalMessageExternal(this, Properties.Resources.Title, Properties.Resources.BestEquipmentMessage, MessageDialogStyle.Affirmative);
        }

        private void ChangeName_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var mds = new MetroDialogSettings();
            mds.DefaultText = si.Character.BaseName;
            var newName = DialogManager.ShowModalInputExternal(this, Properties.Resources.ChangeName, Properties.Resources.ChangeNameDescription, mds);

            var mds2 = new MetroDialogSettings();
            mds2.DefaultText = si.Character.LastName;
            var newLastName = DialogManager.ShowModalInputExternal(this, Properties.Resources.ChangeLastName, Properties.Resources.ChangeLastNameDescription, mds2);

            if (string.IsNullOrWhiteSpace(newName) || string.IsNullOrWhiteSpace(newLastName))
                return;

            if (Regex.IsMatch(newName, @"^\w.{0,5}$") && Regex.IsMatch(newLastName, @"^\w.{0,5}$"))
            {
                Lists.Characters[si.Character.Id] = newName + " " + newLastName;
                si.Character.NotifyPropertyChanged("Name");
                si.Character.BaseName = newName;
                si.Character.LastName = newLastName;
            }
            else
                this.ShowMessageAsync("Invalid name", "The character name " + newName + " " + newLastName + " is invalid and will not be used.");
        }

        private void CheckUpdates_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var thisAssembly = Assembly.GetExecutingAssembly().GetName();
            Process.Start("Updater.exe", string.Format("\"{0}\" \"{1}\" \"{2}\"", thisAssembly.Name, thisAssembly.Version, Assembly.GetEntryAssembly().Location));
        }

        private void Donate_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string reference = "PQSE";

            Process.Start("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=serenitydiary%40gmail%2ecom&lc=ES&item_name=Donate%20to%20Dasanko&item_number=" + reference + "&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted");
        }

        private void FillEnepedia_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            si.Enepedia = Lists.Enepedia;

            DialogManager.ShowModalMessageExternal(this, Properties.Resources.Title, Properties.Resources.FillEnepediaMessage, MessageDialogStyle.Affirmative);
        }

        private void GenerateMap_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            using (var s = Assembly.GetExecutingAssembly().GetManifestResourceStream("Persona_Q_Save_Editor.Resources.001MAP"))
            using (var fs = File.Open("001MAP", FileMode.Create, FileAccess.Write, FileShare.None))
            {
                int readBytes = 0;
                byte[] buffer = new byte[4096];

                while ((readBytes = s.Read(buffer, 0, buffer.Length)) > 0)
                    fs.Write(buffer, 0, readBytes);
            }

            DialogManager.ShowModalMessageExternal(this, Properties.Resources.Title, Properties.Resources.GenerateMapMessage, MessageDialogStyle.Affirmative);
        }

        private void Load_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.FileName = "*GAME";
            ofd.Title = Properties.Resources.Title;

            if (ofd.ShowDialog().Value)
            {
                si.SaveFile = ofd.FileName;

                si.SaveData = File.ReadAllBytes(ofd.FileName);

                si.KeyItems = LoadKeyItemsData();

                si.SummonedPersonas = LoadSummonedPersonasData();

                si.Characters = LoadCharacterData();

                si.Inventory = LoadInventoryData();

                LoadGeneralData();

                si.Character = si.MainCharIsP4 ? si.Characters[0] : si.Characters[1];

                si.SaveLoaded = true;
            }
        }

        private void Save_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            SaveGeneralData();

            SaveKeyItemsData();

            SaveSummonedPersonasData();

            SaveCharacterData();

            SaveInventoryData();

            FixChecksum(si.SaveData, 0x10, 0x4C);

            FixChecksum(si.SaveData, 0xC, 0x4C);

            FixChecksum(si.SaveData, 0x54, si.SaveData.Length);

            File.WriteAllBytes(si.SaveFile, si.SaveData);
        }

        #endregion Executed

        #region Events

        internal void UpdateItemList(KeyValuePair<int, Tuple<string, string>>[] itemData)
        {
            var items = new Item[itemData.Length];

            for (int i = 0; i < items.Length; i++)
                items[i] = new Item(itemData[i].Key, itemData[i].Value.Item1, itemData[i].Value.Item2);

            Item.List = items.OrderBy(x => x.ID != 0) // Set EMPTY as the first entry
                             .ThenBy(x => x.Name).ToArray(); // Then sort the others
        }

        private void Armours_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KeyValuePair<int, Tuple<string, string>>[] itemData = null;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    itemData = (from x in Lists.ArmourAigis
                                select x).ToArray();
                    break;

                case 1:
                    itemData = (from x in Lists.ArmourFemales
                                select x).ToArray();
                    break;

                case 2:
                    itemData = (from x in Lists.ArmourKoromaru
                                select x).ToArray();
                    break;

                case 3:
                    itemData = (from x in Lists.ArmourMales
                                select x).ToArray();
                    break;

                case 4:
                    itemData = (from x in Lists.ArmourUnisex
                                select x).ToArray();
                    break;

                case 5:
                    itemData = (from x in Lists.ArmourZenRei
                                select x).ToArray();
                    break;
            }

            UpdateItemList(itemData);

            e.Handled = true;
        }

        private void DataGrid_Selected(object sender, RoutedEventArgs e)
        {
            if (e.OriginalSource.GetType() == typeof(DataGridCell))
                (sender as DataGrid).BeginEdit(e);
        }

        private void EditSubPersona_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var spw = new SubPersonaWindow(this);
            spw.Owner = this;
            spw.ShowDialog();
        }

        private void Inventory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(e.OriginalSource is TabControl))
                return;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    UpdateItemList((from x in Lists.Consumables
                                    select x).ToArray());
                    break;

                case 2:
                    UpdateItemList((from x in Lists.Accessories
                                    select x).ToArray());
                    break;

                default:
                    return;
            }
        }

        private void Materials_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KeyValuePair<int, Tuple<string, string>>[] itemData = null;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    itemData = (from x in Lists.MaterialsDropped
                                select x).ToArray();
                    break;

                case 1:
                    itemData = (from x in Lists.MaterialsPowerSpot
                                select x).ToArray();
                    break;

                case 2:
                    itemData = (from x in Lists.MaterialsSacrifice
                                select x).ToArray();
                    break;
            }

            UpdateItemList(itemData);

            e.Handled = true;
        }

        private void SkillCards_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KeyValuePair<int, Tuple<string, string>>[] itemData = null;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    itemData = (from x in Lists.SkillCardsAlmighty
                                select x).ToArray();
                    break;

                case 1:
                    itemData = (from x in Lists.SkillCardsBash
                                select x).ToArray();
                    break;

                case 2:
                    itemData = (from x in Lists.SkillCardsBuff
                                select x).ToArray();
                    break;

                case 3:
                    itemData = (from x in Lists.SkillCardsCut
                                select x).ToArray();
                    break;

                case 4:
                    itemData = (from x in Lists.SkillCardsDebuff
                                select x).ToArray();
                    break;

                case 5:
                    itemData = (from x in Lists.SkillCardsMagic
                                select x).ToArray();
                    break;

                case 6:
                    itemData = (from x in Lists.SkillCardsNavi
                                select x).ToArray();
                    break;

                case 7:
                    itemData = (from x in Lists.SkillCardsRecovery
                                select x).ToArray();
                    break;

                case 8:
                    itemData = (from x in Lists.SkillCardsStab
                                select x).ToArray();
                    break;
            }

            UpdateItemList(itemData);

            e.Handled = true;
        }

        private void Weapons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            KeyValuePair<int, Tuple<string, string>>[] itemData = null;

            switch ((sender as TabControl).SelectedIndex)
            {
                case 0:
                    itemData = (from x in Lists.WeaponsAigis
                                select x).ToArray();
                    break;

                case 1:
                    itemData = (from x in Lists.WeaponsAkihiko
                                select x).ToArray();
                    break;

                case 2:
                    itemData = (from x in Lists.WeaponsChie
                                select x).ToArray();
                    break;

                case 3:
                    itemData = (from x in Lists.WeaponsKanji
                                select x).ToArray();
                    break;

                case 4:
                    itemData = (from x in Lists.WeaponsKen
                                select x).ToArray();
                    break;

                case 5:
                    itemData = (from x in Lists.WeaponsMitsuru
                                select x).ToArray();
                    break;

                case 6:
                    itemData = (from x in Lists.WeaponsNaoto
                                select x).ToArray();
                    break;

                case 7:
                    itemData = (from x in Lists.WeaponsP3
                                select x).ToArray();
                    break;

                case 8:
                    itemData = (from x in Lists.WeaponsP4Junpei
                                select x).ToArray();
                    break;

                case 9:
                    itemData = (from x in Lists.WeaponsShinjiro
                                select x).ToArray();
                    break;

                case 10:
                    itemData = (from x in Lists.WeaponsTeddie
                                select x).ToArray();
                    break;

                case 11:
                    itemData = (from x in Lists.WeaponsYosukeKoromaru
                                select x).ToArray();
                    break;

                case 12:
                    itemData = (from x in Lists.WeaponsYukari
                                select x).ToArray();
                    break;

                case 13:
                    itemData = (from x in Lists.WeaponsYukiko
                                select x).ToArray();
                    break;

                case 14:
                    itemData = (from x in Lists.WeaponsZenRei
                                select x).ToArray();
                    break;
            }

            UpdateItemList(itemData);

            e.Handled = true;
        }

        #endregion Events

        #region SaveLoading

        private void BitShiftLoad(BinaryReader br, dynamic stringsArray, dynamic objectsArray, int index, int indexEnd)
        {
            byte @byte = 0;

            for (int offset = 0; index < indexEnd; index++)
            {
                if (offset == 0)
                    @byte = br.ReadByte();

                byte bit = (byte)((@byte >> offset++) & 1);

                objectsArray[index] = new KeyItem(bit, stringsArray[index].Item1, stringsArray[index].Item2, si.MainCharIsP4);

                offset %= 8;
            }
        }

        private string DecryptName(long position)
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = position;

                StringBuilder sb = new StringBuilder();
                int pos = 0;
                byte b;
                while ((b = br.ReadByte()) != 0)
                {
                    if (pos % 2 != 0)
                    {
                        if (b == 0x57)
                            b -= 0x37;
                        else if (b <= 0x45)
                            b -= 0x16;
                        else if (b <= 0x7A)
                            b -= 0x1F;
                        else
                            b -= 0x20;

                        sb.Append(Convert.ToChar(b));
                    }

                    pos++;
                }

                return sb.ToString();
            }
        }

        private Character[] LoadCharacterData()
        {
            var characters = new List<Character>(Lists.Characters.Length);

            using (var ms = new MemoryStream(si.SaveData))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x8A3F;

                for (int i = 0; i < Lists.Characters.Length; i++)
                {
                    int weaponID = br.ReadInt32();
                    var equippedWeapon = Lists.GetItemData(weaponID);

                    ms.Position += 0xA;

                    int armourID = br.ReadInt32();
                    var equippedArmour = Lists.GetItemData(armourID);

                    ms.Position += 0xA;

                    int accessoryID = br.ReadInt32();
                    var equippedAccessory = Lists.GetItemData(accessoryID);

                    ms.Position += 0x1A;

                    var character = new Character((short)i);

                    if (i == 0)
                    {
                        long originalPosition = ms.Position;
                        string lastNameP4 = DecryptName(0x84FF);
                        string nameP4 = DecryptName(0x850D);
                        Lists.Characters[i] = nameP4 + " " + lastNameP4;
                        ms.Position = originalPosition;
                        character.BaseName = nameP4;
                        character.LastName = lastNameP4;
                    }

                    if (i == 1)
                    {
                        long originalPosition = ms.Position;
                        string lastNameP3 = DecryptName(0x84E3);
                        string nameP3 = DecryptName(0x84F1);
                        Lists.Characters[i] = nameP3 + " " + lastNameP3;
                        ms.Position = originalPosition;
                        character.BaseName = nameP3;
                        character.LastName = lastNameP3;
                    }

                    if (equippedWeapon != null)
                        character.EquippedWeapon = new Item(weaponID, equippedWeapon.Item1, equippedWeapon.Item2);

                    if (equippedArmour != null)
                        character.EquippedArmour = new Item(armourID, equippedArmour.Item1, equippedArmour.Item2);

                    if (equippedAccessory != null)
                        character.EquippedAccessory = new Item(accessoryID, equippedAccessory.Item1, equippedAccessory.Item2);

                    var persona = new Persona();
                    persona.Id = br.ReadInt16();
                    character.Persona = persona;

                    character.Level = br.ReadInt32();
                    character.Exp = br.ReadInt32();

                    ms.Position += 0x10;
                    character.MaxHP = br.ReadInt16();
                    character.MaxSP = br.ReadInt16();

                    character.St = br.ReadInt16();
                    character.Ma = br.ReadInt16();
                    character.En = br.ReadInt16();
                    character.Ag = br.ReadInt16();
                    character.Lu = br.ReadInt16();

                    ms.Position += 0x1E;
                    var subPersonaIndex = br.ReadByte();
                    if (subPersonaIndex != byte.MaxValue)
                        character.SubPersona = si.SummonedPersonas[subPersonaIndex];

                    ms.Position += 3;
                    character.CurrentHP = br.ReadInt32();
                    character.CurrentSP = br.ReadInt32();

                    characters.Add(character);

                    ms.Position += 0x10;
                }
            }

            return characters.ToArray();
        }

        private void LoadGeneralData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x34;
                si.ClearStatus = br.ReadByte();

                ms.Position = 0x809B;
                si.Enepedia = br.ReadBytes(Lists.Enepedia.Length);

                ms.Position = 0x84DB;
                si.Yen = br.ReadInt32();
                si.PartyGauge = br.ReadInt16();
            }
        }

        private Item[] LoadInventoryData()
        {
            var inventory = new Item[60]; // Inventory size in-game

            using (var ms = new MemoryStream(si.SaveData))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x966F;

                for (int i = 0; i < inventory.Length; i++)
                {
                    int id = br.ReadInt32();

                    var itemData = Lists.GetItemData(id);

                    inventory[i] = itemData != null ? new Item(id, itemData.Item1, itemData.Item2) : new Item(id, Properties.Resources.Unknown, string.Empty);

                    ms.Position += 0xA;
                }
            }

            return inventory;
        }

        private KeyItem[] LoadKeyItemsData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var br = new BinaryReader(ms))
            {
                KeyItem[] keyItems = new KeyItem[Lists.KeyItems.Length];
                ms.Position = 0x9C;
                int index = 0;
                int indexEnd = index + 8 * 4;
                BitShiftLoad(br, Lists.KeyItems, keyItems, index, indexEnd);

                ms.Position = 0xA4;
                index = indexEnd;
                indexEnd = index + 8;
                BitShiftLoad(br, Lists.KeyItems, keyItems, index, indexEnd);

                ms.Position = 0xAE;
                index = indexEnd;
                indexEnd = index + 8 * 2;
                BitShiftLoad(br, Lists.KeyItems, keyItems, index, indexEnd);

                ms.Position = 0xB2;
                index = indexEnd;
                indexEnd = index + 8 * 5;
                BitShiftLoad(br, Lists.KeyItems, keyItems, index, indexEnd);

                return keyItems;
            }
        }

        private Persona[] LoadSummonedPersonasData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var br = new BinaryReader(ms))
            {
                ms.Position = 0x851B;

                var personas = new Persona[0xE];

                for (int i = 0; i < personas.Length; i++)
                {
                    personas[i] = new Persona();

                    personas[i].Active = br.ReadInt16();
                    personas[i].Id = br.ReadInt16();
                    personas[i].Level = br.ReadInt16();

                    ms.Position += 2;
                    personas[i].Exp = br.ReadInt32();

                    personas[i].Skills = new Skill[6];
                    for (int x = 0; x < personas[i].Skills.Length; x++)
                    {
                        personas[i].Skills[x] = new Skill();
                        personas[i].Skills[x].ID = br.ReadInt16();
                    }

                    ms.Position += 4;
                    personas[i].BonusHP = br.ReadInt16();
                    personas[i].BonusSP = br.ReadInt16();

                    ms.Position += 0x28;
                }

                return personas;
            }
        }

        #endregion SaveLoading

        #region SaveWriting

        private void BitShiftSave(BinaryWriter bw, dynamic objectList, int index, int indexEnd)
        {
            byte @byte = 0;

            for (int offset = 0; index < indexEnd; index++)
            {
                @byte |= (byte)(objectList[index].Bit << offset++);

                offset %= 8;

                if (offset == 0)
                {
                    bw.Write(@byte);
                    @byte = 0;
                }
            }
        }

        private byte[] EncryptName(string name)
        {
            byte[] encryptedName = new byte[name.Length * 2];

            for (int i = 0; i < encryptedName.Length; i++)
            {
                if (i % 2 == 0)
                    encryptedName[i] = 0x82;
                else
                {
                    byte b = (byte)name[(int)Math.Floor(i / 2d)];

                    if (b == 0x20)
                    {
                        encryptedName[i - 1] = 0x81;
                        b += 0x37;
                    }
                    else if (b <= 0x35)
                    {
                        encryptedName[i - 1] = 0x81;
                        b += 0x16;
                    }
                    else if (b <= 0x5A)
                        b += 0x1F;
                    else
                        b += 0x20;

                    encryptedName[i] = b;
                }
            }

            return encryptedName;
        }

        private void FixChecksum(byte[] contents, int position, long finalPosition)
        {
            uint checksum = 0;

            for (int i = position; i < finalPosition; i++)
                checksum += contents[i];

            byte[] cs = BitConverter.GetBytes(checksum);
            Buffer.BlockCopy(cs, 0, contents, position - sizeof(uint), cs.Length);
        }

        private void SaveCharacterData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var bw = new BinaryWriter(ms))
            {
                ms.Position = 0x8A3F;
                for (int i = 0; i < si.Characters.Length; i++)
                {
                    var character = si.Characters[i];

                    if (character.EquippedWeapon != null)
                        bw.Write(character.EquippedWeapon.ID);
                    else
                        ms.Position += sizeof(int);

                    ms.Position += 0xA;
                    if (character.EquippedArmour != null)
                        bw.Write(character.EquippedArmour.ID);
                    else
                        ms.Position += sizeof(int);

                    ms.Position += 0xA;
                    if (character.EquippedAccessory != null)
                        bw.Write(character.EquippedAccessory.ID);
                    else
                        ms.Position += sizeof(int);

                    ms.Position += 0x1C;

                    bw.Write(character.Level);
                    bw.Write(character.Exp);

                    ms.Position += 0x10;
                    bw.Write(character.MaxHP);
                    bw.Write(character.MaxSP);

                    bw.Write(character.St);
                    bw.Write(character.Ma);
                    bw.Write(character.En);
                    bw.Write(character.Ag);
                    bw.Write(character.Lu);

                    ms.Position += 0x22;
                    bw.Write(character.CurrentHP);
                    bw.Write(character.CurrentSP);

                    ms.Position += 0x10;
                }
            }
        }

        private void SaveGeneralData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var bw = new BinaryWriter(ms))
            {
                byte[] lastNameP3 = EncryptName(si.Characters[1].LastName);
                byte[] nameP3 = EncryptName(si.Characters[1].BaseName);
                byte[] lastNameP4 = EncryptName(si.Characters[0].LastName);
                byte[] nameP4 = EncryptName(si.Characters[0].BaseName);
                int fillLastNameP3 = 0xC - lastNameP3.Length;
                int fillNameP3 = 0xC - nameP3.Length;
                int fillLastNameP4 = 0xC - lastNameP4.Length;
                int fillNameP4 = 0xC - nameP4.Length;

                ms.Position = 0x18;
                if (si.MainCharIsP4)
                {
                    bw.Write(lastNameP4);
                    bw.Write(new byte[fillLastNameP4]);
                    ms.Position += 2;
                    bw.Write(nameP4);
                    bw.Write(new byte[fillNameP4]);
                }
                else
                {
                    bw.Write(lastNameP3);
                    bw.Write(new byte[fillLastNameP3]);
                    ms.Position += 2;
                    bw.Write(nameP3);
                    bw.Write(new byte[fillNameP3]);
                }

                ms.Position = 0x34;
                bw.Write(si.ClearStatus);

                ms.Position = 0x809B;
                bw.Write(si.Enepedia);

                ms.Position = 0x84DB;
                bw.Write(si.Yen);
                bw.Write(si.PartyGauge);

                ms.Position = 0x84E3;
                bw.Write(lastNameP3);
                bw.Write(new byte[fillLastNameP3]);

                ms.Position += 2;
                bw.Write(nameP3);
                bw.Write(new byte[fillNameP3]);

                ms.Position = 0x84FF;
                bw.Write(lastNameP4);
                bw.Write(new byte[fillLastNameP4]);

                ms.Position += 2;
                bw.Write(nameP4);
                bw.Write(new byte[fillNameP4]);
            }
        }

        private void SaveInventoryData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var bw = new BinaryWriter(ms))
            {
                ms.Position = 0x966F;
                for (int i = 0; i < si.Inventory.Length; i++)
                {
                    bw.Write(si.Inventory[i].ID);

                    bw.Write(new byte[0xA]);
                }
            }
        }

        private void SaveKeyItemsData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var bw = new BinaryWriter(ms))
            {
                ms.Position = 0x9C;
                int index = 0;
                int indexEnd = index + 8 * 4;
                BitShiftSave(bw, si.KeyItems, index, indexEnd);

                ms.Position = 0xA4;
                index = indexEnd;
                indexEnd = index + 8;
                BitShiftSave(bw, si.KeyItems, index, indexEnd);

                ms.Position = 0xAE;
                index = indexEnd;
                indexEnd = index + 8 * 2;
                BitShiftSave(bw, si.KeyItems, index, indexEnd);

                ms.Position = 0xB2;
                index = indexEnd;
                indexEnd = index + 8 * 5;
                BitShiftSave(bw, si.KeyItems, index, indexEnd);
            }
        }

        private void SaveSummonedPersonasData()
        {
            using (var ms = new MemoryStream(si.SaveData))
            using (var bw = new BinaryWriter(ms))
            {
                ms.Position = 0x851B;
                for (int i = 0; i < si.SummonedPersonas.Length; i++)
                {
                    var persona = si.SummonedPersonas[i];

                    bw.Write(persona.Active);
                    bw.Write(persona.Id);
                    bw.Write(persona.Level);

                    ms.Position += 2;
                    bw.Write(persona.Exp);

                    for (int x = 0; x < persona.Skills.Length; x++)
                        bw.Write(persona.Skills[x].ID);

                    ms.Position += 4;
                    bw.Write(persona.BonusHP);
                    bw.Write(persona.BonusSP);

                    ms.Position += 0x28;
                }
            }
        }

        #endregion SaveWriting
    }
}
