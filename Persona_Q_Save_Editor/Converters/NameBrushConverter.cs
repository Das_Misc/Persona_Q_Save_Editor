﻿using MahApps.Metro;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Persona_Q_Save_Editor.Converters
{
    internal class NameBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int)
                switch ((int)value)
                {
                    case 0x4BD: // Germanium Ring Accessory
                    case 0x4BC: // Omnipotent Orb Accessory
                    case 0x388: // Aigis Armour
                    case 0x359: // Chie Armour
                    case 0x3B5: // Koromaru Armour
                    case 0x35D: // Mitsuru Armour
                    case 0x35B: // Naoto Armour
                    case 0x330: // P4 Armour
                    case 0x331: // P3 Armour
                    case 0x332: // Yosuke Armour
                    case 0x333: // Kanji Armour
                    case 0x334: // Teddie Armour
                    case 0x335: // Junpei Armour
                    case 0x336: // Akihiko Armour
                    case 0x337: // Ken Armour
                    case 0x338: // Shinjiro Armour
                    case 0x308: // Unisex Armour
                    case 0x35C: // Yukari Armour
                    case 0x35A: // Yukiko Armour
                    case 0x3E8: // Zen and Rei Armour
                    case 0x50D: // Balm of Life Consumable
                    case 0x512: // Purifying Water Consumable
                    case 0x513: // Purifying Salt Consumable
                    case 0x532: // Homunculus Consumable
                    case 0x539: // Soma Consumable
                    case 0x53B: // Goho-M More Consumable
                    case 0x53C: // Goba-K More Consumable
                    case 0x542: // Vanish Ball Consumable
                    case 0x186: // Aigis Weapon
                    case 0x2A6: // Akihiko Weapon
                    case 0x96: // Chie Weapon
                    case 0x1A: // Junpei Weapon
                    case 0xF6: // Kanji Weapon
                    case 0x276: // Ken Weapon
                    case 0x57: // Koromaru Weapon
                    case 0x246: // Mitsuru Weapon
                    case 0x126: // Naoto Weapon
                    case 0x1E7: // P3 Weapon
                    case 0x19: // P4 Weapon
                    case 0x2D6: // Shinjiro Weapon
                    case 0x156: // Teddie Weapon
                    case 0x56: // Yosuke Weapon
                    case 0x216: // Yukari Weapon
                    case 0xC6: // Yukiko Weapon
                    case 0x1B7: // Zen and Rei Weapon
                        return new SolidColorBrush((Color)ThemeManager.GetResourceFromAppStyle(Application.Current.MainWindow, "AccentColor"));

                    case 0x35E: // Female Armour
                    case 0x339: // Male Armour
                        return new SolidColorBrush((Color)ThemeManager.GetAccent("Violet").Resources["AccentColor"]);

                    default:
                        return Brushes.White;
                }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
