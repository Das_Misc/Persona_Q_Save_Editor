﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Persona_Q_Save_Editor.Converters
{
    internal class BonusHPSPConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is short)
                return (short)value / 10;

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double)
                return (short)((double)value * 10);

            return null;
        }
    }
}
