﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Persona_Q_Save_Editor.Converters
{
    internal class CharNameVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is short)
            {
                var character = (short)value;

                if (character == 0 || character == 1)
                    return Visibility.Visible;
                else
                    return Visibility.Hidden;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
