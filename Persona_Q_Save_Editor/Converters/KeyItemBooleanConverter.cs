﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Persona_Q_Save_Editor.Converters
{
    internal class KeyItemBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
                return string.IsNullOrWhiteSpace((string)value) ? true : false;

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
