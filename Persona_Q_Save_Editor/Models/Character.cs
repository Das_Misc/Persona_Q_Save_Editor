﻿using System.ComponentModel;

namespace Persona_Q_Save_Editor.Models
{
    public class Character : INotifyPropertyChanged
    {
        private short ag, en, id, lu, ma, maxHP, maxSP, st;
        private string baseName, lastName;
        private int currentHP, currentSP, exp, level;
        private Item equippedAccessory, equippedArmour, equippedWeapon;
        private Persona persona, subPersona;

        public short Ag
        {
            get { return ag; }

            set
            {
                if (ag == value) return;
                ag = value;
                NotifyPropertyChanged("Ag");
            }
        }

        public string BaseName
        {
            get { return baseName; }
            set { baseName = value; }
        }

        public int CurrentHP
        {
            get { return currentHP; }
            set
            {
                if (currentHP == value) return;
                currentHP = value;
                NotifyPropertyChanged("CurrentHP");
            }
        }

        public int CurrentSP
        {
            get { return currentSP; }
            set
            {
                if (currentSP == value) return;
                currentSP = value;
                NotifyPropertyChanged("CurrentSP");
            }
        }

        public short En
        {
            get { return en; }

            set
            {
                if (en == value) return;
                en = value;
                NotifyPropertyChanged("En");
            }
        }

        public Item EquippedAccessory
        {
            get { return equippedAccessory; }
            set { equippedAccessory = value; }
        }

        public Item EquippedArmour
        {
            get { return equippedArmour; }
            set { equippedArmour = value; }
        }

        public Item EquippedWeapon
        {
            get { return equippedWeapon; }
            set { equippedWeapon = value; }
        }

        public int Exp
        {
            get { return exp; }
            set
            {
                if (exp == value) return;
                exp = value;
                NotifyPropertyChanged("Exp");
            }
        }

        public short Id { get { return id; } }

        public string Image
        {
            get
            {
                string file;

                if (id == 0)
                    file = "P4_Yu";
                else if (id == 1)
                    file = "P3_Minato";
                else
                    file = Lists.Characters[id].Replace(" ", "_");

                return "/Resources/" + file + ".png";
            }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public int Level
        {
            get { return level; }

            set
            {
                if (level == value) return;
                level = value;
                NotifyPropertyChanged("Level");
            }
        }

        public short Lu
        {
            get { return lu; }

            set
            {
                if (lu == value) return;
                lu = value;
                NotifyPropertyChanged("Lu");
            }
        }

        public short Ma
        {
            get { return ma; }

            set
            {
                if (ma == value) return;
                ma = value;
                NotifyPropertyChanged("Ma");
            }
        }

        public short MaxHP
        {
            get { return maxHP; }

            set
            {
                if (maxHP == value) return;
                maxHP = value;
                NotifyPropertyChanged("MaxHP");
            }
        }

        public short MaxSP
        {
            get { return maxSP; }

            set
            {
                if (maxSP == value) return;
                maxSP = value;
                NotifyPropertyChanged("MaxSP");
            }
        }

        public string Name { get { return Lists.Characters[id]; } }

        public Persona Persona
        {
            get { return persona; }
            set
            {
                if (persona == value) return;
                persona = value;
                NotifyPropertyChanged("Persona");
            }
        }

        public short St
        {
            get { return st; }

            set
            {
                if (st == value) return;
                st = value;
                NotifyPropertyChanged("St");
            }
        }

        public Persona SubPersona
        {
            get { return subPersona; }
            set
            {
                if (subPersona == value) return;
                subPersona = value;
                NotifyPropertyChanged("SubPersona");
            }
        }

        public Character(short id)
        {
            this.id = id;
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
