﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Persona_Q_Save_Editor.Models
{
    internal class SaveInfo : INotifyPropertyChanged
    {
        private Character character;
        private Character[] characters;
        private byte clearStatus;
        private byte[] enepedia, saveData;
        private Item[] inventory;
        private KeyItem[] keyItems;
        private short partyGauge;
        private string saveFile;
        private bool saveLoaded;
        private Persona[] summonedPersonas;
        private int yen;

        public Character Character
        {
            get { return character; }
            set
            {
                if (character == value) return;
                character = value;
                NotifyPropertyChanged("Character");
            }
        }

        public Character[] Characters
        {
            get { return characters; }
            set
            {
                if (characters == value) return;
                characters = value;
                NotifyPropertyChanged("Characters");
            }
        }

        public byte ClearStatus
        {
            get { return clearStatus; }
            set
            {
                if (clearStatus == value) return;
                clearStatus = value;
                NotifyPropertyChanged("ClearStatus");
            }
        }

        public byte[] Enepedia
        {
            get { return enepedia; }
            set { enepedia = value; }
        }

        public Item[] Inventory
        {
            get { return inventory; }
            set
            {
                if (inventory == value) return;
                inventory = value;
                NotifyPropertyChanged("Inventory");
            }
        }

        public KeyItem[] KeyItems
        {
            get { return keyItems; }
            set
            {
                if (keyItems == value) return;
                keyItems = value;
                NotifyPropertyChanged("KeyItems");
            }
        }

        public bool MainCharIsP4
        {
            get { return ClearStatus % 2 == 0; }
        }

        public short PartyGauge
        {
            get { return partyGauge; }
            set
            {
                if (partyGauge == value) return;
                partyGauge = value;
                NotifyPropertyChanged("PartyGauge");
            }
        }

        public ObservableCollection<Persona> PersonaList { get; set; }

        public byte[] SaveData
        {
            get { return saveData; }
            set { saveData = value; }
        }

        public string SaveFile
        {
            get { return saveFile; }
            set
            {
                if (saveFile == value) return;
                saveFile = value;
                NotifyPropertyChanged("SaveFile");
            }
        }

        public bool SaveLoaded
        {
            get { return saveLoaded; }
            set
            {
                if (saveLoaded == value) return;
                saveLoaded = value;
                NotifyPropertyChanged("SaveLoaded");
            }
        }

        public Persona[] SummonedPersonas
        {
            get { return summonedPersonas; }
            set
            {
                if (summonedPersonas == value) return;
                summonedPersonas = value;
                NotifyPropertyChanged("SummonedPersonas");
            }
        }

        public int Yen
        {
            get { return yen; }
            set
            {
                if (yen == value) return;
                yen = value;
                NotifyPropertyChanged("Yen");
            }
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
