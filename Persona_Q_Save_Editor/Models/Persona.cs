﻿using System.ComponentModel;

namespace Persona_Q_Save_Editor.Models
{
    public class Persona : INotifyPropertyChanged
    {
        private static Persona[] list;
        private short active, bonusHP, bonusSP, id, level;
        private string arcana, name;
        private int exp;
        private Skill[] skills;

        public static Persona[] List
        {
            get { return list; }
            set
            {
                list = value;
                foreach (var persona in list)
                    persona.UpdateData();
            }
        }

        public short Active
        {
            get { return active; }
            set { active = value; }
        }

        public string Arcana { get { return arcana; } }

        public short BonusHP
        {
            get { return bonusHP; }
            set
            {
                if (bonusHP == value) return;
                bonusHP = value;
                NotifyPropertyChanged("BonusHP");
            }
        }

        public short BonusSP
        {
            get { return bonusSP; }
            set
            {
                if (bonusSP == value) return;
                bonusSP = value;
                NotifyPropertyChanged("BonusSP");
            }
        }

        public int Exp
        {
            get { return exp; }
            set
            {
                if (exp == value) return;
                exp = value;
                NotifyPropertyChanged("Exp");
            }
        }

        public short Id
        {
            get { return id; }
            set
            {
                if (id == value) return;
                id = value;
                UpdateData();
            }
        }

        public short Level
        {
            get { return level; }
            set { level = value; }
        }

        public string Name { get { return name; } }

        public Skill[] Skills
        {
            get { return skills; }
            set
            {
                if (skills == value) return;
                skills = value;
                NotifyPropertyChanged("Skills");
            }
        }

        private void UpdateData()
        {
            var personaData = Lists.GetPersonaData(id);
            name = personaData.Item1;
            arcana = personaData.Item2;

            NotifyPropertyChanged("ID");
            NotifyPropertyChanged("Name");
            NotifyPropertyChanged("Arcana");
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
