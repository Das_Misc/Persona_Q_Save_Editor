﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Persona_Q_Save_Editor.Models
{
    internal class Lists
    {
        #region Characters

        internal static string[] Characters =
        {
            "P4",
            "P3",
            "Yosuke",
            "Chie",
            "Yukiko",
            "Rise",
            "Kanji",
            "Naoto",
            "Teddie",
            "Yukari",
            "Junpei",
            "Akihiko",
            "Mitsuru",
            "Fuuka",
            "Aigis",
            "Ken",
            "Koromaru",
            "Shinjiro",
            "Zen and Rei",
            "Zen",
        };

        #endregion Characters

        #region Personas

        // Compendium offset: 0x3020

        #region PersonasChariot

        internal static Dictionary<int, Tuple<string, string>> PersonasChariot = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x27, new Tuple<string, string>("Mezuki", "Chariot") },
            { 0x3F, new Tuple<string, string>("Nata Taishi", "Chariot") },
            { 0x40, new Tuple<string, string>("Chimera", "Chariot") },
            { 0x41, new Tuple<string, string>("Eligor", "Chariot") },
            { 0x42, new Tuple<string, string>("Nezha", "Chariot") },
            { 0x43, new Tuple<string, string>("Ares", "Chariot") },
            { 0x44, new Tuple<string, string>("Oumitsunu", "Chariot") },
            { 0x45, new Tuple<string, string>("Triglav", "Chariot") },
            { 0x46, new Tuple<string, string>("Thor", "Chariot") },
            { 0x48, new Tuple<string, string>("Atavaka", "Chariot") },
            { 0x49, new Tuple<string, string>("Futsunushi", "Chariot") },
            { 0xD9, new Tuple<string, string>("Haraedo-no-Okami", "Chariot") },
            { 0xF6, new Tuple<string, string>("Tomoe", "Chariot") },
            { 0xF7, new Tuple<string, string>("Suzuka Gongen", "Chariot") },
            { 0x10C, new Tuple<string, string>("Palladion", "Chariot") },
            { 0x10D, new Tuple<string, string>("Athena", "Chariot") },
        };

        #endregion PersonasChariot

        #region PersonasDeath

        internal static Dictionary<int, Tuple<string, string>> PersonasDeath = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x75, new Tuple<string, string>("Mokoi", "Death") },
            { 0x76, new Tuple<string, string>("Turdak", "Death") },
            { 0x77, new Tuple<string, string>("Matador", "Death") },
            { 0x78, new Tuple<string, string>("White Rider", "Death") },
            { 0x79, new Tuple<string, string>("Samael", "Death") },
            { 0x7A, new Tuple<string, string>("Mot", "Death") },
            { 0x7B, new Tuple<string, string>("Pale Rider", "Death") },
            { 0x7C, new Tuple<string, string>("Alice", "Death") },
            { 0x7D, new Tuple<string, string>("Mahakala", "Death") },
            { 0xD0, new Tuple<string, string>("Thanatos", "Death") },
        };

        #endregion PersonasDeath

        #region PersonasDevil

        internal static Dictionary<int, Tuple<string, string>> PersonasDevil = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x62, new Tuple<string, string>("Pazuzu", "Devil") },
            { 0x71, new Tuple<string, string>("Basilisk", "Devil") },
            { 0x86, new Tuple<string, string>("Ukobach", "Devil") },
            { 0x87, new Tuple<string, string>("Incubus", "Devil") },
            { 0x88, new Tuple<string, string>("Succubus", "Devil") },
            { 0x89, new Tuple<string, string>("Lilith", "Devil") },
            { 0x8A, new Tuple<string, string>("Belphegor", "Devil") },
            { 0x8B, new Tuple<string, string>("Belial", "Devil") },
            { 0x8C, new Tuple<string, string>("Astaroth", "Devil") },
            { 0x8D, new Tuple<string, string>("Beelzebub", "Devil") },
        };

        #endregion PersonasDevil

        #region PersonasEmperor

        internal static Dictionary<int, Tuple<string, string>> PersonasEmperor = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x24, new Tuple<string, string>("Oberon", "Emperor") },
            { 0x25, new Tuple<string, string>("King Frost", "Emperor") },
            { 0x26, new Tuple<string, string>("Setanta", "Emperor") },
            { 0x28, new Tuple<string, string>("Oukuninushi", "Emperor") },
            { 0x29, new Tuple<string, string>("Thoth", "Emperor") },
            { 0x2B, new Tuple<string, string>("Pabilsag", "Emperor") },
            { 0x2C, new Tuple<string, string>("Kingu", "Emperor") },
            { 0x2D, new Tuple<string, string>("Barong", "Emperor") },
            { 0x2E, new Tuple<string, string>("Odin", "Emperor") },
            { 0xD7, new Tuple<string, string>("Takeji Zaiten", "Emperor") },
            { 0xFC, new Tuple<string, string>("Take-Mikazuchi", "Emperor") },
            { 0xFD, new Tuple<string, string>("Rokuten Maou", "Emperor") },
            { 0x106, new Tuple<string, string>("Polydeuces", "Emperor") },
            { 0x107, new Tuple<string, string>("Caesar", "Emperor") },
        };

        #endregion PersonasEmperor

        #region PersonasEmpress

        internal static Dictionary<int, Tuple<string, string>> PersonasEmpress = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1D, new Tuple<string, string>("Yaksini", "Empress") },
            { 0x1E, new Tuple<string, string>("Titania", "Empress") },
            { 0x1F, new Tuple<string, string>("Gorgon", "Empress") },
            { 0x20, new Tuple<string, string>("Gabriel", "Empress") },
            { 0x21, new Tuple<string, string>("Skadi", "Empress") },
            { 0x22, new Tuple<string, string>("Mother Harlot", "Empress") },
            { 0x23, new Tuple<string, string>("Laksmi", "Empress") },
            { 0xBC, new Tuple<string, string>("Alilat", "Empress") },
            { 0x108, new Tuple<string, string>("Penthesilea", "Empress") },
            { 0x109, new Tuple<string, string>("Artemisia", "Empress") },
        };

        #endregion PersonasEmpress

        #region PersonasFool

        internal static Dictionary<int, Tuple<string, string>> PersonasFool = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x02, new Tuple<string, string>("Slime", "Fool") },
            { 0x04, new Tuple<string, string>("Legion", "Fool") },
            { 0x05, new Tuple<string, string>("Ose", "Fool") },
            { 0x06, new Tuple<string, string>("Black Frost", "Fool") },
            { 0x07, new Tuple<string, string>("Decarabia", "Fool") },
            { 0x08, new Tuple<string, string>("Shiki-Ouji", "Fool") },
            { 0x09, new Tuple<string, string>("Loki", "Fool") },
            { 0xD3, new Tuple<string, string>("Orpheus Telos", "Fool") },
            { 0xF0, new Tuple<string, string>("Izanagi", "Fool") },
            { 0xF2, new Tuple<string, string>("Orpheus", "Fool") },
            { 0x114, new Tuple<string, string>("Zen", "Fool") },
            { 0x115, new Tuple<string, string>("Zen (2)", "Fool") },
            { 0x116, new Tuple<string, string>("Rei", "Fool") },
        };

        #endregion PersonasFool

        #region PersonasFortune

        internal static Dictionary<int, Tuple<string, string>> PersonasFortune = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x5A, new Tuple<string, string>("Empusa", "Fortune") },
            { 0x5C, new Tuple<string, string>("Fortuna", "Fortune") },
            { 0x5D, new Tuple<string, string>("Clotho", "Fortune") },
            { 0x5E, new Tuple<string, string>("Lachesis", "Fortune") },
            { 0x5F, new Tuple<string, string>("Ananta", "Fortune") },
            { 0x60, new Tuple<string, string>("Atropos", "Fortune") },
            { 0x61, new Tuple<string, string>("Norn", "Fortune") },
            { 0xDA, new Tuple<string, string>("Yamato Sumeragi", "Fortune") },
            { 0xFE, new Tuple<string, string>("Sukuna-Hikona", "Fortune") },
            { 0xFF, new Tuple<string, string>("Yamato Takeru", "Fortune") },
        };

        #endregion PersonasFortune

        #region PersonasHangedMan

        internal static Dictionary<int, Tuple<string, string>> PersonasHangedMan = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x14, new Tuple<string, string>("Koropokkur", "Hanged Man") },
            { 0x6D, new Tuple<string, string>("Berith", "Hanged Man") },
            { 0x6E, new Tuple<string, string>("Ikusa", "Hanged Man") },
            { 0x6F, new Tuple<string, string>("Orthrus", "Hanged Man") },
            { 0x70, new Tuple<string, string>("Yatsufusa", "Hanged Man") },
            { 0x72, new Tuple<string, string>("Hell Biker", "Hanged Man") },
            { 0x73, new Tuple<string, string>("Vasuki", "Hanged Man") },
            { 0x74, new Tuple<string, string>("Attis", "Hanged Man") },
        };

        #endregion PersonasHangedMan

        #region PersonasHermit

        internal static Dictionary<int, Tuple<string, string>> PersonasHermit = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x51, new Tuple<string, string>("Azumi", "Hermit") },
            { 0x52, new Tuple<string, string>("Ippon-Datara", "Hermit") },
            { 0x53, new Tuple<string, string>("Mothman", "Hermit") },
            { 0x54, new Tuple<string, string>("Hitokotonusi", "Hermit") },
            { 0x55, new Tuple<string, string>("Kurama Tengu", "Hermit") },
            { 0x56, new Tuple<string, string>("Niddhoggr", "Hermit") },
            { 0x57, new Tuple<string, string>("Nebiros", "Hermit") },
            { 0x58, new Tuple<string, string>("Arahabaki", "Hermit") },
            { 0x59, new Tuple<string, string>("Ongyo-ki", "Hermit") },
            { 0x97, new Tuple<string, string>("Nue", "Hermit") },
        };

        #endregion PersonasHermit

        #region PersonasHierophant

        internal static Dictionary<int, Tuple<string, string>> PersonasHierophant = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x2F, new Tuple<string, string>("Anzu", "Hierophant") },
            { 0x30, new Tuple<string, string>("Shiisaa", "Hierophant") },
            { 0x31, new Tuple<string, string>("Unicorn", "Hierophant") },
            { 0x32, new Tuple<string, string>("Gozuki", "Hierophant") },
            { 0x33, new Tuple<string, string>("Flauros", "Hierophant") },
            { 0x34, new Tuple<string, string>("Daisoujou", "Hierophant") },
            { 0x35, new Tuple<string, string>("Hachiman", "Hierophant") },
            { 0x36, new Tuple<string, string>("Kohryu", "Hierophant") },
            { 0xA3, new Tuple<string, string>("Mishaguji", "Hierophant") },
            { 0x112, new Tuple<string, string>("Castor", "Hierophant") },
            { 0x113, new Tuple<string, string>("Castor (2)", "Hierophant") },
        };

        #endregion PersonasHierophant

        #region PersonasJudgement

        internal static Dictionary<int, Tuple<string, string>> PersonasJudgement = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xB0, new Tuple<string, string>("Red Rider", "Judgement") },
            { 0xB1, new Tuple<string, string>("Anubis", "Judgement") },
            { 0xB2, new Tuple<string, string>("Black Rider", "Judgement") },
            { 0xB3, new Tuple<string, string>("Trumpeter", "Judgement") },
            { 0xB4, new Tuple<string, string>("Michael", "Judgement") },
            { 0xB5, new Tuple<string, string>("Satan", "Judgement") },
            { 0xB6, new Tuple<string, string>("Metatron", "Judgement") },
            { 0xB7, new Tuple<string, string>("Ardha", "Judgement") },
            { 0xB8, new Tuple<string, string>("Lucifer", "Judgement") },
            { 0xB9, new Tuple<string, string>("Warrior Zeus", "Judgement") },
            { 0xBA, new Tuple<string, string>("Zeus", "Judgement") },
        };

        #endregion PersonasJudgement

        #region PersonasJustice

        internal static Dictionary<int, Tuple<string, string>> PersonasJustice = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x47, new Tuple<string, string>("Ophanim", "Justice") },
            { 0x4A, new Tuple<string, string>("Angel", "Justice") },
            { 0x4B, new Tuple<string, string>("Power", "Justice") },
            { 0x4C, new Tuple<string, string>("Virtue", "Justice") },
            { 0x4D, new Tuple<string, string>("Throne", "Justice") },
            { 0x4E, new Tuple<string, string>("Uriel", "Justice") },
            { 0x4F, new Tuple<string, string>("Melchizedek", "Justice") },
            { 0xBB, new Tuple<string, string>("Archangel", "Justice") },
            { 0xBD, new Tuple<string, string>("Dominion", "Justice") },
            { 0x10E, new Tuple<string, string>("Nemesis", "Justice") },
            { 0x10F, new Tuple<string, string>("Kala-Nemi", "Justice") },
        };

        #endregion PersonasJustice

        #region PersonasLovers

        internal static Dictionary<int, Tuple<string, string>> PersonasLovers = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x37, new Tuple<string, string>("Pixie", "Lovers") },
            { 0x38, new Tuple<string, string>("Alp", "Lovers") },
            { 0x39, new Tuple<string, string>("Moh Shuvuu", "Lovers") },
            { 0x3A, new Tuple<string, string>("Queen Mab", "Lovers") },
            { 0x3B, new Tuple<string, string>("Leanan Sidhe", "Lovers") },
            { 0x3C, new Tuple<string, string>("Raphael", "Lovers") },
            { 0x3D, new Tuple<string, string>("Cybele", "Lovers") },
            { 0x3E, new Tuple<string, string>("Ishtar", "Lovers") },
            { 0xD5, new Tuple<string, string>("Kouzeon", "Lovers") },
            { 0xFA, new Tuple<string, string>("Himiko", "Lovers") },
            { 0xFB, new Tuple<string, string>("Kanzeon", "Lovers") },
            { 0x102, new Tuple<string, string>("Io", "Lovers") },
            { 0x103, new Tuple<string, string>("Isis", "Lovers") },
        };

        #endregion PersonasLovers

        #region PersonasMagician

        internal static Dictionary<int, Tuple<string, string>> PersonasMagician = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x0A, new Tuple<string, string>("Agathion", "Magician") },
            { 0x0B, new Tuple<string, string>("Orobas", "Magician") },
            { 0x0C, new Tuple<string, string>("Jack Frost", "Magician") },
            { 0x0D, new Tuple<string, string>("Hua Po", "Magician") },
            { 0x0E, new Tuple<string, string>("Pyro Jack", "Magician") },
            { 0x0F, new Tuple<string, string>("Dis", "Magician") },
            { 0x10, new Tuple<string, string>("Rangda", "Magician") },
            { 0x11, new Tuple<string, string>("Jinn", "Magician") },
            { 0x12, new Tuple<string, string>("Surt", "Magician") },
            { 0x13, new Tuple<string, string>("Mada", "Magician") },
            { 0xD8, new Tuple<string, string>("Takehaya Susano-o", "Magician") },
            { 0xF4, new Tuple<string, string>("Jiraiya", "Magician") },
            { 0xF5, new Tuple<string, string>("Susano-o", "Magician") },
            { 0x104, new Tuple<string, string>("Hermes", "Magician") },
            { 0x105, new Tuple<string, string>("Trismegistus", "Magician") },
        };

        #endregion PersonasMagician

        #region PersonasMoon

        internal static Dictionary<int, Tuple<string, string>> PersonasMoon = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x6C, new Tuple<string, string>("Onmoraki", "Moon") },
            { 0x9E, new Tuple<string, string>("Andras", "Moon") },
            { 0x9F, new Tuple<string, string>("Nozuchi", "Moon") },
            { 0xA0, new Tuple<string, string>("Orochi", "Moon") },
            { 0xA1, new Tuple<string, string>("Alraune", "Moon") },
            { 0xA2, new Tuple<string, string>("Girimehkala", "Moon") },
            { 0xA4, new Tuple<string, string>("Chernobog", "Moon") },
            { 0xA5, new Tuple<string, string>("Seth", "Moon") },
            { 0xA6, new Tuple<string, string>("Baal Zebul", "Moon") },
            { 0xA7, new Tuple<string, string>("Sandalphon", "Moon") },
            { 0xD1, new Tuple<string, string>("Kaguya", "Moon") },
        };

        #endregion PersonasMoon

        #region PersonasPriestess

        internal static Dictionary<int, Tuple<string, string>> PersonasPriestess = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x03, new Tuple<string, string>("Ame-no-Uzume", "Priestess") },
            { 0x16, new Tuple<string, string>("High Pixie", "Priestess") },
            { 0x17, new Tuple<string, string>("Ganga", "Priestess") },
            { 0x18, new Tuple<string, string>("Parvati", "Priestess") },
            { 0x1A, new Tuple<string, string>("Hariti", "Priestess") },
            { 0x1B, new Tuple<string, string>("Tzitzimitl", "Priestess") },
            { 0x1C, new Tuple<string, string>("Scathach", "Priestess") },
            { 0xD6, new Tuple<string, string>("Sumeo-Okami", "Priestess") },
            { 0xF8, new Tuple<string, string>("Konohana Sakuya", "Priestess") },
            { 0xF9, new Tuple<string, string>("Amaterasu", "Priestess") },
            { 0x10A, new Tuple<string, string>("Lucia", "Priestess") },
            { 0x10B, new Tuple<string, string>("Juno", "Priestess") },
        };

        #endregion PersonasPriestess

        #region PersonasStar

        internal static Dictionary<int, Tuple<string, string>> PersonasStar = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x15, new Tuple<string, string>("Sarasvati", "Star") },
            { 0x19, new Tuple<string, string>("Kikuri-Hime", "Star") },
            { 0x50, new Tuple<string, string>("Sraosha", "Star") },
            { 0x96, new Tuple<string, string>("Kaiwan", "Star") },
            { 0x98, new Tuple<string, string>("Neko Shogun", "Star") },
            { 0x9A, new Tuple<string, string>("Garuda", "Star") },
            { 0x9B, new Tuple<string, string>("Kartikeya", "Star") },
            { 0x9C, new Tuple<string, string>("Saturnus", "Star") },
            { 0x9D, new Tuple<string, string>("Helel", "Star") },
            { 0xAA, new Tuple<string, string>("Yatagarasu", "Star") },
            { 0xD4, new Tuple<string, string>("Kamui-Moshiri", "Star") },
            { 0x100, new Tuple<string, string>("Kintoki-Douji", "Star") },
            { 0x101, new Tuple<string, string>("Kamui", "Star") },
        };

        #endregion PersonasStar

        #region PersonasStrength

        internal static Dictionary<int, Tuple<string, string>> PersonasStrength = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x2A, new Tuple<string, string>("Ouyamatsumi", "Strength") },
            { 0x63, new Tuple<string, string>("Sandman", "Strength") },
            { 0x64, new Tuple<string, string>("Valkyrie", "Strength") },
            { 0x65, new Tuple<string, string>("Rakshasa", "Strength") },
            { 0x66, new Tuple<string, string>("Tsuchigumo", "Strength") },
            { 0x67, new Tuple<string, string>("Oni", "Strength") },
            { 0x68, new Tuple<string, string>("Hanuman", "Strength") },
            { 0x69, new Tuple<string, string>("Siegfried", "Strength") },
            { 0x6A, new Tuple<string, string>("Kali", "Strength") },
            { 0x6B, new Tuple<string, string>("Zaou-Gongen", "Strength") },
            { 0x110, new Tuple<string, string>("Cerberus", "Strength") },
            { 0x111, new Tuple<string, string>("Cerberus (2)", "Strength") },
        };

        #endregion PersonasStrength

        #region PersonasSun

        internal static Dictionary<int, Tuple<string, string>> PersonasSun = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x99, new Tuple<string, string>("Ganesha", "Sun") },
            { 0xA8, new Tuple<string, string>("Cu Sith", "Sun") },
            { 0xA9, new Tuple<string, string>("Phoenix", "Sun") },
            { 0xAB, new Tuple<string, string>("Narasimha", "Sun") },
            { 0xAC, new Tuple<string, string>("Tam Lin", "Sun") },
            { 0xAD, new Tuple<string, string>("Jatayu", "Sun") },
            { 0xAE, new Tuple<string, string>("Suparna", "Sun") },
            { 0xAF, new Tuple<string, string>("Asura", "Sun") },
        };

        #endregion PersonasSun

        #region PersonasTemperance

        internal static Dictionary<int, Tuple<string, string>> PersonasTemperance = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x7E, new Tuple<string, string>("Apsaras", "Temperance") },
            { 0x7F, new Tuple<string, string>("Xiezhai", "Temperance") },
            { 0x80, new Tuple<string, string>("Mithra", "Temperance") },
            { 0x81, new Tuple<string, string>("Genbu", "Temperance") },
            { 0x82, new Tuple<string, string>("Seiryu", "Temperance") },
            { 0x83, new Tuple<string, string>("Suzaku", "Temperance") },
            { 0x84, new Tuple<string, string>("Byakko", "Temperance") },
            { 0x85, new Tuple<string, string>("Vishnu", "Temperance") },
        };

        #endregion PersonasTemperance

        #region PersonasTower

        internal static Dictionary<int, Tuple<string, string>> PersonasTower = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x5B, new Tuple<string, string>("Raiju", "Tower") },
            { 0x8E, new Tuple<string, string>("Cu Chulainn", "Tower") },
            { 0x8F, new Tuple<string, string>("Abaddon", "Tower") },
            { 0x90, new Tuple<string, string>("Mara", "Tower") },
            { 0x91, new Tuple<string, string>("Seiten Taisei", "Tower") },
            { 0x92, new Tuple<string, string>("Yoshitsune", "Tower") },
            { 0x93, new Tuple<string, string>("Masakado", "Tower") },
            { 0x94, new Tuple<string, string>("Shiva", "Tower") },
            { 0x95, new Tuple<string, string>("Chi You", "Tower") },
            { 0xD2, new Tuple<string, string>("Magatsu-Izanagi", "Tower") },
        };

        #endregion PersonasTower

        #region PersonasWorld

        internal static Dictionary<int, Tuple<string, string>> PersonasWorld = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xF1, new Tuple<string, string>("Izanagi-no-Okami", "World") },
            { 0xF3, new Tuple<string, string>("Messiah", "World") },
        };

        #endregion PersonasWorld

        #endregion Personas

        #region FullLists

        #region ItemsFullList

        internal static Dictionary<int, Tuple<string, string>> itemsFullList;

        internal static Tuple<string, string> GetItemData(int id)
        {
            InitializeItemsFullList();

            return itemsFullList.ContainsKey(id) ? itemsFullList[id] : null;
        }

        private static void InitializeItemsFullList()
        {
            if (itemsFullList == null)
            {
                itemsFullList = new Dictionary<int, Tuple<string, string>>();

                var armours = ArmourAigis.Concat(ArmourFemales).Concat(ArmourKoromaru).Concat(ArmourMales)
                                         .Concat(ArmourUnisex).Concat(ArmourZenRei)
                                         .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                var materials = MaterialsDropped.Concat(MaterialsPowerSpot).Concat(MaterialsSacrifice)
                                                .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                var weapons = WeaponsAigis.Concat(WeaponsAkihiko).Concat(WeaponsChie).Concat(WeaponsKanji)
                                          .Concat(WeaponsKen).Concat(WeaponsMitsuru).Concat(WeaponsNaoto)
                                          .Concat(WeaponsP3).Concat(WeaponsP4Junpei).Concat(WeaponsShinjiro)
                                          .Concat(WeaponsTeddie).Concat(WeaponsYosukeKoromaru).Concat(WeaponsYukari)
                                          .Concat(WeaponsYukiko).Concat(WeaponsZenRei)
                                          .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                var skillCards = SkillCardsMagic.Concat(SkillCardsDebuff).Concat(SkillCardsBash).Concat(SkillCardsCut)
                                                .Concat(SkillCardsStab).Concat(SkillCardsAlmighty).Concat(SkillCardsNavi)
                                                .Concat(SkillCardsBuff).Concat(SkillCardsRecovery)
                                                .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                itemsFullList = Accessories.Concat(armours).Concat(Consumables).Concat(KeyItemsUnused)
                                          .Concat(materials).Concat(skillCards).Concat(weapons)
                                          .Concat(Reserved)
                                          .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);
            }
        }

        #endregion ItemsFullList

        #region PersonasFullList

        internal static Dictionary<int, Tuple<string, string>> personasFullList;

        internal static Tuple<string, string> GetPersonaData(int id)
        {
            InitializePersonasFullList();

            return personasFullList.ContainsKey(id) ? personasFullList[id] : null;
        }

        private static void InitializePersonasFullList()
        {
            if (personasFullList == null)
            {
                personasFullList = new Dictionary<int, Tuple<string, string>>();

                var personas = PersonasChariot.Concat(PersonasDeath).Concat(PersonasDevil).Concat(PersonasEmperor)
                                              .Concat(PersonasEmpress).Concat(PersonasFool).Concat(PersonasFortune)
                                              .Concat(PersonasHangedMan).Concat(PersonasHermit).Concat(PersonasHierophant)
                                              .Concat(PersonasJudgement).Concat(PersonasJustice).Concat(PersonasLovers)
                                              .Concat(PersonasMagician).Concat(PersonasMoon).Concat(PersonasPriestess)
                                              .Concat(PersonasStar).Concat(PersonasStrength).Concat(PersonasSun)
                                              .Concat(PersonasTemperance).Concat(PersonasTower).Concat(PersonasWorld)
                                              .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                var skills = SkillsMagic.Concat(SkillsDebuff).Concat(SkillsBash).Concat(SkillsCut)
                                        .Concat(SkillsStab).Concat(SkillsAlmighty).Concat(SkillsNavi)
                                        .Concat(SkillsBuff).Concat(SkillsRecovery)
                                        .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);

                personasFullList = personas.Concat(skills)
                                           .GroupBy(x => x.Key).ToDictionary(x => x.Key, x => x.FirstOrDefault().Value);
            }
        }

        #endregion PersonasFullList

        #endregion FullLists

        #region Reserved

        internal static Dictionary<int, Tuple<string, string>> Reserved = new Dictionary<int, Tuple<string, string>>()
        {
            { 0x2FD, new Tuple<string, string>("Armband", "Fuuka's weapon") },
            { 0x2FE, new Tuple<string, string>("Mic", "Rise's weapon") },
            { 0x36D, new Tuple<string, string>("Silk Blouse", "Fuuka's armour") },
            { 0x36E, new Tuple<string, string>("Lace Camisole", "Rise's armour") },
        };

        #endregion Reserved

        #region Enepedia

        // 0x809B-0x8325 Enepedia
        internal static byte[] Enepedia = new byte[]
        {
            0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF,
            0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00,
            0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF,
            0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF,
            0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF,
            0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00,
            0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF,
            0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0xBB, 0xFF, 0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0x00, 0x00, 0xBB, 0xFF,
            0x00, 0x00, 0x00, 0x00, 0xBB, 0xFF, 0xBB, 0xFF
        };

        #endregion Enepedia

        #region Accessories

        internal static Dictionary<int, Tuple<string, string>> Accessories = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x401, new Tuple<string, string>("Vigor Fob", "Max HP +20%") },
            { 0x403, new Tuple<string, string>("Life Fob", "Max HP +30%") },
            { 0x406, new Tuple<string, string>("Plum Potpourri", "Max SP +20") },
            { 0x408, new Tuple<string, string>("Soul Potpourri", "Max SP +40") },
            { 0x40B, new Tuple<string, string>("Headband", "St +1") },
            { 0x40D, new Tuple<string, string>("Power Tasuki", "St +3") },
            { 0x40F, new Tuple<string, string>("Festival Drum", "St +5") },
            { 0x410, new Tuple<string, string>("Lizard Charm", "Ma +1") },
            { 0x412, new Tuple<string, string>("Bat Charm", "Ma +3") },
            { 0x414, new Tuple<string, string>("Witch Charm", "Ma +5") },
            { 0x415, new Tuple<string, string>("Leather Choker", "En +1") },
            { 0x417, new Tuple<string, string>("Defense Amulet", "En +3") },
            { 0x419, new Tuple<string, string>("Guard Rosary", "En +5") },
            { 0x41A, new Tuple<string, string>("Feather Strap", "Ag +1") },
            { 0x41C, new Tuple<string, string>("Flight Strap", "Ag +3") },
            { 0x41E, new Tuple<string, string>("Pegasus Strap", "Ag +5") },
            { 0x41F, new Tuple<string, string>("Four-Leaf Clover", "Lu +1") },
            { 0x421, new Tuple<string, string>("Silver Spoon", "Lu +3") },
            { 0x423, new Tuple<string, string>("Wooden Ebisu", "Lu +5") },
            { 0x424, new Tuple<string, string>("Monocle", "Raise hit rate") },
            { 0x426, new Tuple<string, string>("Aim Goggles", "Greatly raise hit rate") },
            { 0x429, new Tuple<string, string>("Alpha Drive", "Raise critical hit rate") },
            { 0x42B, new Tuple<string, string>("Delta Drive", "Greatly raise critical hit rate") },
            { 0x42E, new Tuple<string, string>("First Choker", "Raise critical hit rate") },
            { 0x430, new Tuple<string, string>("Advance Choker", "Raise movement speed") },
            { 0x433, new Tuple<string, string>("Eclipse Glasses", "Reduce chance of Blind") },
            { 0x435, new Tuple<string, string>("Blind Mask", "Prevent Blind") },
            { 0x438, new Tuple<string, string>("Antiseptic", "Reduce chance of Poison") },
            { 0x43A, new Tuple<string, string>("Disinfectant", "Prevent Poison") },
            { 0x43D, new Tuple<string, string>("Mobile Ankh", "Reduce chance of Paralysis") },
            { 0x43F, new Tuple<string, string>("Freedom Ankh", "Prevent Paralysis") },
            { 0x442, new Tuple<string, string>("Walking Mirror", "Reduce chance of Sleep") },
            { 0x444, new Tuple<string, string>("Sleepless Mirror", "Prevent Sleep") },
            { 0x447, new Tuple<string, string>("Panic Hat", "Reduce chance of Panic") },
            { 0x449, new Tuple<string, string>("Calm Buddha", "Prevent Panic") },
            { 0x44C, new Tuple<string, string>("Purging Charm", "Reduce chance of Curse") },
            { 0x44D, new Tuple<string, string>("Exorcism Stone", "Prevent Curse") },
            { 0x44F, new Tuple<string, string>("Stone Charm", "Reduce chance of Petrification") },
            { 0x450, new Tuple<string, string>("Stone Amulet", "Prevent Petrification") },
            { 0x458, new Tuple<string, string>("Elbow Pad", "Reduce chance of [St]") },
            { 0x459, new Tuple<string, string>("Arm Guard", "Prevent [St]") },
            { 0x45B, new Tuple<string, string>("Safety Helmet", "Reduce chance of [Ma]") },
            { 0x45C, new Tuple<string, string>("Head Guard", "Prevent [Ma]") },
            { 0x45E, new Tuple<string, string>("Leg Warmer", "Reduce chance of [Ag]") },
            { 0x45F, new Tuple<string, string>("Leg Guard", "Prevent [Ag]") },
            { 0x462, new Tuple<string, string>("Fire Pin", "Reduce Fire damage") },
            { 0x464, new Tuple<string, string>("Blaze Pin", "Greatly reduce Fire damage") },
            { 0x467, new Tuple<string, string>("Ice Pin", "Reduce Ice damage") },
            { 0x469, new Tuple<string, string>("Blizzard Pin", "Greatly reduce Ice damage") },
            { 0x46C, new Tuple<string, string>("Thunder Pin", "Reduce Elec damage") },
            { 0x46E, new Tuple<string, string>("Volt Pin", "Greatly reduce Elec damage") },
            { 0x471, new Tuple<string, string>("Wind Pin", "Reduce Wind damage") },
            { 0x473, new Tuple<string, string>("Storm Pin", "Greatly reduce Wind damage") },
            { 0x475, new Tuple<string, string>("Light Pin", "Reduce chance of instant kill from Light") },
            { 0x477, new Tuple<string, string>("Light Charm", "Greatly reduce chance of instant kill from Light") },
            { 0x47A, new Tuple<string, string>("Dark Pin", "Reduce chance of instant kill from Darkness") },
            { 0x47C, new Tuple<string, string>("Dark Charm", "Greatly reduce chance of instant kill from Darkness") },
            { 0x48E, new Tuple<string, string>("Merit Badge", "Max HP +10, Max SP +10, All stats +1") },
            { 0x490, new Tuple<string, string>("Authority Badge", "Max HP +30, Max SP +30, All stats +3") },
            { 0x492, new Tuple<string, string>("All-Around Badge", "Max HP +50, Max SP +50, All stats +5") },
            { 0x493, new Tuple<string, string>("Almighty Badge", "Max HP +100, Max SP +100, All stats +10") },
            { 0x496, new Tuple<string, string>("Direct Guard", "Reduce Cut, Bash, Stab damage") },
            { 0x497, new Tuple<string, string>("Free Ornament", "Reduce chance of [St], [Ma], [Ag]") },
            { 0x498, new Tuple<string, string>("Overflow Bangle", "Max HP +100, Max SP +40") },
            { 0x499, new Tuple<string, string>("Element Guard", "Reduce Fire, Ice, Elec, Wind damage") },
            { 0x49A, new Tuple<string, string>("Auto Wall", "Automatically draws walls on maps") },
            { 0x49D, new Tuple<string, string>("Growth 1", "Exp +25%, even if not in party") },
            { 0x49E, new Tuple<string, string>("Growth 2", "Exp +50%, even if not in party") },
            { 0x49F, new Tuple<string, string>("Growth 3", "Exp +100%, even if not in party") },
            { 0x4A0, new Tuple<string, string>("Lucky Coin", "Coin that protects from any bullet [Only works in the movies]") },
            { 0x4A1, new Tuple<string, string>("Life Belt", "Max HP +20") },
            { 0x4A2, new Tuple<string, string>("Bracer of Might", "Auto-revive with 1 HP [once]") },
            { 0x4A3, new Tuple<string, string>("Sacrificial Idol", "Auto-revive with full HP [once]") },
            { 0x4A4, new Tuple<string, string>("Rudra Ring", "HP costs halved") },
            { 0x4A5, new Tuple<string, string>("Chakra Ring", "SP costs halved") },
            { 0x4A6, new Tuple<string, string>("St-Ma Reverse", "Swaps St and Ma") },
            { 0x4A7, new Tuple<string, string>("St-En Reverse", "Swaps St and En") },
            { 0x4A8, new Tuple<string, string>("St-Ag Reverse", "Swaps St and Ag") },
            { 0x4A9, new Tuple<string, string>("St-Lu Reverse", "Swaps St and Lu") },
            { 0x4AA, new Tuple<string, string>("Ma-En Reverse", "Swaps Ma and En") },
            { 0x4AB, new Tuple<string, string>("Ma-Ag Reverse", "Swaps Ma and Ag") },
            { 0x4AC, new Tuple<string, string>("Ma-Lu Reverse", "Swaps Ma and Lu") },
            { 0x4AD, new Tuple<string, string>("En-Ag Reverse", "Swaps En and Ag") },
            { 0x4AE, new Tuple<string, string>("En-Lu Reverse", "Swaps En and Lu") },
            { 0x4AF, new Tuple<string, string>("Ag-Lu Reverse", "Swaps Ag and Lu") },
            { 0x4B0, new Tuple<string, string>("Willow Wing", "Raise evasion rate against Fire, Ice, Wind, Elec") },
            { 0x4B1, new Tuple<string, string>("Snowy Wing", "Raise evasion rate against Cut, Bash, Stab") },
            { 0x4B2, new Tuple<string, string>("Enduring Wing", "Raise evasion rate against all attacks") },
            { 0x4B3, new Tuple<string, string>("Blazing Flame", "Medium chance of negating Fire damage") },
            { 0x4B4, new Tuple<string, string>("Frozen Stone", "Medium chance of negating Ice damage") },
            { 0x4B5, new Tuple<string, string>("Storm Ring", "Medium chance of negating Wind damage") },
            { 0x4B6, new Tuple<string, string>("Lightning Gloves", "Medium chance of negating Elec damage") },
            { 0x4B7, new Tuple<string, string>("Radiant Halo", "Medium chance of nulling instakill from Light") },
            { 0x4B8, new Tuple<string, string>("Ring of Darkness", "Medium chance of nulling instakill from Darkness") },
            { 0x4B9, new Tuple<string, string>("Bladed Plume", "Medium chance of negating Cut damage") },
            { 0x4BA, new Tuple<string, string>("Iron Pendant", "Medium chance of negating Stab damage") },
            { 0x4BB, new Tuple<string, string>("Helmet", "Medium chance of negating Bash damage") },
            { 0x4BC, new Tuple<string, string>("Omnipotent Orb", "Prevent all damage except Almighty") },
            { 0x4BD, new Tuple<string, string>("Germanium Ring", "Prevent all status ailments") },
            { 0x4BE, new Tuple<string, string>("Body-Soul Reverse", "Swap Max HP and Max SP") },
            { 0x4BF, new Tuple<string, string>("Handmade Collar", "Max HP +70, Max SP +70, All stats +7, " + Properties.Resources.ZenRei) },
            { 0x4C0, new Tuple<string, string>("Heavy Wristband", "Exp x2, All stats halved") },
            { 0x4C1, new Tuple<string, string>("Pink Choker", "En +3, Lu +3") },
            { 0x4C2, new Tuple<string, string>("Red Hairpin", "St +3, Ma +3") },
            { 0x4C3, new Tuple<string, string>("Purple Headband", "Max HP +15%, Ag +3") },
            { 0x4C4, new Tuple<string, string>("Promise Bracer", "Skill: Holy Blessing") },
            { 0x4C5, new Tuple<string, string>("Spirit Ring", "Skill: Power Charge") },
            { 0x4C6, new Tuple<string, string>("Dragon Belt", "Skill: Dragon Cry") },
        };

        #endregion Accessories

        #region Armours

        #region ArmourAigis

        internal static Dictionary<int, Tuple<string, string>> ArmourAigis = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x370, new Tuple<string, string>("Aigis Armor", "Defense 15, Aigis") },
            { 0x372, new Tuple<string, string>("Aigis Armor v1", "Defense 30, Aigis") },
            { 0x373, new Tuple<string, string>("Aigis Armor v3", "Defense 53, Aigis") },
            { 0x374, new Tuple<string, string>("Aigis Armor v4", "Defense 89, Aigis") },
            { 0x375, new Tuple<string, string>("TD W Plate", "Defense 128, Aigis") },
            { 0x376, new Tuple<string, string>("Aigis Armor v7", "Defense 157, Aigis") },
            { 0x377, new Tuple<string, string>("Aigis Armor v8", "Defense 193, Aigis") },
            { 0x378, new Tuple<string, string>("Aigis Armor v9", "Defense 226, Aigis") },
            { 0x379, new Tuple<string, string>("TD Full Body", "Defense 250, Aigis") },
            { 0x37A, new Tuple<string, string>("Aigis Armor v0", "Defense 288, Aigis") },
            { 0x37E, new Tuple<string, string>("TD Full Shell", "Defense 25, Aigis") },
            { 0x37F, new Tuple<string, string>("Aigis Armor v2", "Defense 35, Aigis") },
            { 0x380, new Tuple<string, string>("TD Plate", "Defense 71, Aigis") },
            { 0x381, new Tuple<string, string>("Aigis Armor v5", "Defense 110, Aigis") },
            { 0x382, new Tuple<string, string>("Aigis Armor v6", "Defense 144, Aigis") },
            { 0x383, new Tuple<string, string>("TD C Harness", "Defense 173, Aigis") },
            { 0x384, new Tuple<string, string>("TD Scale", "Defense 210, Aigis") },
            { 0x385, new Tuple<string, string>("Aigis Armor v10", "Defense 242, Aigis") },
            { 0x386, new Tuple<string, string>("Papillon Method", "Defense 310, Aigis") },
            { 0x387, new Tuple<string, string>("Aigis Armor v100", "Defense 350, En x4, Ag x4, Aigis") },
            { 0x388, new Tuple<string, string>("Fearsome V-Suit", "Defense 380, St x2, Ma x2, En x2, Ag x2, Lu x2, Aigis") },
            { 0x389, new Tuple<string, string>("Heavy V-Suit", "Defense 16, Exp x2, All stats halved, Aigis") },
            { 0x38A, new Tuple<string, string>("Anti-Seal Armor", "Defense 183, [Ma] x2, [St] x2, [Ag] x2, Aigis") },
            { 0x38B, new Tuple<string, string>("Element Body", "Defense 260, Fire x1, Ice x1, Elec x1, Wind x1, Aigis") },
        };

        #endregion ArmourAigis

        #region ArmourFemales

        internal static Dictionary<int, Tuple<string, string>> ArmourFemales = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x340, new Tuple<string, string>("Green Sweats", "Defense 14, Female") },
            { 0x341, new Tuple<string, string>("Lace Blouse", "Defense 13, Female") },
            { 0x342, new Tuple<string, string>("Long Bolero", "Defense 13, Female") },
            { 0x343, new Tuple<string, string>("Ruffled Blouse", "Defense 15, Female") },
            { 0x346, new Tuple<string, string>("Combat Dress", "Defense 20, Female") },
            { 0x347, new Tuple<string, string>("All-Purpose Apron", "Defense 35, Female") },
            { 0x348, new Tuple<string, string>("Battle Camisole", "Defense 71, Female") },
            { 0x349, new Tuple<string, string>("Battle Leotard", "Defense 110, Female") },
            { 0x34A, new Tuple<string, string>("Wolf Tunic", "Defense 144, Female") },
            { 0x34B, new Tuple<string, string>("Capital Robe", "Defense 173, Female") },
            { 0x34C, new Tuple<string, string>("Invincible Mini", "Defense 210, Female") },
            { 0x34D, new Tuple<string, string>("Spiked Bra", "Defense 242, Female") },
            { 0x34E, new Tuple<string, string>("Emery Meisen", "Defense 264, Female") },
            { 0x34F, new Tuple<string, string>("Haten Robe", "Defense 310, Female") },
            { 0x350, new Tuple<string, string>("Steel Panier", "Defense 30, Female") },
            { 0x351, new Tuple<string, string>("Charm Robe", "Defense 53, Female") },
            { 0x352, new Tuple<string, string>("Hard Bolero", "Defense 89, Female") },
            { 0x353, new Tuple<string, string>("Armada Bustier", "Defense 128, Female") },
            { 0x354, new Tuple<string, string>("Action Vest", "Defense 157, Female") },
            { 0x355, new Tuple<string, string>("Breeze Tutu", "Defense 193, Female") },
            { 0x356, new Tuple<string, string>("Rune Dress", "Defense 226, Female") },
            { 0x357, new Tuple<string, string>("Elint Duffle", "Defense 250, Female") },
            { 0x358, new Tuple<string, string>("Eternal Plate", "Defense 288, Female") },
            { 0x359, new Tuple<string, string>("Solar Robe", "Defense 350, SP x4, Ag x4, Chie") },
            { 0x35A, new Tuple<string, string>("Ruby Kimono", "Defense 350, HP x4, En x4, Yukiko") },
            { 0x35B, new Tuple<string, string>("Black Dress", "Defense 350, Ma x3, Lu x5, Naoto") },
            { 0x35C, new Tuple<string, string>("Sakura Robe", "Defense 350, En x4, Lu x4, Yukari") },
            { 0x35D, new Tuple<string, string>("Empress Fur", "Defense 350, SP x4, En x4, Mitsuru") },
            { 0x35E, new Tuple<string, string>("Fearsome Gown", "Defense 380, St x2, Ma x2, En x2, Ag x2, Lu x2, Female") },
            { 0x35F, new Tuple<string, string>("Witch Dress", "Defense 150, SP x1, Ma x2, Female") },
            { 0x360, new Tuple<string, string>("Metro Armor", "Defense 290, Ma x3, En x2, Female") },
            { 0x361, new Tuple<string, string>("Mystic Veil", "Defense 183, [Ma] x2, [St] x2, [Ag] x2, Female") },
            { 0x362, new Tuple<string, string>("Element Dress", "Defense 260, Fire x1, Ice x1, Elec x1, Wind x1, Female") },
        };

        #endregion ArmourFemales

        #region ArmourKoromaru

        internal static Dictionary<int, Tuple<string, string>> ArmourKoromaru = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x3A0, new Tuple<string, string>("Silk Dog-Suit", "Defense 15, Koromaru") },
            { 0x3A1, new Tuple<string, string>("Leather Dog-Suit", "Defense 25, Koromaru") },
            { 0x3A2, new Tuple<string, string>("Metal Dog-Suit", "Defense 35, Koromaru") },
            { 0x3A3, new Tuple<string, string>("Light Dog-Suit", "Defense 71, Koromaru") },
            { 0x3A4, new Tuple<string, string>("Titanium Dog-Suit", "Defense 110, Koromaru") },
            { 0x3A5, new Tuple<string, string>("Cross Dog-Suit", "Defense 144, Koromaru") },
            { 0x3A6, new Tuple<string, string>("Power Dog-Suit", "Defense 173, Koromaru") },
            { 0x3A7, new Tuple<string, string>("Dark Dog-Suit", "Defense 210, Koromaru") },
            { 0x3A8, new Tuple<string, string>("Luna Dog-Suit", "Defense 242, Koromaru") },
            { 0x3A9, new Tuple<string, string>("Cosmos Dog-Suit", "Defense 264, Koromaru") },
            { 0x3AA, new Tuple<string, string>("Dotted Dog-Suit", "Defense 30, Koromaru") },
            { 0x3AB, new Tuple<string, string>("Heart Dog-Suit", "Defense 53, Koromaru") },
            { 0x3AC, new Tuple<string, string>("Pearl Dog-Suit", "Defense 89, Koromaru") },
            { 0x3AD, new Tuple<string, string>("Sky Dog-Suit", "Defense 128, Koromaru") },
            { 0x3AE, new Tuple<string, string>("Marine Dog-Suit", "Defense 157, Koromaru") },
            { 0x3AF, new Tuple<string, string>("Star Dog-Suit", "Defense 193, Koromaru") },
            { 0x3B0, new Tuple<string, string>("Denim Dog-Suit", "Defense 226, Koromaru") },
            { 0x3B1, new Tuple<string, string>("Solar Dog-Suit", "Defense 250, Koromaru") },
            { 0x3B2, new Tuple<string, string>("Heavy Dog-Suit", "Defense 288, Koromaru") },
            { 0x3B3, new Tuple<string, string>("Aura Dog-Suit", "Defense 310, Koromaru") },
            { 0x3B4, new Tuple<string, string>("Obsidian Dog-Suit", "Defense 350, Ma x3, En x5, Koromaru") },
            { 0x3B5, new Tuple<string, string>("Fearsome Dog-Suit", "Defense 380, St x2, Ma x2, En x2, Ag x2, Lu x2, Koromaru") },
            { 0x3B6, new Tuple<string, string>("Heavy Dog-Gi", "Defense 15, Exp x2, All stats halved, Koromaru") },
            { 0x3B7, new Tuple<string, string>("Guardian Dog-Suit", "Defense 138, Cut x2, Bash x2, Stab x2, Koromaru") },
            { 0x3B8, new Tuple<string, string>("Mint Dog-Suit", "Defense 236, Sleep x3, Koromaru") },
        };

        #endregion ArmourKoromaru

        #region ArmourMales

        internal static Dictionary<int, Tuple<string, string>> ArmourMales = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x310, new Tuple<string, string>("T-Shirt", "Defense 16, Male") },
            { 0x311, new Tuple<string, string>("Plain Shirt", "Defense 15, Male") },
            { 0x312, new Tuple<string, string>("Long T-Shirt", "Defense 15, Male") },
            { 0x313, new Tuple<string, string>("Skull T-Shirt", "Defense 17, Male") },
            { 0x314, new Tuple<string, string>("Pretty Suit", "Defense 16, Male") },
            { 0x315, new Tuple<string, string>("Dress Shirt", "Defense 15, Male") },
            { 0x316, new Tuple<string, string>("Fleece Vest", "Defense 16, Male") },
            { 0x317, new Tuple<string, string>("Zip Hoodie", "Defense 14, Male") },
            { 0x318, new Tuple<string, string>("Long Pea Coat", "Defense 17, Male") },
            { 0x31B, new Tuple<string, string>("Lion Happi", "Defense 30, Male") },
            { 0x31C, new Tuple<string, string>("Tigerhide Belt", "Defense 53, Male") },
            { 0x31D, new Tuple<string, string>("Aketon", "Defense 89, Male") },
            { 0x31E, new Tuple<string, string>("Purple Suit", "Defense 128, Male") },
            { 0x31F, new Tuple<string, string>("Hero´s Knickers", "Defense 157, Male") },
            { 0x320, new Tuple<string, string>("Plate Mail", "Defense 193, Male") },
            { 0x321, new Tuple<string, string>("Stealth Shirt", "Defense 226, Male") },
            { 0x322, new Tuple<string, string>("Dragon Scale", "Defense 250, Male") },
            { 0x323, new Tuple<string, string>("Full Jin-Baori", "Defense 288, Male") },
            { 0x324, new Tuple<string, string>("Panic Suit", "Defense 99, Panic x3, Male") },
            { 0x325, new Tuple<string, string>("Manly Fundoshi", "Defense 200, Light x2, Darkness x2, Male/" + Properties.Resources.ZenRei) },
            { 0x326, new Tuple<string, string>("Manly Jinbei", "Defense 20, Male") },
            { 0x327, new Tuple<string, string>("Wire Jacket", "Defense 35, Male") },
            { 0x328, new Tuple<string, string>("Doumaru", "Defense 71, Male") },
            { 0x329, new Tuple<string, string>("Desperate Plate", "Defense 110, Male") },
            { 0x32A, new Tuple<string, string>("Blackstone Mail", "Defense 144, Male") },
            { 0x32B, new Tuple<string, string>("Knight Scale", "Defense 173, Male") },
            { 0x32C, new Tuple<string, string>("Passion Sweats", "Defense 210, Male") },
            { 0x32D, new Tuple<string, string>("Kuroito-odoshi", "Defense 242, Male") },
            { 0x32E, new Tuple<string, string>("Carbon Jacket", "Defense 264, Male") },
            { 0x32F, new Tuple<string, string>("Hallowed Plate", "Defense 310, Male") },
            { 0x330, new Tuple<string, string>("King Collar", "Defense 350, En x3, Ag x5, /P4/") },
            { 0x331, new Tuple<string, string>("Monarch Jacket", "Defense 350, En x2, Ag x6, /P3/") },
            { 0x332, new Tuple<string, string>("Fuuma Outfit", "Defense 350, SP x5, Lu x3, Yosuke") },
            { 0x333, new Tuple<string, string>("Gold Tokkoufuku", "Defense 350, En x5, Lu x3, Kanji") },
            { 0x334, new Tuple<string, string>("Magnetic Bearsuit", "Defense 350, HP x4, En x4, Teddie") },
            { 0x335, new Tuple<string, string>("Brave Scale", "Defense 350, HP x3, En x5, Junpei") },
            { 0x336, new Tuple<string, string>("Sirius Suit", "Defense 350, SP x3, Ag x5, Akihiko") },
            { 0x337, new Tuple<string, string>("Undefeated Hoodie", "Defense 350, En x4, Lu x4, Ken") },
            { 0x338, new Tuple<string, string>("Moon Haori", "Defense 350, HP x4, SP x4, Shinjiro") },
            { 0x339, new Tuple<string, string>("Fearsome Armor", "Defense 380, St x2, Ma x2, En x2, Ag x2, Lu x2, Male") },
            { 0x33A, new Tuple<string, string>("Charming Getup", "Defense 80, St x2, Ag x1, Male") },
            { 0x33B, new Tuple<string, string>("Sizzlin Happi", "Defense 218, En x2, Lu x3, Male") },
            { 0x33C, new Tuple<string, string>("Quartz Mail", "Defense 270, HP x3, St x3, Male") },
            { 0x33D, new Tuple<string, string>("Gear Armor", "Defense 310, SP x2, Ag x2, Male") },
            { 0x33E, new Tuple<string, string>("Heavy Armor", "Defense 138, Cut x2, Bash x2, Stab x2, Male") },
            { 0x33F, new Tuple<string, string>("Stone Armor", "Defense 274, Petrify x3, Male") },
        };

        #endregion ArmourMales

        #region ArmourUnisex

        internal static Dictionary<int, Tuple<string, string>> ArmourUnisex = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x301, new Tuple<string, string>("Gothic Shirt", "Defense 13, Unisex") },
            { 0x302, new Tuple<string, string>("Chain Mail ", "Defense 18, HP x1, Unisex") },
            { 0x303, new Tuple<string, string>("Rash Guard ", "Defense 40, SP x1, Unisex") },
            { 0x304, new Tuple<string, string>("Kevlar Vest", "Defense 95, En x2, Unisex") },
            { 0x305, new Tuple<string, string>("Metal Protector", "Defense 150, En x3, Unisex") },
            { 0x306, new Tuple<string, string>("Hard Armor", "Defense 200, HP x3, Unisex") },
            { 0x307, new Tuple<string, string>("Kaiser Armor", "Defense 245, SP x2, Unisex") },
            { 0x308, new Tuple<string, string>("Talisman Cape", "Defense 300, Ag x3, Unisex") },
            { 0x309, new Tuple<string, string>("Energy Armor", "Defense 1, SP x10, Unisex") },
            { 0x30A, new Tuple<string, string>("Ring Mail", "Defense 25, HP x2, Ag x1, Unisex") },
            { 0x30B, new Tuple<string, string>("Amenity Suit", "Defense 80, Ma x2, Unisex") },
            { 0x30C, new Tuple<string, string>("Survival Guard", "Defense 135, HP x2, En x1, Unisex") },
            { 0x30D, new Tuple<string, string>("Metal Jacket", "Defense 180, Cut x1, Bash x1, Stab x1, Unisex") },
            { 0x30E, new Tuple<string, string>("Papal Robe", "Defense 280, SP x1, Ma x2, Unisex") },
            { 0x30F, new Tuple<string, string>("Heavy Gi", "Defense 15, Exp x2, All stats halved, Unisex") },
        };

        #endregion ArmourUnisex

        #region ArmourZenrei

        internal static Dictionary<int, Tuple<string, string>> ArmourZenRei = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x3D0, new Tuple<string, string>("Twin Tweeds", "Defense 16, " + Properties.Resources.ZenRei) },
            { 0x3D1, new Tuple<string, string>("Twin Guards", "Defense 30, " + Properties.Resources.ZenRei) },
            { 0x3D2, new Tuple<string, string>("Twin Armor", "Defense 53, " + Properties.Resources.ZenRei) },
            { 0x3D3, new Tuple<string, string>("Twin Mail", "Defense 89, " + Properties.Resources.ZenRei) },
            { 0x3D4, new Tuple<string, string>("Twin Tunics", "Defense 128, " + Properties.Resources.ZenRei) },
            { 0x3D5, new Tuple<string, string>("Twin Robes", "Defense 157, " + Properties.Resources.ZenRei) },
            { 0x3D6, new Tuple<string, string>("Twin Sweats", "Defense 193, " + Properties.Resources.ZenRei) },
            { 0x3D7, new Tuple<string, string>("Twin Chain", "Defense 226, " + Properties.Resources.ZenRei) },
            { 0x3D8, new Tuple<string, string>("Black Robe", "Defense 250, " + Properties.Resources.ZenRei) },
            { 0x3D9, new Tuple<string, string>("Abyss Armor", "Defense 288, " + Properties.Resources.ZenRei) },
            { 0x3DA, new Tuple<string, string>("Twin Hauberks", "Defense 25, " + Properties.Resources.ZenRei) },
            { 0x3DB, new Tuple<string, string>("Twin Vests", "Defense 35, " + Properties.Resources.ZenRei) },
            { 0x3DC, new Tuple<string, string>("Twin Shirts", "Defense 71, " + Properties.Resources.ZenRei) },
            { 0x3DD, new Tuple<string, string>("Twin Plates", "Defense 110, " + Properties.Resources.ZenRei) },
            { 0x3DE, new Tuple<string, string>("Twin Cuirasses", "Defense 144, " + Properties.Resources.ZenRei) },
            { 0x3DF, new Tuple<string, string>("Twin Suits", "Defense 173, " + Properties.Resources.ZenRei) },
            { 0x3E0, new Tuple<string, string>("Twin Gis", "Defense 210, " + Properties.Resources.ZenRei) },
            { 0x3E1, new Tuple<string, string>("Twin Capes", "Defense 242, " + Properties.Resources.ZenRei) },
            { 0x3E2, new Tuple<string, string>("Chaos Robes", "Defense 264, " + Properties.Resources.ZenRei) },
            { 0x3E3, new Tuple<string, string>("Phantom Mail", "Defense 310, " + Properties.Resources.ZenRei) },
            { 0x3E4, new Tuple<string, string>("Twin Jackets", "Defense 32, HP x1, SP x1, " + Properties.Resources.ZenRei) },
            { 0x3E5, new Tuple<string, string>("Toughness Shirt", "Defense 1, HP x8, " + Properties.Resources.ZenRei) },
            { 0x3E6, new Tuple<string, string>("Twin Sheets", "Defense 119, Curse x3, " + Properties.Resources.ZenRei) },
            { 0x3E7, new Tuple<string, string>("Kairos Scope", "Defense 350, Ma x3, Lu x5, " + Properties.Resources.ZenRei) },
            { 0x3E8, new Tuple<string, string>("Fearsome Armor", "Defense 380, St x2, Ma x2, En x2, Ag x2, Lu x2, " + Properties.Resources.ZenRei) },
            { 0x3E9, new Tuple<string, string>("Heavy Cloak", "Defense 16, Exp x2, All stats halved, " + Properties.Resources.ZenRei) },
            { 0x3EA, new Tuple<string, string>("Panic Robe", "Defense 99, Panic x3, " + Properties.Resources.ZenRei) },
            { 0x3EB, new Tuple<string, string>("Stone Robe", "Defense 274, Petrify x3, " + Properties.Resources.ZenRei) },
            { 0x3EC, new Tuple<string, string>("Poison Robe", "Defense 85, Poison x3, " + Properties.Resources.ZenRei) },
            { 0x3ED, new Tuple<string, string>("Paralysis Robe", "Defense 163, Paralysis x3, " + Properties.Resources.ZenRei) },
        };

        #endregion ArmourZenrei

        #endregion Armours

        #region Consumables

        internal static Dictionary<int, Tuple<string, string>> Consumables = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x501, new Tuple<string, string>("Medicine", "Restore 50 HP (1 Ally)") },
            { 0x502, new Tuple<string, string>("Ointment", "Restore 100 HP (1 Ally)") },
            { 0x503, new Tuple<string, string>("Antibiotic Gel", "Restore 200 HP (1 Ally)") },
            { 0x504, new Tuple<string, string>("Bead", "Restore all HP (1 Ally)") },
            { 0x505, new Tuple<string, string>("Soul Drop", "Restore 30 SP (1 Ally)") },
            { 0x506, new Tuple<string, string>("Snuff Soul", "Restore 60 SP (1 Ally)") },
            { 0x507, new Tuple<string, string>("Chewing Soul", "Restore 100 SP (1 Ally)") },
            { 0x508, new Tuple<string, string>("Soul Food", "Restore all SP (1 Ally)") },
            { 0x509, new Tuple<string, string>("Soma Droplet", "Restore 100 HP and 25 SP (1 Ally)") },
            { 0x50A, new Tuple<string, string>("Soma Tear", "Restore 200 HP and 50 SP (1 Ally)") },
            { 0x50B, new Tuple<string, string>("Soma Jelly", "Restore all HP and SP (1 Ally)") },
            { 0x50C, new Tuple<string, string>("Revival Bead", "Revive and restore half HP") },
            { 0x50D, new Tuple<string, string>("Balm of Life", "Revive and restore all HP") },
            { 0x50E, new Tuple<string, string>("Emergency Kit", "Restore 100 HP (1 Row)") },
            { 0x50F, new Tuple<string, string>("Medical Kit", "Restore 300 HP (1 Row)") },
            { 0x510, new Tuple<string, string>("Mutudi Gem", "Remove all Binds (1 Ally)") },
            { 0x511, new Tuple<string, string>("Patra Gem", "Remove status ailments (1 Ally)") },
            { 0x512, new Tuple<string, string>("Purifying Water", "Remove stat boosts (All Enemies)") },
            { 0x513, new Tuple<string, string>("Purifying Salt", "Remove stat penalties (Party)") },
            { 0x514, new Tuple<string, string>("Bravant", "Raise attack for 3 rounds (1 Ally)") },
            { 0x515, new Tuple<string, string>("Stonard", "Raise defense for 3 rounds (1 Ally)") },
            { 0x519, new Tuple<string, string>("Fire Mist", "Reduce Fire damage for 3 rounds (1 Ally)") },
            { 0x51A, new Tuple<string, string>("Ice Mist", "Reduce Ice damage for 3 rounds (1 Ally)") },
            { 0x51B, new Tuple<string, string>("Elec Mist", "Reduce Elec damage for 3 rounds (1 Ally)") },
            { 0x51C, new Tuple<string, string>("Wind Mist", "Reduce Wind damage for 3 rounds (1 Ally)") },
            { 0x51D, new Tuple<string, string>("Blaze Oil", "Attacks gain Fire for 3 rounds (1 Ally)") },
            { 0x51E, new Tuple<string, string>("Freeze Oil", "Attacks gain Ice for 3 rounds (1 Ally)") },
            { 0x51F, new Tuple<string, string>("Shock Oil", "Attacks gain Elec for 3 rounds (1 Ally)") },
            { 0x520, new Tuple<string, string>("Cyclone Oil", "Attacks gain Wind for 3 rounds (1 Ally)") },
            { 0x521, new Tuple<string, string>("San-zun Tama", "Inflict 100 Fire damage (All Enemies)") },
            { 0x522, new Tuple<string, string>("Scorching Magatama", "Inflict 150 Fire damage (All Enemies)") },
            { 0x523, new Tuple<string, string>("Dry Ice", "Inflict 100 Ice damage (All Enemies)") },
            { 0x524, new Tuple<string, string>("Zero Magatama", "Inflict 150 Ice damage (All Enemies)") },
            { 0x525, new Tuple<string, string>("Tesla Coil", "Inflict 100 Elec damage (All Enemies)") },
            { 0x526, new Tuple<string, string>("Bolt Magatama", "Inflict 150 Elec damage (All Enemies)") },
            { 0x527, new Tuple<string, string>("Yashichi", "Inflict 100 Wind damage (All Enemies)") },
            { 0x528, new Tuple<string, string>("Storm Magatama", "Inflict 150 Wind damage (All Enemies)") },
            { 0x529, new Tuple<string, string>("Segaki Rice", "Medium chance of instakill from Light (1 Enemy)") },
            { 0x52A, new Tuple<string, string>("Curse Paper", "Medium chance of instakill from Darkness (1 Enemy)") },
            { 0x52B, new Tuple<string, string>("Poison Gas", "Medium chance of Poison (All Enemies)") },
            { 0x52C, new Tuple<string, string>("Paralysis Gas", "Medium chance of Paralysis (All Enemies)") },
            { 0x52D, new Tuple<string, string>("Blind Gas", "Medium chance of Blind (All Enemies)") },
            { 0x52E, new Tuple<string, string>("Sleep Gas", "Medium chance of Sleep (All Enemies)") },
            { 0x52F, new Tuple<string, string>("Curse Gas", "Medium chance of Curse (All Enemies)") },
            { 0x530, new Tuple<string, string>("Panic Gas", "Medium chance of Panic (All Enemies)") },
            { 0x531, new Tuple<string, string>("Analysis Scope", "Obtain all materials from Shadows defeated this round") },
            { 0x532, new Tuple<string, string>("Homunculus", "Prevents instakill (Self)") },
            { 0x533, new Tuple<string, string>("Disperse Amulet", "Reduce encounter rate") },
            { 0x534, new Tuple<string, string>("Summon Charm", "Raise encounter rate") },
            { 0x535, new Tuple<string, string>("Goho-M", "Escape from current labyrinth") },
            { 0x536, new Tuple<string, string>("Goba-K", "Return to current floor entrance") },
            { 0x537, new Tuple<string, string>("Winged Sandals", "Prevent floor damage for a few steps") },
            { 0x538, new Tuple<string, string>("Sight Beacon", "Reveal FOE locations") },
            { 0x539, new Tuple<string, string>("Soma", "Restore all HP and SP (Party)") },
            { 0x53A, new Tuple<string, string>("Bead Chain", "Restore all HP (Party)") },
            { 0x53B, new Tuple<string, string>("Goho-M More", "Escape from current labyrinth [Unlimited uses]") },
            { 0x53C, new Tuple<string, string>("Goba-K More", "Return to current floor entrance [Unlimited uses]") },
            { 0x53D, new Tuple<string, string>("Attract Pipe", "Greatly raise encounter rate") },
            { 0x53E, new Tuple<string, string>("Repulse Bell", "Greatly reduce encounter rate") },
            { 0x53F, new Tuple<string, string>("Dawn Charm", "Replenish floor´s Power Spots") },
            { 0x541, new Tuple<string, string>("Heal Stone", "Restore 50 HP (1 Ally) [Unlimited uses]") },
            { 0x542, new Tuple<string, string>("Vanish Ball", "100% chance of escaping battle [Some battles are inescapable]") },
        };

        #endregion Consumables

        #region KeyItems

        internal static Tuple<string, string>[] KeyItems =
        {
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("Blank Card", "Plot event item"),
            new Tuple<string, string>("Blank Card", "Plot event item"),
            new Tuple<string, string>("Blank Card", "Plot event item"),
            new Tuple<string, string>("Watering Can", "Removes the paint on You in Wonderland: Chapter 3's roses"),
            new Tuple<string, string>("North Building Key", "Opens Evil Spirit Club: 1st Story's North Building"),
            new Tuple<string, string>("Practice Building Key", "Opens Evil Spirit Club: 1st Story's Practice Building"),
            new Tuple<string, string>("South Building Key", "Opens Evil Spirit Club: 1st Story's South Building"),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("Red Key Card", "Evil Spirit Club: 4th Story's key"),
            new Tuple<string, string>("Yellow Key Card", "Evil Spirit Club: 4th Story's key"),
            new Tuple<string, string>("Blue Key Card", "Evil Spirit Club: 4th Story's key"),
            new Tuple<string, string>("Torch", "Used to carry Inaba Pride Exhibit's Holy Flame"),
            new Tuple<string, string>("Black Loincloth", "Unlocks Manly Fundoshi Armour, /P4/-side exclusive item"),
            new Tuple<string, string>("Stuffed Animal", "Plot event item"),
            new Tuple<string, string>("Toy Ring", "Plot event item"),
            new Tuple<string, string>("Lock of Hair", "Plot event item"),
            new Tuple<string, string>("Message Card", "Plot event item"),
            new Tuple<string, string>("Leather Notebook", "Unlocks map drawing"),
            new Tuple<string, string>("Small Door Key", "Opens a door in You in Wonderland: Chapter 3"),
            new Tuple<string, string>("Velvet Ticket", "25% of the Compendium completed: 10% discount summoning Personas"),
            new Tuple<string, string>("Velvet Card", "50% of the Compendium completed: 15% discount summoning Personas"),
            new Tuple<string, string>("Velvet Pass", "75% of the Compendium completed: 20% discount summoning Personas"),
            new Tuple<string, string>("Velvet License", "100% of the Compendium completed: 25% discount summoning Personas"),
            new Tuple<string, string>("Blank Card", "Plot event item"),
            new Tuple<string, string>("Blank Card", "Plot event item"),
            new Tuple<string, string>("Lab Storage Key", "Opens Evil Spirit Club: 2nd Story's Lab Storage"),
            new Tuple<string, string>("White Rose", "Labyrinth Part for Clock Hand, found in You in Wonderland: Chapter 3"),
            new Tuple<string, string>("Heart Mark", "Labyrinth Part for Clock Hand, found in Group Date Cafe: Stop 1"),
            new Tuple<string, string>("Syringe", "Labyrinth Part for Clock Hand, found in Evil Spirit Club: 3rd Story (access from the 4th Story)"),
            new Tuple<string, string>("Twisted Bandana", "Labyrinth Part for Clock Hand, found in Inaba Pride Exhibit: Night 2"),
            new Tuple<string, string>("Extra Gear", "Used to get a Delta Drive, deliver it to a broken robot in Clock Tower: 1F, found in Clock Tower: 2F"),
            new Tuple<string, string>("Children´s Book", "Unlocks Promise Bracer, drops after defeating the Queen of Hearts at You in Wonderland"),
            new Tuple<string, string>("Torn Bible", "Unlocks Focus Earring, drops after defeating the Merciful Clergyman at Group Date Cafe"),
            new Tuple<string, string>("Broken Gauge", "Unlocks Spirit Ring, drops after defeating the Kind Doctor at Evil Spirit Club"),
            new Tuple<string, string>("Hand-Sewn Vest", "Unlocks Dragon Belt, drops after defeating the Guardian at Inaba Pride Exhibit"),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("Manila Rope", "Unlocks Bracer of Might, found in Evil Spirit Club: 2nd Story's 100% map completion chest"),
            new Tuple<string, string>("Gas-Wash Bottle", "Unlocks Dawn Charm, found in Clock Tower: 8F's 100% map completion chest"),
            new Tuple<string, string>("Familiar Statue", "Unlocks Growth 1, found in Evil Spirit Club: 1st Story's 100% map completion chest"),
            new Tuple<string, string>("Superficial Statue", "Unlocks Growth 2, found in Inaba Pride Exhibit: Night 3's 100% map completion chest"),
            new Tuple<string, string>("Spiritual Statue", "Unlocks Growth 3, found in Clock Tower: 9F's 100% map completion chest"),
            new Tuple<string, string>("Scratched Lens", "Unlocks Analysis Scope, found in Clock Tower: 1F's 100% map completion chest"),
            new Tuple<string, string>("Summon Bamboo", "Unlocks Attract Pipes, found in Evil Spirit Club: 4th Story's 100% map completion chest"),
            new Tuple<string, string>("Repulse Alloy", "Unlocks Repulse Bell, found in Clock Tower: 3F's 100% map completion chest"),
            new Tuple<string, string>("Yin-Yang Charm", "Unlocks all St, Ma, Ag, Lu stats reversing accessories, found in Evil Spirit Club: 3rd Story's 100% map completion chest"),
            new Tuple<string, string>("Abyss Mirror", "Unlocks Body-Soul Reverse, found in Clock Tower: 2F's 100% map completion chest"),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("White Pompon", "Fulfills the request Defeat the Card Soldier, drops after defeating a Card Soldier FOE"),
            new Tuple<string, string>("Arrow Sample", "Fulfills the request Procuring Arrow Materials, found in Group Date Cafe: Stop 1, requires Aigis"),
            new Tuple<string, string>("Love Potion", "Fulfills the request Obtain a Love Potion, found in Group Date Cafe: Stop 3, requires Teddie"),
            new Tuple<string, string>("Hellish Pacifier", "Fulfills the request Obtain a Hellish Pacifier, found in Evil Spirit Club: 2nd Story"),
            new Tuple<string, string>("Laxative", "Fulfills the request Pharmacy Collection, found in Evil Spirit Club: 3rd Story, requires Akihiko"),
            new Tuple<string, string>("Elizabeth´s Note", "Fulfills the request Convey a Secret Message, found in Evil Spirit Club: 4th Story, requires Aigis"),
            new Tuple<string, string>("Manly Happi", "Fulfills the request Share the Fun of a Festival, found in Inaba Pride Exhibit: Night 2, requires Kanji and Junpei"),
            new Tuple<string, string>("Ominous Key", "Fulfills the request Box Full of Shadows, found in Inaba Pride Exhibit: Night 2"),
            new Tuple<string, string>("Old Watch", "Fulfills the request Open That Treasure Box!, found in Clock Tower: 6F"),
            new Tuple<string, string>("Theodore #66", "Fulfills the request Lost: Prototype Arrow, found in Inaba Pride Exhibit: Night 4"),
            new Tuple<string, string>("Fragile Key", "Fulfills the request The Battle of Wits, obtained when the request is accepted"),
            new Tuple<string, string>("Ice Key", "Fulfills the request Open That Treasure Box!, found in Clock Tower: 1F"),
            new Tuple<string, string>("Broken Chain", "Fulfills the request Defeat the Reaper, found in Clock Tower: 2F and further"),
            new Tuple<string, string>("Yellow Orb", "Fulfills the request Investigate You in Wonderland, found in You in Wonderland: End"),
            new Tuple<string, string>("Pink Orb", "Fulfills the request Investigate the Group Date Cafe, found in Group Date Cafe: The Big Day (End)"),
            new Tuple<string, string>("Black Orb", "Fulfills the request Investigate the Evil Spirit Club, found in Evil Spirit Club: Last Story Story (End)"),
            new Tuple<string, string>("Red Orb", "Fulfills the request Investigate the Inaba Pride Exhibit, found in Inaba Pride Exhibit: Final Night (End)"),
            new Tuple<string, string>("Scarlet Brace", "Allows obtaining Turdak through Fusion, reward for fulfilling the request Time for a Warm-up"),
            new Tuple<string, string>("Rock Dragon Wing", "Allows obtaining Basilisk through Fusion, reward for fulfilling the request Time to Test Your Skills"),
            new Tuple<string, string>("Amenotamu-zake", "Allows obtaining Ouyamatsumi through Fusion, reward for fulfilling the request Time for a Trial"),
            new Tuple<string, string>("Blessed Loincloth", "Allows obtaining Warrior Zeus through Fusion, reward for fulfilling the request Time for the Finale"),
            new Tuple<string, string>("", ""),// Unlock Zeus 2
            new Tuple<string, string>("Adamas Steel", "Crafting material, reward for fulfilling the request The Power of the Wild Card"),
            new Tuple<string, string>("", ""), // Lost Sheep Doll
            new Tuple<string, string>("Pathos Recorder", "Allows listening to Marie's pathos, reward for fulfilling the request Please Escort Us"),
            new Tuple<string, string>("Promotion Ticket", "10% shop discount, reward for fulfilling the request Legendary Medicine"),
            new Tuple<string, string>("Discount Ticket", "10% shop discount, reward for fulfilling the request The Battle of Wits"),
            new Tuple<string, string>("Member Certificate", "10% skill card discount, reward for fulfilling the request Obtain a Fluffy Tail"),
            new Tuple<string, string>("Adamas Bookmark", "Vanity item, reward for fulfilling the request A Family Affair"),
            new Tuple<string, string>("White Flower Pin", "Fulfills the request Defeat the Angel of Love, drops after defeating an Angel of Love FOE"),
            new Tuple<string, string>("", ""), // Mucky Substance
            new Tuple<string, string>("Nice Eyebrow", "Fulfills the request Defeat the Festival Dudes, drops after defeating a Festival Dudes FOE"),
            new Tuple<string, string>("Scapegoat", "Allows reviving /P34/ once per trip, recharges upon returning to school, reward for fulfilling the request Which Mascot Reigns Supreme!?"),
            new Tuple<string, string>("Scapegoat", "Allows reviving /P34/ once per trip, recharges upon returning to school, reward for fulfilling the request Which Mascot Reigns Supreme!?"),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
            new Tuple<string, string>("", ""),
        };

        internal static Dictionary<int, Tuple<string, string>> KeyItemsUnused = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x540, new Tuple<string, string>("Scapegoat", "Prevent Leader from dying on map [Reusable upon return to school]") },
            { 0x98E, new Tuple<string, string>("Shadow Piece", "") },
            { 0x9DB, new Tuple<string, string>("Manila Rope", "") },
            { 0x9DC, new Tuple<string, string>("Gas-Wash Bottle", "") },
            { 0x9DD, new Tuple<string, string>("Familiar Statue", "") },
            { 0x9DE, new Tuple<string, string>("Superficial Statue", "") },
            { 0x9DF, new Tuple<string, string>("Spiritual Statue", "") },
            { 0x9E0, new Tuple<string, string>("Scratched Lens", "") },
            { 0x9E1, new Tuple<string, string>("Summon Bamboo", "") },
            { 0x9E2, new Tuple<string, string>("Repulse Alloy", "") },
            { 0x9E3, new Tuple<string, string>("Yin-Yang Charm", "") },
            { 0x9E4, new Tuple<string, string>("Abyss Mirror", "") },
            { 0x9F0, new Tuple<string, string>("Adamas Steel", "") },
        };

        #endregion KeyItems

        #region Materials

        #region MaterialsDropped

        internal static Dictionary<int, Tuple<string, string>> MaterialsDropped = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x701, new Tuple<string, string>("Fish Piece", "Dropped: Calm Pesce") },
            { 0x702, new Tuple<string, string>("Fish Filet", "Dropped: Calm Pesce") },
            { 0x703, new Tuple<string, string>("Fish Eye", "Dropped: Calm Pesce") },
            { 0x704, new Tuple<string, string>("Lion Piece", "Dropped: Enslaved Beast") },
            { 0x705, new Tuple<string, string>("Lion Iron Ball", "Dropped: Enslaved Beast") },
            { 0x707, new Tuple<string, string>("Desk Piece", "Dropped: Laughing Table") },
            { 0x708, new Tuple<string, string>("Desk Leg", "Dropped: Laughing Table") },
            { 0x70A, new Tuple<string, string>("Table Piece", "Dropped: Sleeping Table") },
            { 0x70B, new Tuple<string, string>("Table Leg", "Dropped: Sleeping Table") },
            { 0x70C, new Tuple<string, string>("Table Silverware", "Dropped: Sleeping Table") },
            { 0x70D, new Tuple<string, string>("Scale Piece", "Dropped: Heat Balance") },
            { 0x70E, new Tuple<string, string>("Scale Dish", "Dropped: Heat Balance") },
            { 0x710, new Tuple<string, string>("Eagle Piece", "Dropped: Jupiter Eagle") },
            { 0x711, new Tuple<string, string>("Eagle Wings", "Dropped: Jupiter Eagle") },
            { 0x713, new Tuple<string, string>("Adhesive Piece", "Dropped: Cowardly Maya") },
            { 0x714, new Tuple<string, string>("Adhesive Hand", "Dropped: Cowardly Maya") },
            { 0x716, new Tuple<string, string>("Tongue Piece", "Dropped: Lying Hablerie") },
            { 0x717, new Tuple<string, string>("Tongue Skin", "Dropped: Lying Hablerie") },
            { 0x719, new Tuple<string, string>("Clapper Piece", "Dropped: Agitating Hablerie") },
            { 0x71A, new Tuple<string, string>("Clapper Skin", "Dropped: Agitating Hablerie") },
            { 0x71C, new Tuple<string, string>("Sword Piece", "Dropped: Justice Sword") },
            { 0x71D, new Tuple<string, string>("Sword Skin", "Dropped: Justice Sword") },
            { 0x71F, new Tuple<string, string>("Bug Piece", "Dropped: Burning Beetle") },
            { 0x720, new Tuple<string, string>("Bug Horn", "Dropped: Burning Beetle") },
            { 0x721, new Tuple<string, string>("Bug Shell", "Dropped: Burning Beetle") },
            { 0x722, new Tuple<string, string>("Wealth Piece", "Dropped: Wealth Hand") },
            { 0x730, new Tuple<string, string>("Dancer Piece", "Dropped: Soul Dancer") },
            { 0x731, new Tuple<string, string>("Dancer Shoes", "Dropped: Soul Dancer") },
            { 0x733, new Tuple<string, string>("Hoofer Piece", "Dropped: Natural Dancer") },
            { 0x734, new Tuple<string, string>("Hoofer Shoes", "Dropped: Natural Dancer") },
            { 0x736, new Tuple<string, string>("Butterfly Piece", "Dropped: Whimsical Papillon") },
            { 0x737, new Tuple<string, string>("Butterfly Wing", "Dropped: Whimsical Papillon") },
            { 0x739, new Tuple<string, string>("Shaman Piece", "Dropped: Wondrous Magus") },
            { 0x73A, new Tuple<string, string>("Shaman Robe", "Dropped: Wondrous Magus") },
            { 0x73B, new Tuple<string, string>("Shaman Ring", "Dropped: Wondrous Magus") },
            { 0x73C, new Tuple<string, string>("Idol Piece", "Dropped: Tranquil Idol") },
            { 0x73D, new Tuple<string, string>("Idol Veil", "Dropped: Tranquil Idol") },
            { 0x73F, new Tuple<string, string>("Madonna Piece", "Dropped: Vehement Idol") },
            { 0x740, new Tuple<string, string>("Madonna Veil", "Dropped: Vehement Idol") },
            { 0x741, new Tuple<string, string>("Madonna Stool", "Dropped: Vehement Idol") },
            { 0x742, new Tuple<string, string>("Madam Piece", "Dropped: Elegant Mother") },
            { 0x743, new Tuple<string, string>("Madam Hat", "Dropped: Elegant Mother") },
            { 0x745, new Tuple<string, string>("Snake Piece", "Dropped: Lustful Snake") },
            { 0x746, new Tuple<string, string>("Snake Ring", "Dropped: Lustful Snake") },
            { 0x747, new Tuple<string, string>("Snake Skin", "Dropped: Lustful Snake") },
            { 0x748, new Tuple<string, string>("Serpent Piece", "Dropped: Amorous Snake") },
            { 0x749, new Tuple<string, string>("Serpent Ring", "Dropped: Amorous Snake") },
            { 0x74A, new Tuple<string, string>("Serpent Scale", "Dropped: Amorous Snake") },
            { 0x74B, new Tuple<string, string>("Angel Piece", "Dropped: Possessive Cupid") },
            { 0x74C, new Tuple<string, string>("Angel Arrow", "Dropped: Possessive Cupid") },
            { 0x74E, new Tuple<string, string>("Eros Piece", "Dropped: Blind Cupid") },
            { 0x74F, new Tuple<string, string>("Eros Arrow", "Dropped: Blind Cupid") },
            { 0x750, new Tuple<string, string>("Eros Wings", "Dropped: Blind Cupid") },
            { 0x751, new Tuple<string, string>("Scripture Piece", "Dropped: Spurious Book") },
            { 0x752, new Tuple<string, string>("Scripture Latch", "Dropped: Spurious Book") },
            { 0x754, new Tuple<string, string>("Rock Piece", "Dropped: Autonomic Basalt") },
            { 0x755, new Tuple<string, string>("Rock Hand", "Dropped: Autonomic Basalt") },
            { 0x756, new Tuple<string, string>("Rock Lump", "Dropped: Autonomic Basalt") },
            { 0x757, new Tuple<string, string>("Boulder Piece", "Dropped: Idle Basalt") },
            { 0x758, new Tuple<string, string>("Boulder Hand", "Dropped: Idle Basalt") },
            { 0x75A, new Tuple<string, string>("Royal Piece", "Dropped: Gorgeous King") },
            { 0x75B, new Tuple<string, string>("Royal Stache", "Dropped: Gorgeous King") },
            { 0x75C, new Tuple<string, string>("Royal Crown", "Dropped: Gorgeous King") },
            { 0x75D, new Tuple<string, string>("Treasure Piece", "Dropped: Treasure Hand") },
            { 0x770, new Tuple<string, string>("Lexy Piece", "Dropped: Protective Lexy") },
            { 0x771, new Tuple<string, string>("Lexy Ribbon", "Dropped: Protective Lexy") },
            { 0x772, new Tuple<string, string>("Lexy Cotton", "Dropped: Protective Lexy") },
            { 0x773, new Tuple<string, string>("Calocy Piece", "Dropped: Earnest Calocy") },
            { 0x774, new Tuple<string, string>("Calocy Glasses", "Dropped: Earnest Calocy") },
            { 0x776, new Tuple<string, string>("Urn Piece", "Dropped: Rainy Pot") },
            { 0x777, new Tuple<string, string>("Urn Hair", "Dropped: Rainy Pot") },
            { 0x779, new Tuple<string, string>("Twins´ Piece", "Dropped: Trance Twins") },
            { 0x77A, new Tuple<string, string>("Twins´ Handcuffs", "Dropped: Trance Twins") },
            { 0x77C, new Tuple<string, string>("Cop Piece", "Dropped: Bribed Fuzz") },
            { 0x77D, new Tuple<string, string>("Cop Badge", "Dropped: Bribed Fuzz") },
            { 0x77F, new Tuple<string, string>("Ghost Piece", "Dropped: Phantom Mage") },
            { 0x780, new Tuple<string, string>("Ghost Lamp", "Dropped: Phantom Mage") },
            { 0x782, new Tuple<string, string>("Phantom Piece", "Dropped: Phantom Master") },
            { 0x783, new Tuple<string, string>("Phantom Lamp", "Dropped: Phantom Master") },
            { 0x785, new Tuple<string, string>("Statue Piece", "Dropped: Change Relic") },
            { 0x786, new Tuple<string, string>("Statue Hinges", "Dropped: Change Relic") },
            { 0x787, new Tuple<string, string>("Statue Blade", "Dropped: Change Relic") },
            { 0x788, new Tuple<string, string>("Crow Piece", "Dropped: Black Raven") },
            { 0x789, new Tuple<string, string>("Crow Feather", "Dropped: Black Raven") },
            { 0x78B, new Tuple<string, string>("Raven Piece", "Dropped: Amenti Raven") },
            { 0x78C, new Tuple<string, string>("Raven Feather", "Dropped: Amenti Raven") },
            { 0x78D, new Tuple<string, string>("Raven Beak", "Dropped: Amenti Raven") },
            { 0x78E, new Tuple<string, string>("Rainy Piece", "Dropped: Rainy Brother 2") },
            { 0x78F, new Tuple<string, string>("Rainy Breath", "Dropped: Rainy Brother 2") },
            { 0x790, new Tuple<string, string>("Rainy Core", "Dropped: Rainy Brother 2") },
            { 0x791, new Tuple<string, string>("Elder Piece", "Dropped: Rainy Brother 1") },
            { 0x792, new Tuple<string, string>("Elder Mist", "Dropped: Rainy Brother 1") },
            { 0x793, new Tuple<string, string>("Elder Core", "Dropped: Rainy Brother 1") },
            { 0x794, new Tuple<string, string>("Sticky Piece", "Dropped: Desirous Maya") },
            { 0x795, new Tuple<string, string>("Sticky Hand", "Dropped: Desirous Maya") },
            { 0x796, new Tuple<string, string>("Sticky Jelly", "Dropped: Desirous Maya") },
            { 0x797, new Tuple<string, string>("Mushy Piece", "Dropped: Devious Maya") },
            { 0x798, new Tuple<string, string>("Mushy Hand", "Dropped: Devious Maya") },
            { 0x79A, new Tuple<string, string>("Monitor Piece", "Dropped: Fate Seeker") },
            { 0x79B, new Tuple<string, string>("Monitor Brow", "Dropped: Fate Seeker") },
            { 0x79C, new Tuple<string, string>("Monitor Eye", "Dropped: Fate Seeker") },
            { 0x79D, new Tuple<string, string>("Supreme Piece", "Dropped: Supreme Hand") },
            { 0x7B0, new Tuple<string, string>("Musha Piece", "Dropped: Rain Leg Musha") },
            { 0x7B1, new Tuple<string, string>("Musha Helm", "Dropped: Rain Leg Musha") },
            { 0x7B3, new Tuple<string, string>("Biceps Piece", "Dropped: Beastly Gigas") },
            { 0x7B4, new Tuple<string, string>("Biceps Mask", "Dropped: Beastly Gigas") },
            { 0x7B6, new Tuple<string, string>("Triceps Piece", "Dropped: Immortal Gigas") },
            { 0x7B7, new Tuple<string, string>("Triceps Mask", "Dropped: Immortal Gigas") },
            { 0x7B9, new Tuple<string, string>("Nyogo Piece", "Dropped: Inviting Nyogo") },
            { 0x7BA, new Tuple<string, string>("Nyogo Kimono", "Dropped: Inviting Nyogo") },
            { 0x7BC, new Tuple<string, string>("Leaf Piece", "Dropped: Valuing Nyogo") },
            { 0x7BD, new Tuple<string, string>("Leaf Kimono", "Dropped: Valuing Nyogo") },
            { 0x7BF, new Tuple<string, string>("Spiral Piece", "Dropped: Happy Gene") },
            { 0x7C0, new Tuple<string, string>("Spiral Spring", "Dropped: Happy Gene") },
            { 0x7C2, new Tuple<string, string>("Tower Piece", "Dropped: Apostate Tower") },
            { 0x7C3, new Tuple<string, string>("Tower Crown", "Dropped: Apostate Tower") },
            { 0x7C4, new Tuple<string, string>("Tower Eye", "Dropped: Apostate Tower") },
            { 0x7C5, new Tuple<string, string>("Okina Piece", "Dropped: Shallow Okina") },
            { 0x7C6, new Tuple<string, string>("Okina Coat", "Dropped: Shallow Okina") },
            { 0x7C7, new Tuple<string, string>("Okina Staff", "Dropped: Shallow Okina") },
            { 0x7C8, new Tuple<string, string>("Sage Piece", "Dropped: Reckless Okina") },
            { 0x7C9, new Tuple<string, string>("Sage Coat", "Dropped: Reckless Okina") },
            { 0x7CB, new Tuple<string, string>("Hanged Piece", "Dropped: Fierce Cyclops") },
            { 0x7CC, new Tuple<string, string>("Hanged Cuffs", "Dropped: Fierce Cyclops") },
            { 0x7CD, new Tuple<string, string>("Hanged Thorn", "Dropped: Fierce Cyclops") },
            { 0x7CE, new Tuple<string, string>("Dice Piece", "Dropped: Iron Dice") },
            { 0x7CF, new Tuple<string, string>("Dice Pip", "Dropped: Iron Dice") },
            { 0x7D1, new Tuple<string, string>("Craps Piece", "Dropped: Platinum Dice") },
            { 0x7D2, new Tuple<string, string>("Craps Lump", "Dropped: Platinum Dice") },
            { 0x7D3, new Tuple<string, string>("Craps Eye", "Dropped: Platinum Dice") },
            { 0x7D4, new Tuple<string, string>("Wheel Piece", "Dropped: Mach Wheel") },
            { 0x7D5, new Tuple<string, string>("Wheel Axle", "Dropped: Mach Wheel") },
            { 0x7D6, new Tuple<string, string>("Wheel Tail", "Dropped: Mach Wheel") },
            { 0x7D7, new Tuple<string, string>("Tire Piece", "Dropped: Battle Wheel") },
            { 0x7D8, new Tuple<string, string>("Tire Axle", "Dropped: Battle Wheel") },
            { 0x7D9, new Tuple<string, string>("Tire Mane", "Dropped: Battle Wheel") },
            { 0x7DA, new Tuple<string, string>("Machine Piece", "Dropped: Indignant Machine") },
            { 0x7DB, new Tuple<string, string>("Machine Mallet", "Dropped: Indignant Machine") },
            { 0x7DD, new Tuple<string, string>("Opulent Piece", "Dropped: Opulent Hand") },
            { 0x7EF, new Tuple<string, string>("Black Loincloth", "Dropped: FOE (Macho Man)") },
            { 0x7F0, new Tuple<string, string>("Bull Piece", "Dropped: Minotaur I") },
            { 0x7F1, new Tuple<string, string>("Bull Chain", "Dropped: Minotaur I") },
            { 0x7F2, new Tuple<string, string>("Bull Horn", "Dropped: Minotaur I") },
            { 0x7F3, new Tuple<string, string>("Talisman Piece", "Dropped: Purple Sigil") },
            { 0x7F4, new Tuple<string, string>("Talisman Cape", "Dropped: Purple Sigil") },
            { 0x7F5, new Tuple<string, string>("Talisman Staff", "Dropped: Purple Sigil") },
            { 0x7F6, new Tuple<string, string>("Castle Piece", "Dropped: King Castle") },
            { 0x7F7, new Tuple<string, string>("Castle Rubble", "Dropped: King Castle") },
            { 0x7F8, new Tuple<string, string>("Castle Cannon", "Dropped: King Castle") },
            { 0x7F9, new Tuple<string, string>("Knight Piece", "Dropped: Intrepid Knight") },
            { 0x7FA, new Tuple<string, string>("Knight Reins", "Dropped: Intrepid Knight") },
            { 0x7FB, new Tuple<string, string>("Knight Lance", "Dropped: Intrepid Knight") },
            { 0x7FC, new Tuple<string, string>("Drive Piece", "Dropped: Slaughter Drive") },
            { 0x7FD, new Tuple<string, string>("Drive Wheel", "Dropped: Slaughter Drive") },
            { 0x7FE, new Tuple<string, string>("Drive Sword", "Dropped: Slaughter Drive") },
            { 0x7FF, new Tuple<string, string>("H Drive Piece", "Dropped: Rampage Drive") },
            { 0x800, new Tuple<string, string>("H Drive Wheel", "Dropped: Rampage Drive") },
            { 0x801, new Tuple<string, string>("H Drive Gear", "Dropped: Rampage Drive") },
            { 0x802, new Tuple<string, string>("Turret Piece", "Dropped: Arcane Turret") },
            { 0x803, new Tuple<string, string>("Turret Cannon", "Dropped: Arcane Turret") },
            { 0x804, new Tuple<string, string>("Turret Belt", "Dropped: Arcane Turret") },
            { 0x805, new Tuple<string, string>("B Horse Piece", "Dropped: Jotun of Power") },
            { 0x806, new Tuple<string, string>("B Horse Cuffs", "Dropped: Jotun of Power") },
            { 0x807, new Tuple<string, string>("B Horse Chain", "Dropped: Jotun of Power") },
            { 0x808, new Tuple<string, string>("W Horse Piece", "Dropped: Jotun of Grief") },
            { 0x809, new Tuple<string, string>("W Horse Cuffs", "Dropped: Jotun of Grief") },
            { 0x80B, new Tuple<string, string>("Ring Piece", "Dropped: Mind Dice") },
            { 0x80C, new Tuple<string, string>("Ring Lump", "Dropped: Mind Dice") },
            { 0x80D, new Tuple<string, string>("Fire Ring Lump", "Dropped: Mind Dice") },
            { 0x80E, new Tuple<string, string>("Red Ring Piece", "Dropped: Curse Dice") },
            { 0x80F, new Tuple<string, string>("Red Ring Lump", "Dropped: Curse Dice") },
            { 0x811, new Tuple<string, string>("R Turret Piece", "Dropped: Scarlet Turret") },
            { 0x812, new Tuple<string, string>("R Cannon", "Dropped: Scarlet Turret") },
            { 0x814, new Tuple<string, string>("Giant Piece", "Dropped: Order Giant") },
            { 0x815, new Tuple<string, string>("Giant Hilt", "Dropped: Order Giant") },
            { 0x816, new Tuple<string, string>("Giant Sword", "Dropped: Order Giant") },
            { 0x817, new Tuple<string, string>("Luxury Piece", "Dropped: Luxury Hand") },
            { 0x82A, new Tuple<string, string>("Torn Card", "Dropped: Card Soldier") },
            { 0x82D, new Tuple<string, string>("Painted Card", "Dropped: Painting Soldier") },
            { 0x830, new Tuple<string, string>("Love Shard", "Dropped: Angel of Love") },
            { 0x833, new Tuple<string, string>("Lust Shard", "Dropped: Beast of Lust") },
            { 0x836, new Tuple<string, string>("Passion Shard", "Dropped: Messenger of Love") },
            { 0x839, new Tuple<string, string>("Romance Shard", "Dropped: God of Romance") },
            { 0x83C, new Tuple<string, string>("Old Pacifier", "Dropped: Cute Baby") },
            { 0x83F, new Tuple<string, string>("Red Clothes", "Dropped: Lovely Doll") },
            { 0x842, new Tuple<string, string>("Blue Clothes", "Dropped: Old Doll") },
            { 0x845, new Tuple<string, string>("Kanroshu", "Dropped: Kowakashu") },
            { 0x848, new Tuple<string, string>("Daiginjo", "Dropped: Kumiashira") },
            { 0x84B, new Tuple<string, string>("Dripping Sweat", "Dropped: Sweaty Guy") },
            { 0x84E, new Tuple<string, string>("Hyottoko Mask", "Dropped: Festival Dudes") },
            { 0x851, new Tuple<string, string>("Speed Mask", "Dropped: Fast Guy") },
            { 0x854, new Tuple<string, string>("Compound Eye", "Dropped: The Watcher") },
            { 0x857, new Tuple<string, string>("Tangled Thread", "Dropped: The Capturer") },
            { 0x85A, new Tuple<string, string>("Death´s Order", "Dropped: The Reaper") },
            { 0x870, new Tuple<string, string>("Children´s Book", "Dropped: Queen of Hearts") },
            { 0x873, new Tuple<string, string>("Torn Bible", "Dropped: Merciful Clergyman") },
            { 0x876, new Tuple<string, string>("Broken Gauge", "Dropped: Kind Doctor") },
            { 0x879, new Tuple<string, string>("Hand-Sewn Vest", "Dropped: Best Friend") },
            { 0x882, new Tuple<string, string>("Bug Gold", "Dropped: Golden Beetle") },
            { 0x885, new Tuple<string, string>("Royal Gold", "Dropped: Engage King") },
            { 0x888, new Tuple<string, string>("Monitor Gold", "Dropped: Soul Seeker") },
            { 0x88B, new Tuple<string, string>("Machine Gold", "Dropped: Guardian Machine") },
        };

        #endregion MaterialsDropped

        #region MaterialsPowerSpot

        internal static Dictionary<int, Tuple<string, string>> MaterialsPowerSpot = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x990, new Tuple<string, string>("Cake Page", "Gather: You in Wonderland (Ch 1)") },
            { 0x991, new Tuple<string, string>("Mushroom Page", "Gather: You in Wonderland (Ch 1)") },
            { 0x992, new Tuple<string, string>("Tart Page", "Gather: You in Wonderland (Ch 1)") },
            { 0x993, new Tuple<string, string>("Turtle Page", "Gather: You in Wonderland (Ch 2)") },
            { 0x994, new Tuple<string, string>("Gryphon Page", "Gather: You in Wonderland (Ch 2)") },
            { 0x995, new Tuple<string, string>("Dormouse Page", "Gather: You in Wonderland (Ch 2)") },
            { 0x996, new Tuple<string, string>("Hatter Page", "Gather: You in Wonderland (Ch 3)") },
            { 0x997, new Tuple<string, string>("Cook Page", "Gather: You in Wonderland (Ch 3)") },
            { 0x998, new Tuple<string, string>("Duchess Page", "Gather: You in Wonderland (Ch 3)") },
            { 0x99C, new Tuple<string, string>("Dried Rose", "Gather: Group Date Cafe (Stop 1)") },
            { 0x99D, new Tuple<string, string>("Dried Orchid", "Gather: Group Date Cafe (Stop 1)") },
            { 0x99E, new Tuple<string, string>("Dried Amaryllis", "Gather: Group Date Cafe (Stop 1)") },
            { 0x99F, new Tuple<string, string>("Chipped Candle", "Gather: Group Date Cafe (Stop 2)") },
            { 0x9A0, new Tuple<string, string>("Chipped Choker", "Gather: Group Date Cafe (Stop 2)") },
            { 0x9A1, new Tuple<string, string>("Chipped Necklace", "Gather: Group Date Cafe (Stop 2)") },
            { 0x9A2, new Tuple<string, string>("Cracked Tiara", "Gather: Group Date Cafe (Stop 3)") },
            { 0x9A3, new Tuple<string, string>("Cracked Earring", "Gather: Group Date Cafe (Stop 3)") },
            { 0x9A4, new Tuple<string, string>("Cracked Heels", "Gather: Group Date Cafe (Stop 3)") },
            { 0x9A5, new Tuple<string, string>("Ripped Veil", "Gather: Group Date Cafe (Stop 4)") },
            { 0x9A6, new Tuple<string, string>("Ripped Shawl", "Gather: Group Date Cafe (Stop 4)") },
            { 0x9A7, new Tuple<string, string>("Ripped Dress", "Gather: Group Date Cafe (Stop 4)") },
            { 0x9AC, new Tuple<string, string>("Note Page", "Gather: Evil Spirit Club (1F)") },
            { 0x9AD, new Tuple<string, string>("Textbook Page", "Gather: Evil Spirit Club (1F)") },
            { 0x9AE, new Tuple<string, string>("Document Page", "Gather: Evil Spirit Club (1F)") },
            { 0x9AF, new Tuple<string, string>("Ripped Schedule", "Gather: Evil Spirit Club (2F)") },
            { 0x9B0, new Tuple<string, string>("Ripped Nametag", "Gather: Evil Spirit Club (2F)") },
            { 0x9B1, new Tuple<string, string>("Ripped Emblem", "Gather: Evil Spirit Club (2F)") },
            { 0x9B2, new Tuple<string, string>("Rusted Syringe", "Gather: Evil Spirit Club (3F)") },
            { 0x9B3, new Tuple<string, string>("Rusted Scalpel", "Gather: Evil Spirit Club (3F)") },
            { 0x9B4, new Tuple<string, string>("Rusted Tweezers", "Gather: Evil Spirit Club (3F)") },
            { 0x9B5, new Tuple<string, string>("Bloody Bandage", "Gather: Evil Spirit Club (4F)") },
            { 0x9B6, new Tuple<string, string>("Bloody Gauze", "Gather: Evil Spirit Club (4F)") },
            { 0x9B7, new Tuple<string, string>("Bloody Chart", "Gather: Evil Spirit Club (4F)") },
            { 0x9BC, new Tuple<string, string>("Agarwood", "Gather: Inaba Pride Exhibit (Night 1)") },
            { 0x9BD, new Tuple<string, string>("Sandalwood", "Gather: Inaba Pride Exhibit (Night 1)") },
            { 0x9BE, new Tuple<string, string>("Kyarako", "Gather: Inaba Pride Exhibit (Night 1)") },
            { 0x9BF, new Tuple<string, string>("Music Sheet 260", "Gather: Inaba Pride Exhibit (Night 2)") },
            { 0x9C0, new Tuple<string, string>("Music Sheet 657", "Gather: Inaba Pride Exhibit (Night 2)") },
            { 0x9C1, new Tuple<string, string>("Music Sheet 581", "Gather: Inaba Pride Exhibit (Night 2)") },
            { 0x9C2, new Tuple<string, string>("Lily", "Gather: Inaba Pride Exhibit (Night 3)") },
            { 0x9C3, new Tuple<string, string>("Carnation", "Gather: Inaba Pride Exhibit (Night 3)") },
            { 0x9C4, new Tuple<string, string>("Chrysanthemum", "Gather: Inaba Pride Exhibit (Night 3)") },
            { 0x9C5, new Tuple<string, string>("White Belt", "Gather: Inaba Pride Exhibit (Night 4)") },
            { 0x9C6, new Tuple<string, string>("White Candle", "Gather: Inaba Pride Exhibit (Night 4)") },
            { 0x9C7, new Tuple<string, string>("White Pot", "Gather: Inaba Pride Exhibit (Night 4)") },
            { 0x9CC, new Tuple<string, string>("Cloudy Crystal", "Gather: Clock Tower (1F, 2F)") },
            { 0x9CD, new Tuple<string, string>("Dirty Crystal", "Gather: Clock Tower (2F, 3F, 4F)") },
            { 0x9CE, new Tuple<string, string>("Cracked Crystal", "Gather: Clock Tower (1F, 2F, 3F, 4F)") },
            { 0x9CF, new Tuple<string, string>("Grey Clockwork", "Gather: Clock Tower (4F, 5F, 6F)") },
            { 0x9D0, new Tuple<string, string>("Silver Clockwork", "Gather: Clock Tower (5F, 6F, 7F)") },
            { 0x9D1, new Tuple<string, string>("Gold Clockwork", "Gather: Clock Tower (4F, 5F, 6F, 7F)") },
            { 0x9D2, new Tuple<string, string>("Still Pendulum", "Gather: Clock Tower (7F, 8F)") },
            { 0x9D3, new Tuple<string, string>("Non-Tuning Fork", "Gather: Clock Tower (8F)") },
            { 0x9D4, new Tuple<string, string>("Stripped Screw", "Gather: Clock Tower (7F, 8F)") },
            { 0x9D5, new Tuple<string, string>("Broken 3rd Gear", "Gather: Clock Tower (9F)") },
            { 0x9D6, new Tuple<string, string>("Broken 2nd Gear", "Gather: Clock Tower (9F)") },
            { 0x9D7, new Tuple<string, string>("Split Gearshaft", "Gather: Clock Tower (9F)") },
            { 0x9F1, new Tuple<string, string>("Labyrinth Parts", "Heart Mark, Syringe, Twisted Bandana, White Rose") },
        };

        #endregion MaterialsPowerSpot

        #region MaterialsSacrifice

        internal static Dictionary<int, Tuple<string, string>> MaterialsSacrifice = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x900, new Tuple<string, string>("Orochi Tail", "Sacrifice: Yamata-no-Orochi") },
            { 0x901, new Tuple<string, string>("Pixie Wing", "Sacrifice: Pixie") },
            { 0x902, new Tuple<string, string>("Genbu Shell", "Sacrifice: Genbu") },
            { 0x903, new Tuple<string, string>("Suzaku Plume", "Sacrifice: Suzaku") },
            { 0x904, new Tuple<string, string>("Black Lantern", "Sacrifice: Pyro Jack") },
            { 0x905, new Tuple<string, string>("Lamp Shard", "Sacrifice: Jinn") },
            { 0x906, new Tuple<string, string>("Byakko Claw", "Sacrifice: Byakko") },
            { 0x907, new Tuple<string, string>("King Snowball", "Sacrifice: King Frost") },
            { 0x908, new Tuple<string, string>("Lust Pheromone", "Sacrifice: Succubus") },
            { 0x909, new Tuple<string, string>("Nue Claw", "Sacrifice: Nue") },
            { 0x90A, new Tuple<string, string>("Skadi´s Shard", "Sacrifice: Skadi") },
            { 0x90B, new Tuple<string, string>("Matador Sword", "Sacrifice: Matador") },
            { 0x90C, new Tuple<string, string>("Seiryu Scale", "Sacrifice: Seiryu") },
            { 0x90D, new Tuple<string, string>("Cybele Hair", "Sacrifice: Cybele") },
            { 0x90E, new Tuple<string, string>("Club Shard", "Sacrifice: Oni") },
            { 0x90F, new Tuple<string, string>("Fearsome Shard", "Sacrifice: Zeus") },
            { 0x910, new Tuple<string, string>("Claw Shard", "Sacrifice: Seth") },
            { 0x911, new Tuple<string, string>("Yoshitsune Shard", "Sacrifice: Yoshitsune") },
            { 0x912, new Tuple<string, string>("Masakado Shard", "Sacrifice: Masakado") },
            { 0x913, new Tuple<string, string>("Asura Bone", "Sacrifice: Asura") },
            { 0x914, new Tuple<string, string>("Vishnu Shard", "Sacrifice: Vishnu") },
            { 0x915, new Tuple<string, string>("Huaguo´s Rock", "Sacrifice: Seiten Taisei") },
            { 0x916, new Tuple<string, string>("Skanda´s Wing", "Sacrifice: Kartikeya") },
            { 0x917, new Tuple<string, string>("Broken Tablet", "Sacrifice: Kingu") },
            { 0x918, new Tuple<string, string>("Opposer´s Wing", "Sacrifice: Satan") },
            { 0x91A, new Tuple<string, string>("Seraph Wing", "Sacrifice: Metatron") },
            { 0x91B, new Tuple<string, string>("Destroyer´s Rage", "Sacrifice: Shiva") },
            { 0x91C, new Tuple<string, string>("Michael Shard", "Sacrifice: Michael") },
            { 0x91D, new Tuple<string, string>("Chi You Shard", "Sacrifice: Chi You") },
            { 0x91E, new Tuple<string, string>("Goddess Obsidian", "Sacrifice: Alilat") },
            { 0x91F, new Tuple<string, string>("Broken Spear", "Sacrifice: Scathach") },
            { 0x920, new Tuple<string, string>("Trickster Karma", "Sacrifice: Loki") },
            { 0x921, new Tuple<string, string>("Fly Shard", "Sacrifice: Beelzebub") },
        };

        #endregion MaterialsSacrifice

        #endregion Materials

        #region Skills

        #region SkillsAlmighty

        internal static Dictionary<int, Tuple<string, string>> SkillsAlmighty = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1033, new Tuple<string, string>("Megido", "A medium Almighty attack, with medium chance of knockdown (All Enemies)") },
            { 0x1034, new Tuple<string, string>("Megidola", "A heavy Almighty attack, with medium chance of knockdown (All Enemies)") },
            { 0x1035, new Tuple<string, string>("Megidolaon", "A severe Almighty attack, with medium chance of knockdown (All Enemies)") },
            { 0x1037, new Tuple<string, string>("Black Viper", "A severe Almighty attack (1 Enemy)") },
            { 0x1038, new Tuple<string, string>("Morning Star", "A severe Almighty attack (All Enemies)") },
            { 0x10AF, new Tuple<string, string>("Photon Edge", "A heavy Almighty attack (1 Enemy)") },
            { 0x10B0, new Tuple<string, string>("Prana", "A severe Almighty attack (All Enemies)") },
            { 0x10C1, new Tuple<string, string>("Shinkuuha", "2 medium Almighty attacks (1 Enemy)") },
            { 0x10C2, new Tuple<string, string>("Resseiha", "1-5 medium Almighty attacks against random targets (All Enemies)") },
            { 0x10C3, new Tuple<string, string>("Pralaya", "A severe Almighty attack, with medium chance of instant kill (All Enemies)") },
            { 0x10C4, new Tuple<string, string>("Shadow Run", "A heavy Almighty attack (All Enemies)") },
            { 0x10C5, new Tuple<string, string>("World´s End", "A severe Almighty attack (All Enemies)") },
            { 0x1125, new Tuple<string, string>("Door of Hades", "3 heavy Almighty attacks, 1 per turn. High chance of instant kill (All Enemies)") },
            { 0x118F, new Tuple<string, string>("Summon Ghost", "A medium Almighty attack (All Enemies) [Requires Circles]") },
            { 0x1190, new Tuple<string, string>("Summon Demon", "A heavy Almighty attack (All Enemies) [Requires Circles]") },
        };

        #endregion SkillsAlmighty

        #region SkillsBash

        internal static Dictionary<int, Tuple<string, string>> SkillsBash = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1081, new Tuple<string, string>("Fang Smash", "A medium Bash attack, lowers target´s attack for 3 turns (1 Enemy)") },
            { 0x1082, new Tuple<string, string>("Kidney Smash", "A medium Bash attack, lowers elemental attack for 3 turns (1 Enemy)") },
            { 0x1091, new Tuple<string, string>("Bolt Strike", "A light Bash + Elec attack (1 Enemy)") },
            { 0x1092, new Tuple<string, string>("Holy Touch", "A heavy Bash attack (1 Enemy)") },
            { 0x1093, new Tuple<string, string>("Revenge Blow", "A Bash attack delayed to next turn, stronger for each hit taken (1 Enemy)") },
            { 0x1095, new Tuple<string, string>("Gigantic Fist", "A medium Bash attack that splashes to either side (1 Enemy)") },
            { 0x10A0, new Tuple<string, string>("Assault Dive", "A medium Bash attack, with medium chance of knockdown (1 Enemy)") },
            { 0x10A1, new Tuple<string, string>("Sleeper Punch", "A light Bash attack, with medium chance of Sleep (1 Enemy)") },
            { 0x10A2, new Tuple<string, string>("Teardrop", "A light Bash attack, lowers target's defense for that turn (1 Enemy)") },
            { 0x10A3, new Tuple<string, string>("Buchikamashi", "A light Bash attack (1 Enemy)") },
            { 0x10A9, new Tuple<string, string>("Skull Cracker", "A light Bash attack, with medium chance of Magic Bind (1 Enemy)") },
            { 0x10AA, new Tuple<string, string>("Fusion Blast", "A light Bash + Fire attack (1 Enemy)") },
            { 0x10AB, new Tuple<string, string>("Heat Wave", "A medium Bash attack that splashes to either side (1 Enemy)") },
            { 0x10AC, new Tuple<string, string>("Light Wave", "A severe Bash attack (1 Enemy)") },
            { 0x10B2, new Tuple<string, string>("God´s Hand", "A severe Bash attack (1 Enemy)") },
            { 0x10B3, new Tuple<string, string>("Angelic Trumpet", "3 heavy Bash attacks against random targets (All Enemies)") },
            { 0x112D, new Tuple<string, string>("Lightning Smash", "A medium Bash + Elec attack (1 Enemy)") },
            { 0x112E, new Tuple<string, string>("Thunder Smash", "A heavy Bash + Elec attack (1 Enemy)") },
            { 0x112F, new Tuple<string, string>("Scorching Blast", "A medium Bash + Fire attack (1 Enemy)") },
            { 0x1130, new Tuple<string, string>("Nuclear Blast", "A heavy Bash + Fire attack (1 Enemy)") },
            { 0x1131, new Tuple<string, string>("Sonic Punch", "A medium Bash attack, with medium chance of Sleep (1 Enemy)") },
            { 0x1132, new Tuple<string, string>("Brain Snake", "A heavy Bash attack, with medium chance of Sleep (1 Enemy)") },
            { 0x1133, new Tuple<string, string>("Moondrop", "A medium Bash attack, lowers target´s defense for that turn (1 Enemy)") },
            { 0x1134, new Tuple<string, string>("Stardrop", "A heavy Bash attack, lowers target´s defense for that turn (1 Enemy)") },
            { 0x1259, new Tuple<string, string>("Demon´s Bash", "Slightly raise Bash attack strength") },
            { 0x125C, new Tuple<string, string>("God´s Bash", "Raise Bash attack strength") },
        };

        #endregion SkillsBash

        #region SkillsBuff

        internal static Dictionary<int, Tuple<string, string>> SkillsBuff = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x10E6, new Tuple<string, string>("Bestial Roar", "Act first and raise physical attack for 3 turns") },
            { 0x10E7, new Tuple<string, string>("Dragon Cry", "Act first and greatly raise physical attack for 3 turns") },
            { 0x10EA, new Tuple<string, string>("Shadow Cloak", "Nullifies next physical attack once. Lasts 3 turns") },
            { 0x10EC, new Tuple<string, string>("Pain-Eater", "Raise defense and chance to be targeted for 3 turns") },
            { 0x10ED, new Tuple<string, string>("Safeguard", "Take attacks in place of an ally, with reduced damage (1 Ally)") },
            { 0x10EE, new Tuple<string, string>("Body Shield", "Take attacks in place of allies, with reduced damage (1 Row)") },
            { 0x10EF, new Tuple<string, string>("Life Wall", "Take attacks in place of allies, with reduced damage (Party)") },
            { 0x10F0, new Tuple<string, string>("True Warden", "Take attacks in place of allies under half HP for 3 turns") },
            { 0x10F1, new Tuple<string, string>("Holy Blessing", "Nullifies all Binds and status ailments 1 time for 3 turns (Party)") },
            { 0x10F2, new Tuple<string, string>("Guiding Sword", "Raise attack for 3 turns (1 Row)") },
            { 0x10F3, new Tuple<string, string>("Guarding Staff", "Raise defense for 3 turns (1 Row)") },
            { 0x10F5, new Tuple<string, string>("Critical Eye", "Raise critical hit rate for 3 turns") },
            { 0x10F7, new Tuple<string, string>("Matarukaja", "Raise attack for 3 turns (Party)") },
            { 0x10F8, new Tuple<string, string>("Marakukaja", "Raise defense for 3 turns (Party)") },
            { 0x10F9, new Tuple<string, string>("Masukukaja", "Raise hit rate and evasion rate for 3 turns (Party)") },
            { 0x10FA, new Tuple<string, string>("Power Charge", "Nearly triple the user´s physical attack for next hit. Lasts 3 turns") },
            { 0x10FB, new Tuple<string, string>("Mind Charge", "Nearly triple the user´s magic attack for next hit. Lasts 3 turns") },
            { 0x1116, new Tuple<string, string>("Fire Wall", "Raise Fire resistance for 3 turns (Party)") },
            { 0x1117, new Tuple<string, string>("Wind Wall", "Raise Wind resistance for 3 turns (Party)") },
            { 0x1118, new Tuple<string, string>("Ice Wall", "Raise Ice resistance for 3 turns (Party)") },
            { 0x1119, new Tuple<string, string>("Elec Wall", "Raise Elec resistance for 3 turns (Party)") },
            { 0x111A, new Tuple<string, string>("Light Wall", "Raise Light resistance for 3 turns (Party)") },
            { 0x111B, new Tuple<string, string>("Dark Wall", "Raise Darkness resistance for 3 turns (Party)") },
            { 0x111C, new Tuple<string, string>("Tarukaja", "Raise attack for 3 turns (1 Ally)") },
            { 0x111D, new Tuple<string, string>("Rakukaja", "Raise defense for 3 turns (1 Ally)") },
            { 0x111E, new Tuple<string, string>("Sukukaja", "Raise hit rate and evasion rate for 3 turns (1 Ally)") },
            { 0x1123, new Tuple<string, string>("Heat Riser", "Greatly raise attack, defense, hit and evasion for 3 turns (1 Ally)") },
            { 0x1124, new Tuple<string, string>("Mind´s Eye", "Raise critical hit rate for 3 turns (Party)") },
            { 0x1129, new Tuple<string, string>("Lunar Blessing", "Nullifies 1 attack per turn, for 3 turns (Party)") },
            { 0x1191, new Tuple<string, string>("Shura Tensei", "Greatly lowers HP each turn but greatly raises attack while active") },
            { 0x1192, new Tuple<string, string>("Shura Revert", "Ends Shura Tensei, restoring moderate HP") },
            { 0x1193, new Tuple<string, string>("Shura Instincts", "High chance of Shura Tensei when battle starts [Attack up, HP down each turn]") },
            { 0x119D, new Tuple<string, string>("Death Counter", "Counter-attack whenever user´s row is attacked, for 3 turns") },
            { 0x119F, new Tuple<string, string>("Death Chaser", "User may follow-up user´s row´s attacks, for 3 turns") },
            { 0x11A1, new Tuple<string, string>("Fierce Stance", "Medium chance allies gain a follow-up hit, for 3 turns (User´s Row)") },
            { 0x11A7, new Tuple<string, string>("Rush Dance", "Attack enemies after ally attacks up to 3 times a turn [Only usable during Stances]") },
            { 0x11B1, new Tuple<string, string>("Pain Watcher", "High chance of Pain-Eater when battle starts [Defense up, draw enemy attacks]") },
            { 0x11B2, new Tuple<string, string>("Auto-Cloak", "Medium chance of Shadow Cloak activation at start of battle") },
            { 0x11D8, new Tuple<string, string>("Bloody Vanguard", "When user attacks, allies deal more damage to that enemy that turn") },
            { 0x11D9, new Tuple<string, string>("Deadly Vanguard", "When user attacks, allies deal even more damage to that enemy that turn") },
            { 0x11DC, new Tuple<string, string>("Squire Card", "Slightly raise Fire, Ice, Wind and Elec attack strength") },
            { 0x11DD, new Tuple<string, string>("Knight Card", "Raise Fire, Ice, Wind and Elec attack strength") },
            { 0x11E6, new Tuple<string, string>("Rebel Spirit", "Slightly raise attack against enemies with higher HP") },
            { 0x11E7, new Tuple<string, string>("Uprising", "Raise attack against enemies with higher HP") },
            { 0x11F2, new Tuple<string, string>("Heroic Gemini", "Low chance of attack skills executing twice") },
            { 0x11F3, new Tuple<string, string>("Golden Gemini", "Medium chance of attack skills executing twice") },
            { 0x11F4, new Tuple<string, string>("Runic Shield", "Medium chance to nullify Fire, Wind, Ice and Elec versus the user´s row") },
            { 0x11F5, new Tuple<string, string>("Aegis Shield", "High chance to nullify Fire, Wind, Ice and Elec versus the user´s row") },
            { 0x11F6, new Tuple<string, string>("Swordbreaker", "Medium chance of halving physical damage versus the user´s row") },
            { 0x11F9, new Tuple<string, string>("First Star", "Raise damage when attacking before all enemies act") },
            { 0x11FA, new Tuple<string, string>("Binding Hands", "Raise chance of causing Binds") },
            { 0x11FB, new Tuple<string, string>("Restoring Touch", "Greatly raise natural recovery speed from status ailments and Binds") },
            { 0x11FC, new Tuple<string, string>("Warrior Title", "Raise critical hit rate") },
            { 0x11FD, new Tuple<string, string>("Conqueror Title", "Greatly raise critical hit rate") },
            { 0x11FF, new Tuple<string, string>("Healing Hand", "Raise HP restored by recovery skills") },
            { 0x1202, new Tuple<string, string>("Colossal Swing", "Low chance to add splash effect on single-target attack skills") },
            { 0x1204, new Tuple<string, string>("Golden Link", "Link damage raises with Link number [No effect if only 1 Link]") },
            { 0x1206, new Tuple<string, string>("Forked Spear", "Medium chance of another attack after regular attack") },
            { 0x1207, new Tuple<string, string>("Trick Step", "Raise evasion rate against all attacks") },
            { 0x1208, new Tuple<string, string>("Endure", "Withstand damage that brings HP down to 0 with 1 HP") },
            { 0x120A, new Tuple<string, string>("Immunity Buffer", "High chance of nullifying physical attacks against the user") },
            { 0x120B, new Tuple<string, string>("Lemegeton", "Raise power of summons in Circles") },
            { 0x120C, new Tuple<string, string>("Platinum Coin", "Medium chance to prevent all Binds and ailments against the user´s row") },
            { 0x1212, new Tuple<string, string>("Fire Boost", "Slightly raise Fire attack strength") },
            { 0x1213, new Tuple<string, string>("Ice Boost", "Slightly raise Ice attack strength") },
            { 0x1214, new Tuple<string, string>("Elec Boost", "Slightly raise Elec attack strength") },
            { 0x1215, new Tuple<string, string>("Wind Boost", "Slightly raise Wind attack strength") },
            { 0x1216, new Tuple<string, string>("Fire Amp", "Raise Fire attack strength") },
            { 0x1217, new Tuple<string, string>("Ice Amp", "Raise Ice attack strength") },
            { 0x1218, new Tuple<string, string>("Elec Amp", "Raise Elec attack strength") },
            { 0x1219, new Tuple<string, string>("Wind Amp", "Raise Wind attack strength") },
            { 0x121E, new Tuple<string, string>("Death´s Scythe", "Greatly raise damage against enemies with status ailments") },
            { 0x121F, new Tuple<string, string>("Impure Reach", "Raise chance of inflicting ailments") },
            { 0x1222, new Tuple<string, string>("King Card", "Greatly raise Fire, Ice, Wind and Elec attack strength") },
            { 0x1223, new Tuple<string, string>("Inferno Boost", "Greatly raise Fire attack strength") },
            { 0x1224, new Tuple<string, string>("Tornado Boost", "Greatly raise Wind attack strength") },
            { 0x1225, new Tuple<string, string>("Powerhouse", "Raise chance of knockdown") },
            { 0x1236, new Tuple<string, string>("Double Link", "Raise damage dealt with first hit and number of Links") },
            { 0x1241, new Tuple<string, string>("Resist Fire", "Reduce damage from Fire attacks") },
            { 0x1242, new Tuple<string, string>("Resist Ice", "Reduce damage from Ice attacks") },
            { 0x1243, new Tuple<string, string>("Resist Elec", "Reduce damage from Elec attacks") },
            { 0x1244, new Tuple<string, string>("Resist Wind", "Reduce damage from Wind attacks") },
            { 0x1245, new Tuple<string, string>("Resist Light", "Reduce chance of instant kill from Light") },
            { 0x1246, new Tuple<string, string>("Resist Dark", "Reduce chance of instant kill from Darkness") },
            { 0x124A, new Tuple<string, string>("Null Petrify", "Prevent Petrification") },
            { 0x124B, new Tuple<string, string>("Null Sleep", "Prevent Sleep") },
            { 0x124C, new Tuple<string, string>("Null Panic", "Prevent Panic") },
            { 0x124D, new Tuple<string, string>("Null Poison", "Prevent Poison") },
            { 0x124F, new Tuple<string, string>("Null Curse", "Prevent Curse") },
            { 0x1250, new Tuple<string, string>("Null Paralysis", "Prevent Paralysis") },
            { 0x1253, new Tuple<string, string>("Judgement Sword", "Perform a regular attack whenever a Bind is inflicted on an enemy") },
            { 0x1255, new Tuple<string, string>("Null M-Bind", "Prevent Magic Bind") },
            { 0x1256, new Tuple<string, string>("Null S-Bind", "Prevent Strength Bind") },
            { 0x1257, new Tuple<string, string>("Null A-Bind", "Prevent Agility Bind") },
            { 0x12C4, new Tuple<string, string>("Punisher", "Raise critical hit rate versus enemies with Bind") },
            { 0x12CC, new Tuple<string, string>("Triple Link", "Raise damage dealt with first hit and number of Links") },
            { 0x12CD, new Tuple<string, string>("Quadruple Link", "Raise damage dealt with first hit and number of Links") },
            { 0x12CE, new Tuple<string, string>("Infinite Link", "Raise damage dealt with first hit and number of Links") },
            { 0x12D0, new Tuple<string, string>("Skill Boost", "Raise damage from physical skills") },
        };

        #endregion SkillsBuff

        #region SkillsCut

        internal static Dictionary<int, Tuple<string, string>> SkillsCut = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1083, new Tuple<string, string>("Cleave", "A light Cut attack (1 Enemy)") },
            { 0x1084, new Tuple<string, string>("Twin Slash", "2 light Cut attacks (1 Enemy)") },
            { 0x1087, new Tuple<string, string>("Mighty Swing", "A light Cut attack that splashes to either side (1 Enemy)") },
            { 0x1088, new Tuple<string, string>("Fatal End", "A heavy Cut attack which takes longer to execute (1 Enemy)") },
            { 0x108B, new Tuple<string, string>("Frost Edge", "A light Cut + Ice attack (1 Enemy)") },
            { 0x108C, new Tuple<string, string>("Zanshinken", "Medium Cut. Hits again if user takes no damage that turn (1 Enemy)") },
            { 0x108F, new Tuple<string, string>("Guillotine", "A heavy Cut attack, higher damage if target has ailment (1 Enemy)") },
            { 0x1090, new Tuple<string, string>("Tempest Slash", "3 light Cut attacks (1 Enemy)") },
            { 0x10A4, new Tuple<string, string>("Deathbound", "A heavy Cut attack (All Enemies)") },
            { 0x10A5, new Tuple<string, string>("Blade of Fury", "A heavy Cut attack (1 Row)") },
            { 0x10A6, new Tuple<string, string>("Glacial Edge", "A light Cut + Ice attack (1 Enemy)") },
            { 0x10A7, new Tuple<string, string>("Vorpal Blade", "A heavy Cut attack (1 Enemy)") },
            { 0x10A8, new Tuple<string, string>("Danse Macabre", "5-7 medium Cut attacks against random targets (All Enemies)") },
            { 0x10AE, new Tuple<string, string>("Brutal Slash", "A heavy Cut attack (1 Enemy)") },
            { 0x10B1, new Tuple<string, string>("Heaven´s Blade", "A severe Cut attack (1 Enemy)") },
            { 0x10B4, new Tuple<string, string>("Hassou Tobi", "8 medium Cut attacks against random targets (All Enemies)") },
            { 0x10B8, new Tuple<string, string>("Blinding Slice", "A light Cut attack, with medium chance of Blind (1 Enemy)") },
            { 0x10B9, new Tuple<string, string>("Toxic Slice", "A light Cut attack, with medium chance of Poison (1 Enemy)") },
            { 0x10BA, new Tuple<string, string>("Dream Slice", "A light Cut attack, with medium chance of Sleep (1 Enemy)") },
            { 0x10BB, new Tuple<string, string>("Stunning Slice", "A light Cut attack, with medium chance of Paralysis (1 Enemy)") },
            { 0x10BC, new Tuple<string, string>("Bane Slice", "A light Cut attack, with medium chance of Curse (1 Enemy)") },
            { 0x10BD, new Tuple<string, string>("Gurentou", "A light Cut + Fire attack (1 Enemy)") },
            { 0x10BE, new Tuple<string, string>("Tousatsujin", "A heavy Cut + Ice attack (1 Enemy)") },
            { 0x10BF, new Tuple<string, string>("Raikouzan", "A light Cut + Elec attack (1 Enemy)") },
            { 0x10C0, new Tuple<string, string>("Jinpugeki", "A light Cut + Wind attack (1 Enemy)") },
            { 0x10C6, new Tuple<string, string>("Raimeizan", "A medium Cut + Elec attack (1 Enemy)") },
            { 0x10C7, new Tuple<string, string>("Raijinzan", "A heavy Cut + Elec attack (1 Enemy)") },
            { 0x10C8, new Tuple<string, string>("Gokuentou", "A medium Cut + Fire attack (1 Enemy)") },
            { 0x10C9, new Tuple<string, string>("Hientou", "A heavy Cut + Fire attack (1 Enemy)") },
            { 0x10CA, new Tuple<string, string>("Reppu Strike", "A medium Cut + Wind attack (1 Enemy)") },
            { 0x10CB, new Tuple<string, string>("Kamikaze Strike", "A heavy Cut + Wind attack (1 Enemy)") },
            { 0x10CC, new Tuple<string, string>("Hex Slice", "A medium Cut attack, with medium chance of Curse (1 Enemy)") },
            { 0x10CD, new Tuple<string, string>("Soul Slice", "A heavy Cut attack, with medium chance of Curse (1 Enemy)") },
            { 0x1127, new Tuple<string, string>("Calamity Seed", "Multiple heavy Cut attacks per turn, for 3 turns (All Enemies)") },
            { 0x11A9, new Tuple<string, string>("Flame Link", "A light Cut + Fire attack (1 Enemy) [Link damage with allies´ attacks]") },
            { 0x11AB, new Tuple<string, string>("Frost Link", "A light Cut + Ice attack (1 Enemy) [Link damage with allies´ attacks]") },
            { 0x11AD, new Tuple<string, string>("Bolt Link", "A light Cut + Elec attack (1 Enemy) [Link damage with allies´ attacks]") },
            { 0x11AF, new Tuple<string, string>("Gale Link", "A light Cut + Wind attack (1 Enemy) [Link damage with allies´ attacks]") },
            { 0x1258, new Tuple<string, string>("Demon´s Cut", "Slightly raise Cut attack strength") },
            { 0x125B, new Tuple<string, string>("God´s Cut", "Raise Cut attack strength") },
        };

        #endregion SkillsCut

        #region SkillsDebuff

        internal static Dictionary<int, Tuple<string, string>> SkillsDebuff = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1053, new Tuple<string, string>("Pulinpa", "Medium chance of Panic (1 Enemy)") },
            { 0x1054, new Tuple<string, string>("Tentarafoo", "Medium chance of Panic (All Enemies)") },
            { 0x1055, new Tuple<string, string>("Poisma", "Medium chance of Poison (1 Enemy)") },
            { 0x1056, new Tuple<string, string>("Poison Breath", "Medium chance of Poison (All Enemies)") },
            { 0x1059, new Tuple<string, string>("Dormina", "Medium chance of Sleep (1 Enemy)") },
            { 0x105A, new Tuple<string, string>("Lullaby Song", "Medium chance of Sleep (All Enemies)") },
            { 0x105B, new Tuple<string, string>("Evil Touch", "Medium chance of Curse (1 Enemy)") },
            { 0x105C, new Tuple<string, string>("Evil Smile", "Medium chance of Curse (All Enemies)") },
            { 0x1097, new Tuple<string, string>("Scarecrow", "Medium chance of Agility Bind (1 Enemy)") },
            { 0x1098, new Tuple<string, string>("Disarm", "Medium chance of Strength Bind (1 Enemy)") },
            { 0x1099, new Tuple<string, string>("Makajam", "Medium chance of Magic Bind (1 Enemy)") },
            { 0x1105, new Tuple<string, string>("Matarunda", "Lower physical attack for 3 turns (All Enemies)") },
            { 0x1106, new Tuple<string, string>("Marakunda", "Lower physical defense for 3 turns (All Enemies)") },
            { 0x1107, new Tuple<string, string>("Masukunda", "Lower hit rate and evasion rate for 3 turns (All Enemies)") },
            { 0x110C, new Tuple<string, string>("Stagnant Air", "Extend timers on Binds and ailments (All Enemies) [Doesn't stack]") },
            { 0x110D, new Tuple<string, string>("Salome´s Kiss", "High chance of Magic Bind, Strength Bind and Agility Bind (1 Enemy)") },
            { 0x1111, new Tuple<string, string>("Dekaja", "Remove stat buffs (All Enemies)") },
            { 0x111F, new Tuple<string, string>("Tarunda", "Lower attack for 3 turns (1 Enemy)") },
            { 0x1120, new Tuple<string, string>("Rakunda", "Lower defense for 3 turns (1 Enemy)") },
            { 0x1121, new Tuple<string, string>("Sukunda", "Lower hit rate and evasion rate for 3 turns (1 Enemy)") },
            { 0x1122, new Tuple<string, string>("Debilitate", "Greatly lower attack, defense, hit and evasion for 3 turns (1 Enemy)") },
            { 0x1181, new Tuple<string, string>("Stun Circle", "Medium chance of Paralysis each turn, for 3 turns (All Enemies)") },
            { 0x1183, new Tuple<string, string>("Sleep Circle", "Medium chance of Sleep each turn, for 3 turns (All Enemies)") },
            { 0x1185, new Tuple<string, string>("Poison Circle", "Medium chance of Poison each turn, for 3 turns (All Enemies)") },
            { 0x1187, new Tuple<string, string>("Panic Circle", "Medium chance of Panic each turn, for 3 turns (All Enemies)") },
            { 0x1189, new Tuple<string, string>("Silence Circle", "Medium chance of Magic Bind each turn, for 3 turns (All Enemies)") },
            { 0x118B, new Tuple<string, string>("Decay Circle", "Medium chance of Strength Bind each turn, for 3 turns (All Enemies)") },
            { 0x118D, new Tuple<string, string>("Lethargy Circle", "Medium chance of Agility Bind each turn, for 3 turns (All Enemies)") },
        };

        #endregion SkillsDebuff

        #region SkillsMagic

        internal static Dictionary<int, Tuple<string, string>> SkillsMagic = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1002, new Tuple<string, string>("Agi", "A light Fire attack (1 Enemy)") },
            { 0x1003, new Tuple<string, string>("Agilao", "A medium Fire attack (1 Enemy)") },
            { 0x1004, new Tuple<string, string>("Agidyne", "A heavy Fire attack (1 Enemy)") },
            { 0x1005, new Tuple<string, string>("Maragi", "A light Fire attack (All Enemies)") },
            { 0x1006, new Tuple<string, string>("Maragion", "A medium Fire attack (All Enemies)") },
            { 0x1007, new Tuple<string, string>("Maragidyne", "A heavy Fire attack (All Enemies)") },
            { 0x1008, new Tuple<string, string>("Fire Spray", "A light Fire attack that splashes to either side (1 Enemy)") },
            { 0x1009, new Tuple<string, string>("Ragnarok", "A severe Fire attack (1 Enemy)") },
            { 0x100E, new Tuple<string, string>("Garu", "A light Wind attack (1 Enemy)") },
            { 0x100F, new Tuple<string, string>("Garula", "A medium Wind attack (1 Enemy)") },
            { 0x1010, new Tuple<string, string>("Garudyne", "A heavy Wind attack (1 Enemy)") },
            { 0x1011, new Tuple<string, string>("Magaru", "A light Wind attack (All Enemies)") },
            { 0x1012, new Tuple<string, string>("Magarula", "A medium Wind attack (All Enemies)") },
            { 0x1013, new Tuple<string, string>("Magarudyne", "A heavy Wind attack (All Enemies)") },
            { 0x1014, new Tuple<string, string>("Cyclone", "2-3 light Wind attacks against random targets (1 Row)") },
            { 0x1015, new Tuple<string, string>("Panta Rhei", "A severe Wind attack that splashes to either side (1 Enemy)") },
            { 0x101A, new Tuple<string, string>("Bufu", "A light Ice attack (1 Enemy)") },
            { 0x101B, new Tuple<string, string>("Bufula", "A medium Ice attack (1 Enemy)") },
            { 0x101C, new Tuple<string, string>("Bufudyne", "A heavy Ice attack (1 Enemy)") },
            { 0x101D, new Tuple<string, string>("Mabufu", "A light Ice attack (All Enemies)") },
            { 0x101E, new Tuple<string, string>("Mabufula", "A medium Ice attack (All Enemies)") },
            { 0x101F, new Tuple<string, string>("Mabufudyne", "A heavy Ice attack (All Enemies)") },
            { 0x1020, new Tuple<string, string>("Icy Paradise", "2-4 medium Ice attacks against random targets (All Enemies)") },
            { 0x1021, new Tuple<string, string>("Niflheim", "A severe Ice attack (1 Enemy)") },
            { 0x1022, new Tuple<string, string>("Frozen Spear", "A light Ice attack that pierces to the back row (1 Enemy)") },
            { 0x1027, new Tuple<string, string>("Zio", "A light Elec attack (1 Enemy)") },
            { 0x1028, new Tuple<string, string>("Zionga", "A medium Elec attack (1 Enemy)") },
            { 0x1029, new Tuple<string, string>("Ziodyne", "A heavy Elec attack (1 Enemy)") },
            { 0x102A, new Tuple<string, string>("Mazio", "A light Elec attack (All Enemies)") },
            { 0x102B, new Tuple<string, string>("Mazionga", "A medium Elec attack (All Enemies)") },
            { 0x102C, new Tuple<string, string>("Maziodyne", "A heavy Elec attack (All Enemies)") },
            { 0x102E, new Tuple<string, string>("Thunder Reign", "A severe Elec attack (1 Enemy)") },
            { 0x102F, new Tuple<string, string>("Keraunos", "6 heavy Elec attacks against random targets (All Enemies)") },
            { 0x1030, new Tuple<string, string>("Thunder Clap", "A light Elec attack (1 Row)") },
            { 0x103D, new Tuple<string, string>("Hama", "Slight chance of instant kill (1 Enemy) [Light-based attack]") },
            { 0x103E, new Tuple<string, string>("Hamaon", "Medium chance of instant kill (1 Enemy) [Light-based attack]") },
            { 0x103F, new Tuple<string, string>("Mahama", "Slight chance of instant kill (All Enemies) [Light-based attack]") },
            { 0x1040, new Tuple<string, string>("Mahamaon", "Medium chance of instant kill (All Enemies) [Light-based attack]") },
            { 0x1043, new Tuple<string, string>("Samsara", "High chance of instant kill (All Enemies) [Light-based attack]") },
            { 0x1048, new Tuple<string, string>("Mudo", "Slight chance of instant kill (1 Enemy) [Darkness-based attack]") },
            { 0x1049, new Tuple<string, string>("Mudoon", "Medium chance of instant kill (1 Enemy) [Darkness-based attack]") },
            { 0x104A, new Tuple<string, string>("Mamudo", "Slight chance of instant kill (All Enemies) [Darkness-based attack]") },
            { 0x104B, new Tuple<string, string>("Mamudoon", "Medium chance of instant kill (All Enemies) [Darkness-based attack]") },
            { 0x104E, new Tuple<string, string>("Die For Me!", "High chance of instant kill (All Enemies) [Darkness-based attack]") },
        };

        #endregion SkillsMagic

        #region SkillsNavi

        internal static Dictionary<int, Tuple<string, string>> SkillsNavi = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1142, new Tuple<string, string>("Sutakora Foot", "[Navi] Flee from battle and teleport to floor entrance (Party)") },
            { 0x1145, new Tuple<string, string>("Snake Glare", "[Navi] High chance of preventing preemptive attacks") },
            { 0x1146, new Tuple<string, string>("Heroic Wind", "[Navi] Restore 5% of HP each turn (Party)") },
            { 0x114B, new Tuple<string, string>("Harvest Prayer", "[Navi] Higher rate of rare materials. Rare chance to gather multiple times") },
            { 0x114C, new Tuple<string, string>("Cornucopia", "[Navi] Higher rate of rare materials. Low chance to gather multiple items") },
            { 0x114D, new Tuple<string, string>("Healing Tide", "[Navi] Moderate HP restore at end of turn, for 3 turns (Party)") },
            { 0x114F, new Tuple<string, string>("Renewal Ray", "[Navi] Slight HP restore (Party) [Battle-only]") },
            { 0x1150, new Tuple<string, string>("Renewal Aura", "[Navi] Moderate HP restore (Party) [Battle-only]") },
            { 0x1151, new Tuple<string, string>("Healing Breeze", "[Navi] Slight HP restore at end of turn, for 3 turns (Party)") },
            { 0x1152, new Tuple<string, string>("Spotlight", "[Navi] Allow someone to act first for the turn (1 Ally)") },
            { 0x1153, new Tuple<string, string>("Yomi Return", "[Navi] Revive, with half HP restore (1 Ally)") },
            { 0x1155, new Tuple<string, string>("Orb of Resolve", "[Navi] Raise physical and elemental defense for 3 turns (Party)") },
            { 0x1156, new Tuple<string, string>("Orb of Power", "[Navi] Raise physical and elemental attack for 3 turns (Party)") },
            { 0x1157, new Tuple<string, string>("Orb of Haste", "[Navi] Raise hit rate and evasion rate for 3 turns (Party)") },
            { 0x1158, new Tuple<string, string>("Life Aid", "[Navi] 20% HP restore at the end of battle (Party)") },
            { 0x1159, new Tuple<string, string>("Victory Cry", "[Navi] Full HP restore at the end of battle (Party)") },
            { 0x115A, new Tuple<string, string>("Safety Prayer", "[Navi] Gather somewhat safely from Power Spots") },
            { 0x115B, new Tuple<string, string>("Calming Lull", "[Navi] Gather safely from Power Spots") },
            { 0x115C, new Tuple<string, string>("Spotter", "[Navi] Slightly higher rate of rare materials from Power Spots") },
            { 0x115D, new Tuple<string, string>("Magic Mallet", "[Navi] Higher rate of rare materials from Power Spots") },
            { 0x115E, new Tuple<string, string>("Fanfare", "[Navi] Prevent preemptive attacks") },
            { 0x115F, new Tuple<string, string>("Into the Void", "[Navi] Nullifies all damage for 1 turn") },
            { 0x1160, new Tuple<string, string>("Tidal Wave", "[Navi] Allow the party to act before any enemies for the turn (Party)") },
            { 0x1161, new Tuple<string, string>("Oracle", "[Navi] Give a random beneficial effect (Party)") },
            { 0x1169, new Tuple<string, string>("Prayer", "[Navi] Full HP restore and remove all Binds and ailments (Party)") },
            { 0x116B, new Tuple<string, string>("Treasure Hunter", "[Navi] Reveal nearby treasure boxes") },
            { 0x116C, new Tuple<string, string>("Fortune Hunter", "[Navi] Reveal treasure boxes in a wide area") },
            { 0x116D, new Tuple<string, string>("Faerie´s Vigil", "[Navi] Reveal nearby shortcuts") },
            { 0x116E, new Tuple<string, string>("Tengu´s Vigil", "[Navi] Reveal shortcuts in a wide area") },
            { 0x116F, new Tuple<string, string>("Clairvoyance", "[Navi] Reveal nearby treasure boxes, FOEs and shortcuts") },
            { 0x1170, new Tuple<string, string>("Farsight", "[Navi] Reveal treasure boxes, FOEs, and shortcuts in a wide area") },
            { 0x1171, new Tuple<string, string>("Purifying Rain", "[Navi] Remove all Binds and ailments (Party)") },
            { 0x1173, new Tuple<string, string>("Zero Set", "[Navi] Skill costs are reduced to 0 for 1 turn (Party)") },
            { 0x1174, new Tuple<string, string>("Eternal Melody", "[Navi] Extend stat buffs by 3 additional turns (Party)") },
            { 0x1179, new Tuple<string, string>("Hunting Prayer", "[Navi] Slightly higher rate of material drops from enemies") },
            { 0x117A, new Tuple<string, string>("Endless Bounty", "[Navi] Higher rate of material drops from enemies") },
            { 0x117C, new Tuple<string, string>("Alleys of Light", "[Navi] Slight HP restore for each step taken in a labyrinth (Party)") },
            { 0x117D, new Tuple<string, string>("Paths of Light", "[Navi] HP restore for each step taken in a labyrinth (Party)") },
            { 0x117E, new Tuple<string, string>("Roads of Light", "[Navi] Slight SP restore for each step taken in a labyrinth (Party)") },
        };

        #endregion SkillsNavi

        #region SkillsRecovery

        internal static Dictionary<int, Tuple<string, string>> SkillsRecovery = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x10D1, new Tuple<string, string>("Dia", "Slight HP restore (1 Ally)") },
            { 0x10D2, new Tuple<string, string>("Diarama", "Moderate HP restore (1 Ally)") },
            { 0x10D3, new Tuple<string, string>("Diarahan", "Full HP restore (1 Ally)") },
            { 0x10D4, new Tuple<string, string>("Recovery", "Slight HP restore (1 Row)") },
            { 0x10D5, new Tuple<string, string>("Life Goblet", "Moderate HP restore (1 Row)") },
            { 0x10D6, new Tuple<string, string>("Media", "Slight HP restore (Party)") },
            { 0x10D7, new Tuple<string, string>("Mediarama", "Moderate HP restore (Party)") },
            { 0x10D8, new Tuple<string, string>("Mediarahan", "Full HP restore (Party)") },
            { 0x10D9, new Tuple<string, string>("Recarm", "Revive, with half HP restore (1 Ally)") },
            { 0x10DA, new Tuple<string, string>("Samarecarm", "Revive, with full HP restore (1 Ally)") },
            { 0x10DC, new Tuple<string, string>("Renewal", "Remove stat penalties (1 Row) [Battle-only]") },
            { 0x10DD, new Tuple<string, string>("Patra", "Remove status ailments (1 Ally)") },
            { 0x10DE, new Tuple<string, string>("Refresh", "Remove status ailments (1 Row) [Battle-only]") },
            { 0x10DF, new Tuple<string, string>("Me Patra", "Remove status ailments (Party)") },
            { 0x10E0, new Tuple<string, string>("Mutudi", "Remove all Binds (1 Ally)") },
            { 0x10E1, new Tuple<string, string>("Becalm", "Remove all Binds (1 Row) [Battle-only]") },
            { 0x10E2, new Tuple<string, string>("Mamutudi", "Remove all Binds (Party)") },
            { 0x10E3, new Tuple<string, string>("Amrita", "Full HP restore. Remove all Binds and status ailments (1 Ally)") },
            { 0x1112, new Tuple<string, string>("Dekunda", "Remove stat penalties (Party)") },
            { 0x112B, new Tuple<string, string>("Healing Harp", "Restore HP and remove ailments/Binds each turn for 3 turns (Party)") },
            { 0x1196, new Tuple<string, string>("Heal Memento", "Slight HP restore at end of turn, for 3 turns (User´s Row)") },
            { 0x1198, new Tuple<string, string>("Free Memento", "Remove all Binds at end of turn, for 3 turns (User´s Row)") },
            { 0x119A, new Tuple<string, string>("Pure Memento", "Remove status ailments at end of each turn, for 3 turns (User´s Row)") },
            { 0x11A5, new Tuple<string, string>("Healing Step", "Full HP restore (Party) [Only usable during Blessings]") },
            { 0x1200, new Tuple<string, string>("Dia Guard", "Slight HP restore when the user´s row uses Defend") },
            { 0x1201, new Tuple<string, string>("Diarama Guard", "HP restore when the user´s row uses Defend") },
            { 0x120D, new Tuple<string, string>("Absorb Fire", "Absorb damage from Fire attacks") },
            { 0x120E, new Tuple<string, string>("Absorb Wind", "Absorb damage from Wind attacks") },
            { 0x120F, new Tuple<string, string>("Absorb Ice", "Absorb damage from Ice attacks") },
            { 0x1210, new Tuple<string, string>("Absorb Elec", "Absorb damage from Elec attacks") },
            { 0x1221, new Tuple<string, string>("Circle Recovery", "Slight HP restore each turn (Party) [Requires Circles]") },
            { 0x1233, new Tuple<string, string>("Serene Journey", "Slight SP restore for each step taken in a labyrinth") },
            { 0x12CB, new Tuple<string, string>("Return from Yomi", "Medium chance of HP restore when the user's HP drops below 30%") },
            { 0x12CF, new Tuple<string, string>("Magic Aura", "SP restore when defeating an enemy using its weakness") },
        };

        #endregion SkillsRecovery

        #region SkillsStab

        internal static Dictionary<int, Tuple<string, string>> SkillsStab = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1085, new Tuple<string, string>("End Shot", "A heavy Stab attack which takes longer to execute (1 Enemy)") },
            { 0x1086, new Tuple<string, string>("Double Shot", "2 light Stab attacks (1 Enemy)") },
            { 0x1089, new Tuple<string, string>("Silent Thrust", "A heavy Stab attack that pierces to the back row (1 Enemy)") },
            { 0x108A, new Tuple<string, string>("Swift Strike", "3 quick, medium Stab attacks against random targets (1 Row)") },
            { 0x108E, new Tuple<string, string>("Death Needle", "Heavy Stab attack. Low chance instant kill, higher with ailment (1 Enemy)") },
            { 0x1096, new Tuple<string, string>("Single Shot", "A light Stab attack that pierces to the back row (1 Enemy)") },
            { 0x109A, new Tuple<string, string>("Assault Shot", "A medium Stab attack that pierces to the back row (1 Enemy)") },
            { 0x109B, new Tuple<string, string>("Primal Force", "A heavy Stab attack, ignoring target´s resistances and buffs (1 Enemy)") },
            { 0x109C, new Tuple<string, string>("Heavy Shot", "A heavy Stab attack that pierces to the back row (1 Enemy)") },
            { 0x109D, new Tuple<string, string>("Broadshot", "A light Stab attack (1 Row)") },
            { 0x109E, new Tuple<string, string>("Myriad Arrows", "6-8 light Stab attacks at random, with low accuracy (All Enemies)") },
            { 0x109F, new Tuple<string, string>("Punishment", "A heavy Stab attack effective on Bind, but will remove Binds (1 Enemy)") },
            { 0x10B5, new Tuple<string, string>("Hailstorm", "A medium Stab attack (All Enemies)") },
            { 0x10B6, new Tuple<string, string>("Arrow Rain", "A light Stab attack (All Enemies)") },
            { 0x10B7, new Tuple<string, string>("Aeon Rain", "A medium Stab attack (All Enemies)") },
            { 0x125A, new Tuple<string, string>("Demon´s Stab", "Slightly raise Stab attack strength") },
            { 0x125D, new Tuple<string, string>("God´s Stab", "Raise Stab attack strength") },
        };

        #endregion SkillsStab

        #endregion Skills

        #region SkillCards

        #region SkillCardsAlmighty

        internal static Dictionary<int, Tuple<string, string>> SkillCardsAlmighty = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xA33, new Tuple<string, string>("Megido", "A medium Almighty attack, with medium chance of knockdown (All Enemies)") },
            { 0xA34, new Tuple<string, string>("Megidola", "A heavy Almighty attack, with medium chance of knockdown (All Enemies)") },
            { 0xA35, new Tuple<string, string>("Megidolaon", "A severe Almighty attack, with medium chance of knockdown (All Enemies)") },
            { 0xA37, new Tuple<string, string>("Black Viper", "A severe Almighty attack (1 Enemy)") },
            { 0xA38, new Tuple<string, string>("Morning Star", "A severe Almighty attack (All Enemies)") },
            { 0xAAF, new Tuple<string, string>("Photon Edge", "A heavy Almighty attack (1 Enemy)") },
            { 0xAB0, new Tuple<string, string>("Prana", "A severe Almighty attack (All Enemies)") },
            { 0xAC1, new Tuple<string, string>("Shinkuuha", "2 medium Almighty attacks (1 Enemy)") },
            { 0xAC2, new Tuple<string, string>("Resseiha", "1-5 medium Almighty attacks against random targets (All Enemies)") },
            { 0xAC3, new Tuple<string, string>("Pralaya", "A severe Almighty attack, with medium chance of instant kill (All Enemies)") },
            { 0xAC4, new Tuple<string, string>("Shadow Run", "A heavy Almighty attack (All Enemies)") },
            { 0xAC5, new Tuple<string, string>("World´s End", "A severe Almighty attack (All Enemies)") },
            { 0xB25, new Tuple<string, string>("Door of Hades", "3 heavy Almighty attacks, 1 per turn. High chance of instant kill (All Enemies)") },
            { 0xB8F, new Tuple<string, string>("Summon Ghost", "A medium Almighty attack (All Enemies) [Requires Circles]") },
            { 0xB90, new Tuple<string, string>("Summon Demon", "A heavy Almighty attack (All Enemies) [Requires Circles]") },
        };

        #endregion SkillCardsAlmighty

        #region SkillCardsBash

        internal static Dictionary<int, Tuple<string, string>> SkillCardsBash = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xA81, new Tuple<string, string>("Fang Smash", "A medium Bash attack, lowers target´s attack for 3 turns (1 Enemy)") },
            { 0xA82, new Tuple<string, string>("Kidney Smash", "A medium Bash attack, lowers elemental attack for 3 turns (1 Enemy)") },
            { 0xA91, new Tuple<string, string>("Bolt Strike", "A light Bash + Elec attack (1 Enemy)") },
            { 0xA92, new Tuple<string, string>("Holy Touch", "A heavy Bash attack (1 Enemy)") },
            { 0xA93, new Tuple<string, string>("Revenge Blow", "A Bash attack delayed to next turn, stronger for each hit taken (1 Enemy)") },
            { 0xA95, new Tuple<string, string>("Gigantic Fist", "A medium Bash attack that splashes to either side (1 Enemy)") },
            { 0xAA0, new Tuple<string, string>("Assault Dive", "A medium Bash attack, with medium chance of knockdown (1 Enemy)") },
            { 0xAA1, new Tuple<string, string>("Sleeper Punch", "A light Bash attack, with medium chance of Sleep (1 Enemy)") },
            { 0xAA2, new Tuple<string, string>("Teardrop", "A light Bash attack, lowers target's defense for that turn (1 Enemy)") },
            { 0xAA3, new Tuple<string, string>("Buchikamashi", "A light Bash attack (1 Enemy)") },
            { 0xAA9, new Tuple<string, string>("Skull Cracker", "A light Bash attack, with medium chance of Magic Bind (1 Enemy)") },
            { 0xAAA, new Tuple<string, string>("Fusion Blast", "A light Bash + Fire attack (1 Enemy)") },
            { 0xAAB, new Tuple<string, string>("Heat Wave", "A medium Bash attack that splashes to either side (1 Enemy)") },
            { 0xAAC, new Tuple<string, string>("Light Wave", "A severe Bash attack (1 Enemy)") },
            { 0xAB2, new Tuple<string, string>("God´s Hand", "A severe Bash attack (1 Enemy)") },
            { 0xAB3, new Tuple<string, string>("Angelic Trumpet", "3 heavy Bash attacks against random targets (All Enemies)") },
            { 0xB2D, new Tuple<string, string>("Lightning Smash", "A medium Bash + Elec attack (1 Enemy)") },
            { 0xB2E, new Tuple<string, string>("Thunder Smash", "A heavy Bash + Elec attack (1 Enemy)") },
            { 0xB2F, new Tuple<string, string>("Scorching Blast", "A medium Bash + Fire attack (1 Enemy)") },
            { 0xB30, new Tuple<string, string>("Nuclear Blast", "A heavy Bash + Fire attack (1 Enemy)") },
            { 0xB31, new Tuple<string, string>("Sonic Punch", "A medium Bash attack, with medium chance of Sleep (1 Enemy)") },
            { 0xB32, new Tuple<string, string>("Brain Snake", "A heavy Bash attack, with medium chance of Sleep (1 Enemy)") },
            { 0xB33, new Tuple<string, string>("Moondrop", "A medium Bash attack, lowers target´s defense for that turn (1 Enemy)") },
            { 0xB34, new Tuple<string, string>("Stardrop", "A heavy Bash attack, lowers target´s defense for that turn (1 Enemy)") },
            { 0xC59, new Tuple<string, string>("Demon´s Bash", "Slightly raise Bash attack strength") },
            { 0xC5C, new Tuple<string, string>("God´s Bash", "Raise Bash attack strength") },
        };

        #endregion SkillCardsBash

        #region SkillCardsBuff

        internal static Dictionary<int, Tuple<string, string>> SkillCardsBuff = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xAE6, new Tuple<string, string>("Bestial Roar", "Act first and raise physical attack for 3 turns") },
            { 0xAE7, new Tuple<string, string>("Dragon Cry", "Act first and greatly raise physical attack for 3 turns") },
            { 0xAEA, new Tuple<string, string>("Shadow Cloak", "Nullifies next physical attack once. Lasts 3 turns") },
            { 0xAEC, new Tuple<string, string>("Pain-Eater", "Raise defense and chance to be targeted for 3 turns") },
            { 0xAED, new Tuple<string, string>("Safeguard", "Take attacks in place of an ally, with reduced damage (1 Ally)") },
            { 0xAEE, new Tuple<string, string>("Body Shield", "Take attacks in place of allies, with reduced damage (1 Row)") },
            { 0xAEF, new Tuple<string, string>("Life Wall", "Take attacks in place of allies, with reduced damage (Party)") },
            { 0xAF0, new Tuple<string, string>("True Warden", "Take attacks in place of allies under half HP for 3 turns") },
            { 0xAF1, new Tuple<string, string>("Holy Blessing", "Nullifies all Binds and status ailments 1 time for 3 turns (Party)") },
            { 0xAF2, new Tuple<string, string>("Guiding Sword", "Raise attack for 3 turns (1 Row)") },
            { 0xAF3, new Tuple<string, string>("Guarding Staff", "Raise defense for 3 turns (1 Row)") },
            { 0xAF5, new Tuple<string, string>("Critical Eye", "Raise critical hit rate for 3 turns") },
            { 0xAF7, new Tuple<string, string>("Matarukaja", "Raise attack for 3 turns (Party)") },
            { 0xAF8, new Tuple<string, string>("Marakukaja", "Raise defense for 3 turns (Party)") },
            { 0xAF9, new Tuple<string, string>("Masukukaja", "Raise hit rate and evasion rate for 3 turns (Party)") },
            { 0xAFA, new Tuple<string, string>("Power Charge", "Nearly triple the user´s physical attack for next hit. Lasts 3 turns") },
            { 0xAFB, new Tuple<string, string>("Mind Charge", "Nearly triple the user´s magic attack for next hit. Lasts 3 turns") },
            { 0xB16, new Tuple<string, string>("Fire Wall", "Raise Fire resistance for 3 turns (Party)") },
            { 0xB17, new Tuple<string, string>("Wind Wall", "Raise Wind resistance for 3 turns (Party)") },
            { 0xB18, new Tuple<string, string>("Ice Wall", "Raise Ice resistance for 3 turns (Party)") },
            { 0xB19, new Tuple<string, string>("Elec Wall", "Raise Elec resistance for 3 turns (Party)") },
            { 0xB1A, new Tuple<string, string>("Light Wall", "Raise Light resistance for 3 turns (Party)") },
            { 0xB1B, new Tuple<string, string>("Dark Wall", "Raise Darkness resistance for 3 turns (Party)") },
            { 0xB1C, new Tuple<string, string>("Tarukaja", "Raise attack for 3 turns (1 Ally)") },
            { 0xB1D, new Tuple<string, string>("Rakukaja", "Raise defense for 3 turns (1 Ally)") },
            { 0xB1E, new Tuple<string, string>("Sukukaja", "Raise hit rate and evasion rate for 3 turns (1 Ally)") },
            { 0xB23, new Tuple<string, string>("Heat Riser", "Greatly raise attack, defense, hit and evasion for 3 turns (1 Ally)") },
            { 0xB24, new Tuple<string, string>("Mind´s Eye", "Raise critical hit rate for 3 turns (Party)") },
            { 0xB29, new Tuple<string, string>("Lunar Blessing", "Nullifies 1 attack per turn, for 3 turns (Party)") },
            { 0xB91, new Tuple<string, string>("Shura Tensei", "Greatly lowers HP each turn but greatly raises attack while active") },
            { 0xB92, new Tuple<string, string>("Shura Revert", "Ends Shura Tensei, restoring moderate HP") },
            { 0xB93, new Tuple<string, string>("Shura Instincts", "High chance of Shura Tensei when battle starts [Attack up, HP down each turn]") },
            { 0xB9D, new Tuple<string, string>("Death Counter", "Counter-attack whenever user´s row is attacked, for 3 turns") },
            { 0xB9F, new Tuple<string, string>("Death Chaser", "User may follow-up user´s row´s attacks, for 3 turns") },
            { 0xBA1, new Tuple<string, string>("Fierce Stance", "Medium chance allies gain a follow-up hit, for 3 turns (User´s Row)") },
            { 0xBA7, new Tuple<string, string>("Rush Dance", "Attack enemies after ally attacks up to 3 times a turn [Only usable during Stances]") },
            { 0xBB1, new Tuple<string, string>("Pain Watcher", "High chance of Pain-Eater when battle starts [Defense up, draw enemy attacks]") },
            { 0xBB2, new Tuple<string, string>("Auto-Cloak", "Medium chance of Shadow Cloak activation at start of battle") },
            { 0xBD8, new Tuple<string, string>("Bloody Vanguard", "When user attacks, allies deal more damage to that enemy that turn") },
            { 0xBD9, new Tuple<string, string>("Deadly Vanguard", "When user attacks, allies deal even more damage to that enemy that turn") },
            { 0xBDC, new Tuple<string, string>("Squire Card", "Slightly raise Fire, Ice, Wind and Elec attack strength") },
            { 0xBDD, new Tuple<string, string>("Knight Card", "Raise Fire, Ice, Wind and Elec attack strength") },
            { 0xBE6, new Tuple<string, string>("Rebel Spirit", "Slightly raise attack against enemies with higher HP") },
            { 0xBE7, new Tuple<string, string>("Uprising", "Raise attack against enemies with higher HP") },
            { 0xBF2, new Tuple<string, string>("Heroic Gemini", "Low chance of attack skills executing twice") },
            { 0xBF3, new Tuple<string, string>("Golden Gemini", "Medium chance of attack skills executing twice") },
            { 0xBF4, new Tuple<string, string>("Runic Shield", "Medium chance to nullify Fire, Wind, Ice and Elec versus the user´s row") },
            { 0xBF5, new Tuple<string, string>("Aegis Shield", "High chance to nullify Fire, Wind, Ice and Elec versus the user´s row") },
            { 0xBF6, new Tuple<string, string>("Swordbreaker", "Medium chance of halving physical damage versus the user´s row") },
            { 0xBF9, new Tuple<string, string>("First Star", "Raise damage when attacking before all enemies act") },
            { 0xBFA, new Tuple<string, string>("Binding Hands", "Raise chance of causing Binds") },
            { 0xBFB, new Tuple<string, string>("Restoring Touch", "Greatly raise natural recovery speed from status ailments and Binds") },
            { 0xBFC, new Tuple<string, string>("Warrior Title", "Raise critical hit rate") },
            { 0xBFD, new Tuple<string, string>("Conqueror Title", "Greatly raise critical hit rate") },
            { 0xBFF, new Tuple<string, string>("Healing Hand", "Raise HP restored by recovery skills") },
            { 0xC02, new Tuple<string, string>("Colossal Swing", "Low chance to add splash effect on single-target attack skills") },
            { 0xC04, new Tuple<string, string>("Golden Link", "Link damage raises with Link number [No effect if only 1 Link]") },
            { 0xC06, new Tuple<string, string>("Forked Spear", "Medium chance of another attack after regular attack") },
            { 0xC07, new Tuple<string, string>("Trick Step", "Raise evasion rate against all attacks") },
            { 0xC08, new Tuple<string, string>("Endure", "Withstand damage that brings HP down to 0 with 1 HP") },
            { 0xC0A, new Tuple<string, string>("Immunity Buffer", "High chance of nullifying physical attacks against the user") },
            { 0xC0B, new Tuple<string, string>("Lemegeton", "Raise power of summons in Circles") },
            { 0xC0C, new Tuple<string, string>("Platinum Coin", "Medium chance to prevent all Binds and ailments against the user´s row") },
            { 0xC12, new Tuple<string, string>("Fire Boost", "Slightly raise Fire attack strength") },
            { 0xC13, new Tuple<string, string>("Ice Boost", "Slightly raise Ice attack strength") },
            { 0xC14, new Tuple<string, string>("Elec Boost", "Slightly raise Elec attack strength") },
            { 0xC15, new Tuple<string, string>("Wind Boost", "Slightly raise Wind attack strength") },
            { 0xC16, new Tuple<string, string>("Fire Amp", "Raise Fire attack strength") },
            { 0xC17, new Tuple<string, string>("Ice Amp", "Raise Ice attack strength") },
            { 0xC18, new Tuple<string, string>("Elec Amp", "Raise Elec attack strength") },
            { 0xC19, new Tuple<string, string>("Wind Amp", "Raise Wind attack strength") },
            { 0xC1E, new Tuple<string, string>("Death´s Scythe", "Greatly raise damage against enemies with status ailments") },
            { 0xC1F, new Tuple<string, string>("Impure Reach", "Raise chance of inflicting ailments") },
            { 0xC22, new Tuple<string, string>("King Card", "Greatly raise Fire, Ice, Wind and Elec attack strength") },
            { 0xC23, new Tuple<string, string>("Inferno Boost", "Greatly raise Fire attack strength") },
            { 0xC24, new Tuple<string, string>("Tornado Boost", "Greatly raise Wind attack strength") },
            { 0xC25, new Tuple<string, string>("Powerhouse", "Raise chance of knockdown") },
            { 0xC36, new Tuple<string, string>("Double Link", "Raise damage dealt with first hit and number of Links") },
            { 0xC41, new Tuple<string, string>("Resist Fire", "Reduce damage from Fire attacks") },
            { 0xC42, new Tuple<string, string>("Resist Ice", "Reduce damage from Ice attacks") },
            { 0xC43, new Tuple<string, string>("Resist Elec", "Reduce damage from Elec attacks") },
            { 0xC44, new Tuple<string, string>("Resist Wind", "Reduce damage from Wind attacks") },
            { 0xC45, new Tuple<string, string>("Resist Light", "Reduce chance of instant kill from Light") },
            { 0xC46, new Tuple<string, string>("Resist Dark", "Reduce chance of instant kill from Darkness") },
            { 0xC4A, new Tuple<string, string>("Null Petrify", "Prevent Petrification") },
            { 0xC4B, new Tuple<string, string>("Null Sleep", "Prevent Sleep") },
            { 0xC4C, new Tuple<string, string>("Null Panic", "Prevent Panic") },
            { 0xC4D, new Tuple<string, string>("Null Poison", "Prevent Poison") },
            { 0xC4F, new Tuple<string, string>("Null Curse", "Prevent Curse") },
            { 0xC50, new Tuple<string, string>("Null Paralysis", "Prevent Paralysis") },
            { 0xC53, new Tuple<string, string>("Judgement Sword", "Perform a regular attack whenever a Bind is inflicted on an enemy") },
            { 0xC55, new Tuple<string, string>("Null M-Bind", "Prevent Magic Bind") },
            { 0xC56, new Tuple<string, string>("Null S-Bind", "Prevent Strength Bind") },
            { 0xC57, new Tuple<string, string>("Null A-Bind", "Prevent Agility Bind") },
        };

        #endregion SkillCardsBuff

        #region SkillCardsCut

        internal static Dictionary<int, Tuple<string, string>> SkillCardsCut = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xA83, new Tuple<string, string>("Cleave", "A light Cut attack (1 Enemy)") },
            { 0xA84, new Tuple<string, string>("Twin Slash", "2 light Cut attacks (1 Enemy)") },
            { 0xA87, new Tuple<string, string>("Mighty Swing", "A light Cut attack that splashes to either side (1 Enemy)") },
            { 0xA88, new Tuple<string, string>("Fatal End", "A heavy Cut attack which takes longer to execute (1 Enemy)") },
            { 0xA8B, new Tuple<string, string>("Frost Edge", "A light Cut + Ice attack (1 Enemy)") },
            { 0xA8C, new Tuple<string, string>("Zanshinken", "Medium Cut. Hits again if user takes no damage that turn (1 Enemy)") },
            { 0xA8F, new Tuple<string, string>("Guillotine", "A heavy Cut attack, higher damage if target has ailment (1 Enemy)") },
            { 0xA90, new Tuple<string, string>("Tempest Slash", "3 light Cut attacks (1 Enemy)") },
            { 0xAA4, new Tuple<string, string>("Deathbound", "A heavy Cut attack (All Enemies)") },
            { 0xAA5, new Tuple<string, string>("Blade of Fury", "A heavy Cut attack (1 Row)") },
            { 0xAA6, new Tuple<string, string>("Glacial Edge", "A light Cut + Ice attack (1 Enemy)") },
            { 0xAA7, new Tuple<string, string>("Vorpal Blade", "A heavy Cut attack (1 Enemy)") },
            { 0xAA8, new Tuple<string, string>("Danse Macabre", "5-7 medium Cut attacks against random targets (All Enemies)") },
            { 0xAAE, new Tuple<string, string>("Brutal Slash", "A heavy Cut attack (1 Enemy)") },
            { 0xAB1, new Tuple<string, string>("Heaven´s Blade", "A severe Cut attack (1 Enemy)") },
            { 0xAB4, new Tuple<string, string>("Hassou Tobi", "8 medium Cut attacks against random targets (All Enemies)") },
            { 0xAB8, new Tuple<string, string>("Blinding Slice", "A light Cut attack, with medium chance of Blind (1 Enemy)") },
            { 0xAB9, new Tuple<string, string>("Toxic Slice", "A light Cut attack, with medium chance of Poison (1 Enemy)") },
            { 0xABA, new Tuple<string, string>("Dream Slice", "A light Cut attack, with medium chance of Sleep (1 Enemy)") },
            { 0xABB, new Tuple<string, string>("Stunning Slice", "A light Cut attack, with medium chance of Paralysis (1 Enemy)") },
            { 0xABC, new Tuple<string, string>("Bane Slice", "A light Cut attack, with medium chance of Curse (1 Enemy)") },
            { 0xABD, new Tuple<string, string>("Gurentou", "A light Cut + Fire attack (1 Enemy)") },
            { 0xABE, new Tuple<string, string>("Tousatsujin", "A heavy Cut + Ice attack (1 Enemy)") },
            { 0xABF, new Tuple<string, string>("Raikouzan", "A light Cut + Elec attack (1 Enemy)") },
            { 0xAC0, new Tuple<string, string>("Jinpugeki", "A light Cut + Wind attack (1 Enemy)") },
            { 0xAC6, new Tuple<string, string>("Raimeizan", "A medium Cut + Elec attack (1 Enemy)") },
            { 0xAC7, new Tuple<string, string>("Raijinzan", "A heavy Cut + Elec attack (1 Enemy)") },
            { 0xAC8, new Tuple<string, string>("Gokuentou", "A medium Cut + Fire attack (1 Enemy)") },
            { 0xAC9, new Tuple<string, string>("Hientou", "A heavy Cut + Fire attack (1 Enemy)") },
            { 0xACA, new Tuple<string, string>("Reppu Strike", "A medium Cut + Wind attack (1 Enemy)") },
            { 0xACB, new Tuple<string, string>("Kamikaze Strike", "A heavy Cut + Wind attack (1 Enemy)") },
            { 0xACC, new Tuple<string, string>("Hex Slice", "A medium Cut attack, with medium chance of Curse (1 Enemy)") },
            { 0xACD, new Tuple<string, string>("Soul Slice", "A heavy Cut attack, with medium chance of Curse (1 Enemy)") },
            { 0xB27, new Tuple<string, string>("Calamity Seed", "Multiple heavy Cut attacks per turn, for 3 turns (All Enemies)") },
            { 0xBA9, new Tuple<string, string>("Flame Link", "A light Cut + Fire attack (1 Enemy) [Link damage with allies´ attacks]") },
            { 0xBAB, new Tuple<string, string>("Frost Link", "A light Cut + Ice attack (1 Enemy) [Link damage with allies´ attacks]") },
            { 0xBAD, new Tuple<string, string>("Bolt Link", "A light Cut + Elec attack (1 Enemy) [Link damage with allies´ attacks]") },
            { 0xBAF, new Tuple<string, string>("Gale Link", "A light Cut + Wind attack (1 Enemy) [Link damage with allies´ attacks]") },
            { 0xC58, new Tuple<string, string>("Demon´s Cut", "Slightly raise Cut attack strength") },
            { 0xC5B, new Tuple<string, string>("God´s Cut", "Raise Cut attack strength") },
        };

        #endregion SkillCardsCut

        #region SkillCardsDebuff

        internal static Dictionary<int, Tuple<string, string>> SkillCardsDebuff = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xA53, new Tuple<string, string>("Pulinpa", "Medium chance of Panic (1 Enemy)") },
            { 0xA54, new Tuple<string, string>("Tentarafoo", "Medium chance of Panic (All Enemies)") },
            { 0xA55, new Tuple<string, string>("Poisma", "Medium chance of Poison (1 Enemy)") },
            { 0xA56, new Tuple<string, string>("Poison Breath", "Medium chance of Poison (All Enemies)") },
            { 0xA59, new Tuple<string, string>("Dormina", "Medium chance of Sleep (1 Enemy)") },
            { 0xA5A, new Tuple<string, string>("Lullaby Song", "Medium chance of Sleep (All Enemies)") },
            { 0xA5B, new Tuple<string, string>("Evil Touch", "Medium chance of Curse (1 Enemy)") },
            { 0xA5C, new Tuple<string, string>("Evil Smile", "Medium chance of Curse (All Enemies)") },
            { 0xA97, new Tuple<string, string>("Scarecrow", "Medium chance of Agility Bind (1 Enemy)") },
            { 0xA98, new Tuple<string, string>("Disarm", "Medium chance of Strength Bind (1 Enemy)") },
            { 0xA99, new Tuple<string, string>("Makajam", "Medium chance of Magic Bind (1 Enemy)") },
            { 0xB05, new Tuple<string, string>("Matarunda", "Lower physical attack for 3 turns (All Enemies)") },
            { 0xB06, new Tuple<string, string>("Marakunda", "Lower physical defense for 3 turns (All Enemies)") },
            { 0xB07, new Tuple<string, string>("Masukunda", "Lower hit rate and evasion rate for 3 turns (All Enemies)") },
            { 0xB0C, new Tuple<string, string>("Stagnant Air", "Extend timers on Binds and ailments (All Enemies) [Doesn't stack]") },
            { 0xB0D, new Tuple<string, string>("Salome´s Kiss", "High chance of Magic Bind, Strength Bind and Agility Bind (1 Enemy)") },
            { 0xB11, new Tuple<string, string>("Dekaja", "Remove stat buffs (All Enemies)") },
            { 0xB1F, new Tuple<string, string>("Tarunda", "Lower attack for 3 turns (1 Enemy)") },
            { 0xB20, new Tuple<string, string>("Rakunda", "Lower defense for 3 turns (1 Enemy)") },
            { 0xB21, new Tuple<string, string>("Sukunda", "Lower hit rate and evasion rate for 3 turns (1 Enemy)") },
            { 0xB22, new Tuple<string, string>("Debilitate", "Greatly lower attack, defense, hit and evasion for 3 turns (1 Enemy)") },
            { 0xB81, new Tuple<string, string>("Stun Circle", "Medium chance of Paralysis each turn, for 3 turns (All Enemies)") },
            { 0xB83, new Tuple<string, string>("Sleep Circle", "Medium chance of Sleep each turn, for 3 turns (All Enemies)") },
            { 0xB85, new Tuple<string, string>("Poison Circle", "Medium chance of Poison each turn, for 3 turns (All Enemies)") },
            { 0xB87, new Tuple<string, string>("Panic Circle", "Medium chance of Panic each turn, for 3 turns (All Enemies)") },
            { 0xB89, new Tuple<string, string>("Silence Circle", "Medium chance of Magic Bind each turn, for 3 turns (All Enemies)") },
            { 0xB8B, new Tuple<string, string>("Decay Circle", "Medium chance of Strength Bind each turn, for 3 turns (All Enemies)") },
            { 0xB8D, new Tuple<string, string>("Lethargy Circle", "Medium chance of Agility Bind each turn, for 3 turns (All Enemies)") },
        };

        #endregion SkillCardsDebuff

        #region SkillCardsMagic

        internal static Dictionary<int, Tuple<string, string>> SkillCardsMagic = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xA02, new Tuple<string, string>("Agi", "A light Fire attack (1 Enemy)") },
            { 0xA03, new Tuple<string, string>("Agilao", "A medium Fire attack (1 Enemy)") },
            { 0xA04, new Tuple<string, string>("Agidyne", "A heavy Fire attack (1 Enemy)") },
            { 0xA05, new Tuple<string, string>("Maragi", "A light Fire attack (All Enemies)") },
            { 0xA06, new Tuple<string, string>("Maragion", "A medium Fire attack (All Enemies)") },
            { 0xA07, new Tuple<string, string>("Maragidyne", "A heavy Fire attack (All Enemies)") },
            { 0xA08, new Tuple<string, string>("Fire Spray", "A light Fire attack that splashes to either side (1 Enemy)") },
            { 0xA09, new Tuple<string, string>("Ragnarok", "A severe Fire attack (1 Enemy)") },
            { 0xA0E, new Tuple<string, string>("Garu", "A light Wind attack (1 Enemy)") },
            { 0xA0F, new Tuple<string, string>("Garula", "A medium Wind attack (1 Enemy)") },
            { 0xA10, new Tuple<string, string>("Garudyne", "A heavy Wind attack (1 Enemy)") },
            { 0xA11, new Tuple<string, string>("Magaru", "A light Wind attack (All Enemies)") },
            { 0xA12, new Tuple<string, string>("Magarula", "A medium Wind attack (All Enemies)") },
            { 0xA13, new Tuple<string, string>("Magarudyne", "A heavy Wind attack (All Enemies)") },
            { 0xA14, new Tuple<string, string>("Cyclone", "2-3 light Wind attacks against random targets (1 Row)") },
            { 0xA15, new Tuple<string, string>("Panta Rhei", "A severe Wind attack that splashes to either side (1 Enemy)") },
            { 0xA1A, new Tuple<string, string>("Bufu", "A light Ice attack (1 Enemy)") },
            { 0xA1B, new Tuple<string, string>("Bufula", "A medium Ice attack (1 Enemy)") },
            { 0xA1C, new Tuple<string, string>("Bufudyne", "A heavy Ice attack (1 Enemy)") },
            { 0xA1D, new Tuple<string, string>("Mabufu", "A light Ice attack (All Enemies)") },
            { 0xA1E, new Tuple<string, string>("Mabufula", "A medium Ice attack (All Enemies)") },
            { 0xA1F, new Tuple<string, string>("Mabufudyne", "A heavy Ice attack (All Enemies)") },
            { 0xA20, new Tuple<string, string>("Icy Paradise", "2-4 medium Ice attacks against random targets (All Enemies)") },
            { 0xA21, new Tuple<string, string>("Niflheim", "A severe Ice attack (1 Enemy)") },
            { 0xA22, new Tuple<string, string>("Frozen Spear", "A light Ice attack that pierces to the back row (1 Enemy)") },
            { 0xA27, new Tuple<string, string>("Zio", "A light Elec attack (1 Enemy)") },
            { 0xA28, new Tuple<string, string>("Zionga", "A medium Elec attack (1 Enemy)") },
            { 0xA29, new Tuple<string, string>("Ziodyne", "A heavy Elec attack (1 Enemy)") },
            { 0xA2A, new Tuple<string, string>("Mazio", "A light Elec attack (All Enemies)") },
            { 0xA2B, new Tuple<string, string>("Mazionga", "A medium Elec attack (All Enemies)") },
            { 0xA2C, new Tuple<string, string>("Maziodyne", "A heavy Elec attack (All Enemies)") },
            { 0xA2E, new Tuple<string, string>("Thunder Reign", "A severe Elec attack (1 Enemy)") },
            { 0xA2F, new Tuple<string, string>("Keraunos", "6 heavy Elec attacks against random targets (All Enemies)") },
            { 0xA30, new Tuple<string, string>("Thunder Clap", "A light Elec attack (1 Row)") },
            { 0xA3D, new Tuple<string, string>("Hama", "Slight chance of instant kill (1 Enemy) [Light-based attack]") },
            { 0xA3E, new Tuple<string, string>("Hamaon", "Medium chance of instant kill (1 Enemy) [Light-based attack]") },
            { 0xA3F, new Tuple<string, string>("Mahama", "Slight chance of instant kill (All Enemies) [Light-based attack]") },
            { 0xA40, new Tuple<string, string>("Mahamaon", "Medium chance of instant kill (All Enemies) [Light-based attack]") },
            { 0xA43, new Tuple<string, string>("Samsara", "High chance of instant kill (All Enemies) [Light-based attack]") },
            { 0xA48, new Tuple<string, string>("Mudo", "Slight chance of instant kill (1 Enemy) [Darkness-based attack]") },
            { 0xA49, new Tuple<string, string>("Mudoon", "Medium chance of instant kill (1 Enemy) [Darkness-based attack]") },
            { 0xA4A, new Tuple<string, string>("Mamudo", "Slight chance of instant kill (All Enemies) [Darkness-based attack]") },
            { 0xA4B, new Tuple<string, string>("Mamudoon", "Medium chance of instant kill (All Enemies) [Darkness-based attack]") },
            { 0xA4E, new Tuple<string, string>("Die For Me!", "High chance of instant kill (All Enemies) [Darkness-based attack]") },
        };

        #endregion SkillCardsMagic

        #region SkillCardsNavi

        internal static Dictionary<int, Tuple<string, string>> SkillCardsNavi = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xB42, new Tuple<string, string>("Sutakora Foot", "[Navi] Flee from battle and teleport to floor entrance (Party)") },
            { 0xB45, new Tuple<string, string>("Snake Glare", "[Navi] High chance of preventing preemptive attacks") },
            { 0xB46, new Tuple<string, string>("Heroic Wind", "[Navi] Restore 5% of HP each turn (Party)") },
            { 0xB4B, new Tuple<string, string>("Harvest Prayer", "[Navi] Higher rate of rare materials. Rare chance to gather multiple times") },
            { 0xB4C, new Tuple<string, string>("Cornucopia", "[Navi] Higher rate of rare materials. Low chance to gather multiple items") },
            { 0xB4D, new Tuple<string, string>("Healing Tide", "[Navi] Moderate HP restore at end of turn, for 3 turns (Party)") },
            { 0xB4F, new Tuple<string, string>("Renewal Ray", "[Navi] Slight HP restore (Party) [Battle-only]") },
            { 0xB50, new Tuple<string, string>("Renewal Aura", "[Navi] Moderate HP restore (Party) [Battle-only]") },
            { 0xB51, new Tuple<string, string>("Healing Breeze", "[Navi] Slight HP restore at end of turn, for 3 turns (Party)") },
            { 0xB52, new Tuple<string, string>("Spotlight", "[Navi] Allow someone to act first for the turn (1 Ally)") },
            { 0xB53, new Tuple<string, string>("Yomi Return", "[Navi] Revive, with half HP restore (1 Ally)") },
            { 0xB55, new Tuple<string, string>("Orb of Resolve", "[Navi] Raise physical and elemental defense for 3 turns (Party)") },
            { 0xB56, new Tuple<string, string>("Orb of Power", "[Navi] Raise physical and elemental attack for 3 turns (Party)") },
            { 0xB57, new Tuple<string, string>("Orb of Haste", "[Navi] Raise hit rate and evasion rate for 3 turns (Party)") },
            { 0xB58, new Tuple<string, string>("Life Aid", "[Navi] 20% HP restore at the end of battle (Party)") },
            { 0xB59, new Tuple<string, string>("Victory Cry", "[Navi] Full HP restore at the end of battle (Party)") },
            { 0xB5A, new Tuple<string, string>("Safety Prayer", "[Navi] Gather somewhat safely from Power Spots") },
            { 0xB5B, new Tuple<string, string>("Calming Lull", "[Navi] Gather safely from Power Spots") },
            { 0xB5C, new Tuple<string, string>("Spotter", "[Navi] Slightly higher rate of rare materials from Power Spots") },
            { 0xB5D, new Tuple<string, string>("Magic Mallet", "[Navi] Higher rate of rare materials from Power Spots") },
            { 0xB5E, new Tuple<string, string>("Fanfare", "[Navi] Prevent preemptive attacks") },
            { 0xB5F, new Tuple<string, string>("Into the Void", "[Navi] Nullifies all damage for 1 turn") },
            { 0xB60, new Tuple<string, string>("Tidal Wave", "[Navi] Allow the party to act before any enemies for the turn (Party)") },
            { 0xB61, new Tuple<string, string>("Oracle", "[Navi] Give a random beneficial effect (Party)") },
            { 0xB69, new Tuple<string, string>("Prayer", "[Navi] Full HP restore and remove all Binds and ailments (Party)") },
            { 0xB6B, new Tuple<string, string>("Treasure Hunter", "[Navi] Reveal nearby treasure boxes") },
            { 0xB6C, new Tuple<string, string>("Fortune Hunter", "[Navi] Reveal treasure boxes in a wide area") },
            { 0xB6D, new Tuple<string, string>("Faerie´s Vigil", "[Navi] Reveal nearby shortcuts") },
            { 0xB6E, new Tuple<string, string>("Tengu´s Vigil", "[Navi] Reveal shortcuts in a wide area") },
            { 0xB6F, new Tuple<string, string>("Clairvoyance", "[Navi] Reveal nearby treasure boxes, FOEs and shortcuts") },
            { 0xB70, new Tuple<string, string>("Farsight", "[Navi] Reveal treasure boxes, FOEs, and shortcuts in a wide area") },
            { 0xB71, new Tuple<string, string>("Purifying Rain", "[Navi] Remove all Binds and ailments (Party)") },
            { 0xB73, new Tuple<string, string>("Zero Set", "[Navi] Skill costs are reduced to 0 for 1 turn (Party)") },
            { 0xB74, new Tuple<string, string>("Eternal Melody", "[Navi] Extend stat buffs by 3 additional turns (Party)") },
            { 0xB79, new Tuple<string, string>("Hunting Prayer", "[Navi] Slightly higher rate of material drops from enemies") },
            { 0xB7A, new Tuple<string, string>("Endless Bounty", "[Navi] Higher rate of material drops from enemies") },
            { 0xB7C, new Tuple<string, string>("Alleys of Light", "[Navi] Slight HP restore for each step taken in a labyrinth (Party)") },
            { 0xB7D, new Tuple<string, string>("Paths of Light", "[Navi] HP restore for each step taken in a labyrinth (Party)") },
            { 0xB7E, new Tuple<string, string>("Roads of Light", "[Navi] Slight SP restore for each step taken in a labyrinth (Party)") },
        };

        #endregion SkillCardsNavi

        #region SkillCardsRecovery

        internal static Dictionary<int, Tuple<string, string>> SkillCardsRecovery = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xAD1, new Tuple<string, string>("Dia", "Slight HP restore (1 Ally)") },
            { 0xAD2, new Tuple<string, string>("Diarama", "Moderate HP restore (1 Ally)") },
            { 0xAD3, new Tuple<string, string>("Diarahan", "Full HP restore (1 Ally)") },
            { 0xAD4, new Tuple<string, string>("Recovery", "Slight HP restore (1 Row)") },
            { 0xAD5, new Tuple<string, string>("Life Goblet", "Moderate HP restore (1 Row)") },
            { 0xAD6, new Tuple<string, string>("Media", "Slight HP restore (Party)") },
            { 0xAD7, new Tuple<string, string>("Mediarama", "Moderate HP restore (Party)") },
            { 0xAD8, new Tuple<string, string>("Mediarahan", "Full HP restore (Party)") },
            { 0xAD9, new Tuple<string, string>("Recarm", "Revive, with half HP restore (1 Ally)") },
            { 0xADA, new Tuple<string, string>("Samarecarm", "Revive, with full HP restore (1 Ally)") },
            { 0xADC, new Tuple<string, string>("Renewal", "Remove stat penalties (1 Row) [Battle-only]") },
            { 0xADD, new Tuple<string, string>("Patra", "Remove status ailments (1 Ally)") },
            { 0xADE, new Tuple<string, string>("Refresh", "Remove status ailments (1 Row) [Battle-only]") },
            { 0xADF, new Tuple<string, string>("Me Patra", "Remove status ailments (Party)") },
            { 0xAE0, new Tuple<string, string>("Mutudi", "Remove all Binds (1 Ally)") },
            { 0xAE1, new Tuple<string, string>("Becalm", "Remove all Binds (1 Row) [Battle-only]") },
            { 0xAE2, new Tuple<string, string>("Mamutudi", "Remove all Binds (Party)") },
            { 0xAE3, new Tuple<string, string>("Amrita", "Full HP restore. Remove all Binds and status ailments (1 Ally)") },
            { 0xB12, new Tuple<string, string>("Dekunda", "Remove stat penalties (Party)") },
            { 0xB2B, new Tuple<string, string>("Healing Harp", "Restore HP and remove ailments/Binds each turn for 3 turns (Party)") },
            { 0xB96, new Tuple<string, string>("Heal Memento", "Slight HP restore at end of turn, for 3 turns (User´s Row)") },
            { 0xB98, new Tuple<string, string>("Free Memento", "Remove all Binds at end of turn, for 3 turns (User´s Row)") },
            { 0xB9A, new Tuple<string, string>("Pure Memento", "Remove status ailments at end of each turn, for 3 turns (User´s Row)") },
            { 0xBA5, new Tuple<string, string>("Healing Step", "Full HP restore (Party) [Only usable during Blessings]") },
            { 0xC00, new Tuple<string, string>("Dia Guard", "Slight HP restore when the user´s row uses Defend") },
            { 0xC01, new Tuple<string, string>("Diarama Guard", "HP restore when the user´s row uses Defend") },
            { 0xC0D, new Tuple<string, string>("Absorb Fire", "Absorb damage from Fire attacks") },
            { 0xC0E, new Tuple<string, string>("Absorb Wind", "Absorb damage from Wind attacks") },
            { 0xC0F, new Tuple<string, string>("Absorb Ice", "Absorb damage from Ice attacks") },
            { 0xC10, new Tuple<string, string>("Absorb Elec", "Absorb damage from Elec attacks") },
            { 0xC21, new Tuple<string, string>("Circle Recovery", "Slight HP restore each turn (Party) [Requires Circles]") },
            { 0xC33, new Tuple<string, string>("Serene Journey", "Slight SP restore for each step taken in a labyrinth") },
        };

        #endregion SkillCardsRecovery

        #region SkillCardsStab

        internal static Dictionary<int, Tuple<string, string>> SkillCardsStab = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xA85, new Tuple<string, string>("End Shot", "A heavy Stab attack which takes longer to execute (1 Enemy)") },
            { 0xA86, new Tuple<string, string>("Double Shot", "2 light Stab attacks (1 Enemy)") },
            { 0xA89, new Tuple<string, string>("Silent Thrust", "A heavy Stab attack that pierces to the back row (1 Enemy)") },
            { 0xA8A, new Tuple<string, string>("Swift Strike", "3 quick, medium Stab attacks against random targets (1 Row)") },
            { 0xA8E, new Tuple<string, string>("Death Needle", "Heavy Stab attack. Low chance instant kill, higher with ailment (1 Enemy)") },
            { 0xA96, new Tuple<string, string>("Single Shot", "A light Stab attack that pierces to the back row (1 Enemy)") },
            { 0xA9A, new Tuple<string, string>("Assault Shot", "A medium Stab attack that pierces to the back row (1 Enemy)") },
            { 0xA9B, new Tuple<string, string>("Primal Force", "A heavy Stab attack, ignoring target´s resistances and buffs (1 Enemy)") },
            { 0xA9C, new Tuple<string, string>("Heavy Shot", "A heavy Stab attack that pierces to the back row (1 Enemy)") },
            { 0xA9D, new Tuple<string, string>("Broadshot", "A light Stab attack (1 Row)") },
            { 0xA9E, new Tuple<string, string>("Myriad Arrows", "6-8 light Stab attacks at random, with low accuracy (All Enemies)") },
            { 0xA9F, new Tuple<string, string>("Punishment", "A heavy Stab attack effective on Bind, but will remove Binds (1 Enemy)") },
            { 0xAB5, new Tuple<string, string>("Hailstorm", "A medium Stab attack (All Enemies)") },
            { 0xAB6, new Tuple<string, string>("Arrow Rain", "A light Stab attack (All Enemies)") },
            { 0xAB7, new Tuple<string, string>("Aeon Rain", "A medium Stab attack (All Enemies)") },
            { 0xC5A, new Tuple<string, string>("Demon´s Stab", "Slightly raise Stab attack strength") },
            { 0xC5D, new Tuple<string, string>("God´s Stab", "Raise Stab attack strength") },
        };

        #endregion SkillCardsStab

        #endregion SkillCards

        #region Weapons

        #region WeaponsAigis

        internal static Dictionary<int, Tuple<string, string>> WeaponsAigis = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x170, new Tuple<string, string>("3-Mount Albireo", "Power 27, Aigis") },
            { 0x171, new Tuple<string, string>("Six-Shot", "Power 44, Aigis") },
            { 0x172, new Tuple<string, string>("Phalanx B2", "Power 60, Aigis") },
            { 0x173, new Tuple<string, string>("5-Mount Medusa", "Power 75, Aigis") },
            { 0x174, new Tuple<string, string>("Carbon Railgun", "Power 92, Aigis") },
            { 0x175, new Tuple<string, string>("Axion IG", "Power 107, Aigis") },
            { 0x176, new Tuple<string, string>("Kiss of Athena", "Power 125, Aigis") },
            { 0x177, new Tuple<string, string>("Maxima Sniper", "Power 139, Aigis") },
            { 0x178, new Tuple<string, string>("Kuni-kuzushi", "Power 157, Aigis") },
            { 0x179, new Tuple<string, string>("Pandemonium", "Power 183, Aigis") },
            { 0x17A, new Tuple<string, string>("Grenade Launcher", "Power 37, Aigis") },
            { 0x17B, new Tuple<string, string>("Orgone Rifle", "Power 52, Aigis") },
            { 0x17C, new Tuple<string, string>("Firegun", "Power 68, Aigis") },
            { 0x17D, new Tuple<string, string>("Madfa", "Power 83, Aigis") },
            { 0x17E, new Tuple<string, string>("Orthrus ", "Power 99, Aigis") },
            { 0x17F, new Tuple<string, string>("Hell Grenade", "Power 115, Aigis") },
            { 0x180, new Tuple<string, string>("Handcannon", "Power 131, Aigis") },
            { 0x181, new Tuple<string, string>("Iron Saker", "Power 149, Aigis") },
            { 0x182, new Tuple<string, string>("Volt Gun", "Power 169, Aigis") },
            { 0x183, new Tuple<string, string>("Anti-Matter Gun", "Power 193, Aigis") },
            { 0x185, new Tuple<string, string>("Frost Cannon", "Power 87, Ice x3, Aigis") },
            { 0x186, new Tuple<string, string>("Metatronius", "Power 243, HP x4, Ag x4, Aigis") },
            { 0x187, new Tuple<string, string>("Lime Howitzer", "Power 1, [Ag] x10, Aigis") },
            { 0x188, new Tuple<string, string>("Heavy Howitzer", "Power 1, Exp x2, All stats halved, Aigis") },
            { 0x189, new Tuple<string, string>("Wide Bomber", "Power 152, Splash Effect x5, Aigis") },
            { 0x18A, new Tuple<string, string>("Paralysis Blaster", "Power 135, Paralysis x3, Aigis") },
        };

        #endregion WeaponsAigis

        #region WeaponsAkihiko

        internal static Dictionary<int, Tuple<string, string>> WeaponsAkihiko = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x290, new Tuple<string, string>("Brass Knuckles", "Power 31, Akihiko") },
            { 0x291, new Tuple<string, string>("Sonic Fist", "Power 41, Akihiko") },
            { 0x292, new Tuple<string, string>("Champion Gloves", "Power 56, Akihiko") },
            { 0x293, new Tuple<string, string>("Beast Fangs", "Power 72, Akihiko") },
            { 0x294, new Tuple<string, string>("Furious Fists", "Power 87, Akihiko") },
            { 0x295, new Tuple<string, string>("Blood Baghnakh", "Power 103, Akihiko") },
            { 0x296, new Tuple<string, string>("Pugilist´s Fists", "Power 119, Akihiko") },
            { 0x297, new Tuple<string, string>("Dragon Fangs", "Power 135, Akihiko") },
            { 0x298, new Tuple<string, string>("Bluster Knuckle", "Power 153, Akihiko") },
            { 0x299, new Tuple<string, string>("White Fangs", "Power 173, Akihiko") },
            { 0x29A, new Tuple<string, string>("Pegasus Fist", "Power 197, Akihiko") },
            { 0x29B, new Tuple<string, string>("Bladefist", "Power 48, Akihiko") },
            { 0x29C, new Tuple<string, string>("Jack´s Gloves", "Power 64, Akihiko") },
            { 0x29D, new Tuple<string, string>("Kaiser Knuckles", "Power 79, Akihiko") },
            { 0x29E, new Tuple<string, string>("Wicked Cestus", "Power 96, Akihiko") },
            { 0x29F, new Tuple<string, string>("Titanic Knuckles", "Power 111, Akihiko") },
            { 0x2A0, new Tuple<string, string>("Crusher Fist", "Power 129, Akihiko") },
            { 0x2A1, new Tuple<string, string>("Wings of Vanth", "Power 143, Akihiko") },
            { 0x2A2, new Tuple<string, string>("Golden Gloves", "Power 161, Akihiko") },
            { 0x2A3, new Tuple<string, string>("Double Ziggurat", "Power 187, Akihiko") },
            { 0x2A5, new Tuple<string, string>("Sabazios", "Power 180, [St] x3, Akihiko") },
            { 0x2A6, new Tuple<string, string>("Fimbulvetr", "Power 247, St x4, Ice x4, Akihiko") },
            { 0x2A7, new Tuple<string, string>("Goodnight Glove", "Power 1, Sleep x10, Akihiko") },
            { 0x2A8, new Tuple<string, string>("Heavy Gloves", "Power 1, Exp x2, All stats halved, Akihiko") },
            { 0x2A9, new Tuple<string, string>("Lucky Catpaw", "Power 114, Lu x3, Akihiko") },
            { 0x2AA, new Tuple<string, string>("Heavy Punch", "Power 75, [Ag] x3, Akihiko") },
        };

        #endregion WeaponsAkihiko

        #region WeaponsChie

        internal static Dictionary<int, Tuple<string, string>> WeaponsChie = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x80, new Tuple<string, string>("Leather Shoes", "Power 28, Chie") },
            { 0x81, new Tuple<string, string>("Spring Boots", "Power 45, Chie") },
            { 0x82, new Tuple<string, string>("Nanban Gusoku", "Power 61, Chie") },
            { 0x83, new Tuple<string, string>("Heavy Heels", "Power 76, Chie") },
            { 0x84, new Tuple<string, string>("Furinkazan", "Power 93, Chie") },
            { 0x85, new Tuple<string, string>("Sleipnir", "Power 108, Chie") },
            { 0x86, new Tuple<string, string>("Mjolnir Boots", "Power 126, Chie") },
            { 0x87, new Tuple<string, string>("Demon Boots", "Power 140, Chie") },
            { 0x88, new Tuple<string, string>("Gigant Fall", "Power 158, Chie") },
            { 0x89, new Tuple<string, string>("Judgement Boots", "Power 184, Chie") },
            { 0x8A, new Tuple<string, string>("Platform Sneaks", "Power 38, Chie") },
            { 0x8B, new Tuple<string, string>("Shield Boots", "Power 53, Chie") },
            { 0x8C, new Tuple<string, string>("Punk Shoes", "Power 69, Chie") },
            { 0x8D, new Tuple<string, string>("Bishamonten", "Power 84, Chie") },
            { 0x8E, new Tuple<string, string>("Bucking Broncos", "Power 100, Chie") },
            { 0x8F, new Tuple<string, string>("Four Beasts", "Power 116, Chie") },
            { 0x90, new Tuple<string, string>("Vidar's Boots", "Power 132, Chie") },
            { 0x91, new Tuple<string, string>("Kehaya", "Power 150, Chie") },
            { 0x92, new Tuple<string, string>("Peerless Heels", "Power 170, Chie") },
            { 0x93, new Tuple<string, string>("Stella Greaves", "Power 194, Chie") },
            { 0x95, new Tuple<string, string>("Genbu Greaves", "Power 103, En x3, Chie") },
            { 0x96, new Tuple<string, string>("Reigan Greaves", "Power 244, Lu x3, Hit Rate x5, Chie") },
            { 0x97, new Tuple<string, string>("Electro Shoes", "Power 1, Paralysis x10, Chie") },
            { 0x98, new Tuple<string, string>("Heavy Shoes ", "Power 1, Exp x2, All stats halved, Chie") },
            { 0x99, new Tuple<string, string>("Panic Boots", "Power 161, Panic x3, Chie") },
            { 0x9A, new Tuple<string, string>("Cobra Boots", "Power 73, Poison x3, Chie") },
        };

        #endregion WeaponsChie

        #region WeaponsKanji

        internal static Dictionary<int, Tuple<string, string>> WeaponsKanji = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xE0, new Tuple<string, string>("Folding Chair", "Power 34, Kanji") },
            { 0xE1, new Tuple<string, string>("Yasogami Desk", "Power 44, Kanji") },
            { 0xE2, new Tuple<string, string>("Round Shield", "Power 59, Kanji") },
            { 0xE3, new Tuple<string, string>("Photon Plate", "Power 75, Kanji") },
            { 0xE4, new Tuple<string, string>("Power Plate", "Power 90, Kanji") },
            { 0xE5, new Tuple<string, string>("Alloyed Plate", "Power 106, Kanji") },
            { 0xE6, new Tuple<string, string>("Scutum", "Power 122, Kanji") },
            { 0xE7, new Tuple<string, string>("Sol Breaker", "Power 138, Kanji") },
            { 0xE8, new Tuple<string, string>("Asturias", "Power 156, Kanji") },
            { 0xE9, new Tuple<string, string>("Wisdom Shield", "Power 178, Kanji") },
            { 0xEA, new Tuple<string, string>("Christ Mirror", "Power 200, Kanji") },
            { 0xEB, new Tuple<string, string>("Iron Plate", "Power 51, Kanji") },
            { 0xEC, new Tuple<string, string>("Steel Plate", "Power 67, Kanji") },
            { 0xED, new Tuple<string, string>("Hard Board", "Power 82, Kanji") },
            { 0xEE, new Tuple<string, string>("Bodyboard", "Power 99, Kanji") },
            { 0xEF, new Tuple<string, string>("Gorgon Plate", "Power 114, Kanji") },
            { 0xF0, new Tuple<string, string>("Oni-Gawara", "Power 132, Kanji") },
            { 0xF1, new Tuple<string, string>("Phalanx", "Power 146, Kanji") },
            { 0xF2, new Tuple<string, string>("Black Targe", "Power 164, Kanji") },
            { 0xF3, new Tuple<string, string>("Dullahan", "Power 190, Kanji") },
            { 0xF5, new Tuple<string, string>("Lantern Shield", "Power 109, [Ag] x3, Kanji") },
            { 0xF6, new Tuple<string, string>("Destiny Tablet", "Power 251, SP x3, Critical Rate x5, Kanji") },
            { 0xF7, new Tuple<string, string>("Straw Doll", "Power 1, Curse x10, Kanji") },
            { 0xF8, new Tuple<string, string>("Heavy Shield", "Power 1, Exp x2, All stats halved, Kanji") },
            { 0xF9, new Tuple<string, string>("Jet Shield", "Power 161, Ag x3, Kanji") },
            { 0xFA, new Tuple<string, string>("Drowsy Shield", "Power 79, Sleep x3, Kanji") },
        };

        #endregion WeaponsKanji

        #region WeaponsKen

        internal static Dictionary<int, Tuple<string, string>> WeaponsKen = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x260, new Tuple<string, string>("Cross Spear", "Power 29, Ken") },
            { 0x261, new Tuple<string, string>("Sexy Lance", "Power 46, Ken") },
            { 0x262, new Tuple<string, string>("Fuscina", "Power 62, Ken") },
            { 0x263, new Tuple<string, string>("Scorpion", "Power 77, Ken") },
            { 0x264, new Tuple<string, string>("Lance of Death", "Power 94, Ken") },
            { 0x265, new Tuple<string, string>("Ranseur", "Power 109, Ken") },
            { 0x266, new Tuple<string, string>("Ouka Jumonji", "Power 127, Ken") },
            { 0x267, new Tuple<string, string>("Ote-gine", "Power 141, Ken") },
            { 0x268, new Tuple<string, string>("Voulge", "Power 159, Ken") },
            { 0x269, new Tuple<string, string>("Romulus´ Spear", "Power 185, Ken") },
            { 0x26A, new Tuple<string, string>("Omega Spear", "Power 39, Ken") },
            { 0x26B, new Tuple<string, string>("Framea", "Power 54, Ken") },
            { 0x26C, new Tuple<string, string>("Javelin", "Power 70, Ken") },
            { 0x26D, new Tuple<string, string>("Do Sanga", "Power 85, Ken") },
            { 0x26E, new Tuple<string, string>("Rhomphair", "Power 101, Ken") },
            { 0x26F, new Tuple<string, string>("Ahlspiess", "Power 117, Ken") },
            { 0x270, new Tuple<string, string>("Oomiyari", "Power 133, Ken") },
            { 0x271, new Tuple<string, string>("Hayakaze", "Power 151, Ken") },
            { 0x272, new Tuple<string, string>("Winged Spear", "Power 171, Ken") },
            { 0x273, new Tuple<string, string>("Tonbo-kiri", "Power 195, Ken") },
            { 0x275, new Tuple<string, string>("Quinlong Yanyue", "Power 122, St x3, Ken") },
            { 0x276, new Tuple<string, string>("Gae Bolg", "Power 245, Poison x3, Piercing x5, Ken") },
            { 0x277, new Tuple<string, string>("Sasumata", "Power 1, [Ag] x10, Ken") },
            { 0x278, new Tuple<string, string>("Heavy Spear", "Power 1, Exp x2, All stats halved, Ken") },
            { 0x279, new Tuple<string, string>("Clover Pole", "Power 188, Lu x3, Ken") },
            { 0x27A, new Tuple<string, string>("Power Snare", "Power 105, [St] x3, Ken") },
        };

        #endregion WeaponsKen

        #region WeaponsMitsuru

        internal static Dictionary<int, Tuple<string, string>> WeaponsMitsuru = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x230, new Tuple<string, string>("Rapier", "Power 28, Mitsuru") },
            { 0x231, new Tuple<string, string>("Fleuret", "Power 38, Mitsuru") },
            { 0x232, new Tuple<string, string>("Flamberge", "Power 53, Mitsuru") },
            { 0x233, new Tuple<string, string>("Night Falcon", "Power 69, Mitsuru") },
            { 0x234, new Tuple<string, string>("Silver Rapier", "Power 84, Mitsuru") },
            { 0x235, new Tuple<string, string>("Serpent Sword", "Power 100, Mitsuru") },
            { 0x236, new Tuple<string, string>("Elegant Fleuret", "Power 116, Mitsuru") },
            { 0x237, new Tuple<string, string>("Misericorde", "Power 132, Mitsuru") },
            { 0x238, new Tuple<string, string>("Skrep", "Power 150, Mitsuru") },
            { 0x239, new Tuple<string, string>("Last Estoc", "Power 170, Mitsuru") },
            { 0x23A, new Tuple<string, string>("Damascus Rapier", "Power 194, Mitsuru") },
            { 0x23B, new Tuple<string, string>("Panzerstecher", "Power 45, Mitsuru") },
            { 0x23C, new Tuple<string, string>("Town Sword", "Power 61, Mitsuru") },
            { 0x23D, new Tuple<string, string>("Sting", "Power 76, Mitsuru") },
            { 0x23E, new Tuple<string, string>("Espada Ropera", "Power 93, Mitsuru") },
            { 0x23F, new Tuple<string, string>("Stiletto", "Power 108, Mitsuru") },
            { 0x240, new Tuple<string, string>("Moralltach", "Power 126, Mitsuru") },
            { 0x241, new Tuple<string, string>("Joyeuse", "Power 140, Mitsuru") },
            { 0x242, new Tuple<string, string>("Struggle", "Power 158, Mitsuru") },
            { 0x243, new Tuple<string, string>("Illuminati", "Power 184, Mitsuru") },
            { 0x245, new Tuple<string, string>("Bloody Estoc", "Power 88, Curse x3, Mitsuru") },
            { 0x246, new Tuple<string, string>("Kokuseki Senjin", "Power 244, En x4, Lu x4, Mitsuru") },
            { 0x247, new Tuple<string, string>("Small Sword", "Power 49, Mitsuru") },
            { 0x24B, new Tuple<string, string>("Electric Rod", "Power 1, Paralysis x10, Mitsuru") },
            { 0x24C, new Tuple<string, string>("Heavy Rapier", "Power 1, Exp x2, All stats halved, Mitsuru") },
            { 0x24D, new Tuple<string, string>("Thunder Rapier", "Power 161, Elec x3, Mitsuru") },
            { 0x24E, new Tuple<string, string>("Eternal Blade", "Power 136, Instakill x2, Mitsuru") },
        };

        #endregion WeaponsMitsuru

        #region WeaponsNaoto

        internal static Dictionary<int, Tuple<string, string>> WeaponsNaoto = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x110, new Tuple<string, string>("Nambu 2", "Power 27, Naoto") },
            { 0x111, new Tuple<string, string>("Culverin", "Power 44, Naoto") },
            { 0x112, new Tuple<string, string>("Raging Bull", "Power 60, Naoto") },
            { 0x113, new Tuple<string, string>("Crimson Dirge", "Power 75, Naoto") },
            { 0x114, new Tuple<string, string>("Chrome Heart", "Power 92, Naoto") },
            { 0x115, new Tuple<string, string>("Abyss", "Power 107, Naoto") },
            { 0x116, new Tuple<string, string>("Camel Red", "Power 125, Naoto") },
            { 0x117, new Tuple<string, string>("Machine Pistol", "Power 139, Naoto") },
            { 0x118, new Tuple<string, string>("Dragoon", "Power 157, Naoto") },
            { 0x119, new Tuple<string, string>("Judge of Hell", "Power 183, Naoto") },
            { 0x11A, new Tuple<string, string>("Peacemaker", "Power 37, Naoto") },
            { 0x11B, new Tuple<string, string>("Serpentinelock", "Power 52, Naoto") },
            { 0x11C, new Tuple<string, string>("Algernon", "Power 68, Naoto") },
            { 0x11D, new Tuple<string, string>(".44 Anaconda", "Power 83, Naoto") },
            { 0x11E, new Tuple<string, string>("Unlimited", "Power 99, Naoto") },
            { 0x11F, new Tuple<string, string>("Nalnari", "Power 115, Naoto") },
            { 0x120, new Tuple<string, string>("Wyrmhunter", "Power 131, Naoto") },
            { 0x121, new Tuple<string, string>("From Zero", "Power 149, Naoto") },
            { 0x122, new Tuple<string, string>("Abaddon", "Power 169, Naoto") },
            { 0x123, new Tuple<string, string>("Magatsu Kiba", "Power 193, Naoto") },
            { 0x125, new Tuple<string, string>("Magical Gun", "Power 144, Wind x3, Naoto") },
            { 0x126, new Tuple<string, string>("Megido Gun", "Power 233, SP x3, Fire x5, Naoto") },
            { 0x127, new Tuple<string, string>("Ray Gun", "Power 1, [St] x10, Naoto") },
            { 0x128, new Tuple<string, string>("Heavy Gun", "Power 1, Exp x2, All stats halved, Naoto") },
            { 0x129, new Tuple<string, string>("Shield Gun", "Power 86, En x3, Naoto") },
            { 0x12A, new Tuple<string, string>("Magic Sealer", "Power 103, [Ma] x3, Naoto") },
        };

        #endregion WeaponsNaoto

        #region WeaponsP3

        internal static Dictionary<int, Tuple<string, string>> WeaponsP3 = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1D0, new Tuple<string, string>("Short Sword", "Power 30, /P3/") },
            { 0x1D1, new Tuple<string, string>("Nagawakizashi", "Power 40, /P3/") },
            { 0x1D2, new Tuple<string, string>("Sabre", "Power 55, /P3/") },
            { 0x1D3, new Tuple<string, string>("Fudou Masamune", "Power 71, /P3/") },
            { 0x1D4, new Tuple<string, string>("Silver Saber", "Power 86, /P3/") },
            { 0x1D5, new Tuple<string, string>("Tizona", "Power 102, /P3/") },
            { 0x1D6, new Tuple<string, string>("Kaneshige", "Power 118, /P3/") },
            { 0x1D7, new Tuple<string, string>("Sin Blade", "Power 134, /P3/") },
            { 0x1D8, new Tuple<string, string>("Qi Xing Dao", "Power 152, /P3/") },
            { 0x1D9, new Tuple<string, string>("Macabuin", "Power 172, /P3/") },
            { 0x1DA, new Tuple<string, string>("Gimlet", "Power 186, /P3/") },
            { 0x1DB, new Tuple<string, string>("Short Blade", "Power 35, En x1, /P3/") },
            { 0x1DC, new Tuple<string, string>("Excalibur", "Power 196, *called Aroundight in-game*, /P3/") },
            { 0x1DD, new Tuple<string, string>("Falcata", "Power 47, /P3/") },
            { 0x1DE, new Tuple<string, string>("Shamshir", "Power 63, /P3/") },
            { 0x1DF, new Tuple<string, string>("Cutlass", "Power 78, /P3/") },
            { 0x1E0, new Tuple<string, string>("Schiavona", "Power 95, /P3/") },
            { 0x1E1, new Tuple<string, string>("Aroundight", "Power 110, /P3/") },
            { 0x1E2, new Tuple<string, string>("Katzbalger", "Power 128, /P3/") },
            { 0x1E3, new Tuple<string, string>("Kogarasumaru", "Power 142, /P3/") },
            { 0x1E4, new Tuple<string, string>("Amakuni", "Power 160, /P3/") },
            { 0x1E6, new Tuple<string, string>("Shishioh", "Power 73, [Ma] x3, /P3/") },
            { 0x1E7, new Tuple<string, string>("Deus Xiphos", "Power 246, HP x2, SP x2, St x2, Ma x2, /P3/") },
            { 0x1E8, new Tuple<string, string>("Spiral Sword", "Power 1, Panic x10, /P3/") },
            { 0x1E9, new Tuple<string, string>("Slumber Sword", "Power 1, Sleep x10, /P3/") },
            { 0x1EA, new Tuple<string, string>("Heavy Sword", "Power 1, Exp x2, All stats halved, /P3/") },
            { 0x1EB, new Tuple<string, string>("Gravity Sword", "Power 66, [Ag] x3, /P3/") },
            { 0x1EC, new Tuple<string, string>("Poison Sword", "Power 82, Poison x2, /P3/") },
            { 0x1ED, new Tuple<string, string>("Curse Blade", "Power 180, Curse x3, /P3/") },
        };

        #endregion WeaponsP3

        #region WeaponsP4Junpei

        internal static Dictionary<int, Tuple<string, string>> WeaponsP4Junpei = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1, new Tuple<string, string>("Golf Club", "Power 31, /P4//Junpei") },
            { 0x2, new Tuple<string, string>("Imitation Katana", "Power 31, /P4//Junpei") },
            { 0x3, new Tuple<string, string>("Iai Katana", "Power 41, /P4//Junpei") },
            { 0x4, new Tuple<string, string>("Bastard Sword", "Power 56, /P4//Junpei") },
            { 0x5, new Tuple<string, string>("Two-Handed Sword", "Power 72, /P4//Junpei") },
            { 0x6, new Tuple<string, string>("Dark Basher", "Power 87, /P4//Junpei") },
            { 0x7, new Tuple<string, string>("Type-98 Gunto", "Power 103, /P4//Junpei") },
            { 0x8, new Tuple<string, string>("Tsubaki-maru", "Power 119, /P4//Junpei") },
            { 0x9, new Tuple<string, string>("Great Sword", "Power 135, /P4//Junpei") },
            { 0xA, new Tuple<string, string>("Deathbringer ", "Power 153, /P4//Junpei") },
            { 0xB, new Tuple<string, string>("Caladbolg", "Power 173, /P4//Junpei") },
            { 0xD, new Tuple<string, string>("Long Sword", "Power 36, En x1, /P4//Junpei") },
            { 0xE, new Tuple<string, string>("Soul Crusher", "Power 197, /P4//Junpei") },
            { 0xF, new Tuple<string, string>("Kishido Blade", "Power 48, /P4//Junpei") },
            { 0x10, new Tuple<string, string>("Zweihander", "Power 64, /P4//Junpei") },
            { 0x11, new Tuple<string, string>("Gothic Sword", "Power 79, /P4//Junpei") },
            { 0x12, new Tuple<string, string>("Zanbatou", "Power 96, /P4//Junpei") },
            { 0x13, new Tuple<string, string>("Edge", "Power 111, /P4//Junpei") },
            { 0x14, new Tuple<string, string>("Mikazuki Munechika", "Power 129, /P4//Junpei") },
            { 0x15, new Tuple<string, string>("Midare Hamon", "Power 143, /P4//Junpei") },
            { 0x16, new Tuple<string, string>("Kakitsubata", "Power 161, /P4//Junpei") },
            { 0x17, new Tuple<string, string>("Tobi-botaru", "Power 187, /P4//Junpei") },
            { 0x18, new Tuple<string, string>("Kusanagi no Tsurugi", "Power 91, Sleep x3, /P4//Junpei") },
            { 0x19, new Tuple<string, string>("Usumidori", "Power 247, HP x2, SP x2, St x2, Ma x2, /P4/") },
            { 0x1A, new Tuple<string, string>("Masakado's Katana", "Power 247, St x4, Critical Rate x4, Junpei") },
            { 0x1B, new Tuple<string, string>("Kopis", "Power 52, Lu x3, /P4//Junpei") },
            { 0x1C, new Tuple<string, string>("Onusa Sword", "Power 1, [Ma] x10, /P4//Junpei") },
            { 0x1D, new Tuple<string, string>("Harisen", "Power 1, [St] x10, /P4//Junpei") },
            { 0x1E, new Tuple<string, string>("Heavy Longsword", "Power 1, Exp x2, All stats halved, /P4//Junpei") },
            { 0x1F, new Tuple<string, string>("Paralysis Blade", "Power 67, Paralysis x3, /P4//Junpei") },
            { 0x20, new Tuple<string, string>("Venom Sword", "Power 83, Poison x3, /P4//Junpei") },
            { 0x21, new Tuple<string, string>("Setsugetsuka", "Power 180, Curse x3, /P4//Junpei") },
        };

        #endregion WeaponsP4Junpei

        #region WeaponsShinjiro

        internal static Dictionary<int, Tuple<string, string>> WeaponsShinjiro = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x2C0, new Tuple<string, string>("Sledgehammer", "Power 34, Shinjiro") },
            { 0x2C1, new Tuple<string, string>("Ogre Hammer", "Power 51, Shinjiro") },
            { 0x2C2, new Tuple<string, string>("Night Stalker", "Power 67, Shinjiro") },
            { 0x2C3, new Tuple<string, string>("Bus Stop Sign", "Power 82, Shinjiro") },
            { 0x2C4, new Tuple<string, string>("Megaton Rod", "Power 99, Shinjiro") },
            { 0x2C5, new Tuple<string, string>("Heavy Mace", "Power 114, Shinjiro") },
            { 0x2C6, new Tuple<string, string>("Charun´s Hammer", "Power 132, Shinjiro") },
            { 0x2C7, new Tuple<string, string>("Gaea´s Grace", "Power 146, Shinjiro") },
            { 0x2C8, new Tuple<string, string>("Eliminator", "Power 164, Shinjiro") },
            { 0x2C9, new Tuple<string, string>("War Pick", "Power 190, Shinjiro") },
            { 0x2CA, new Tuple<string, string>("Light Mace", "Power 44, Shinjiro") },
            { 0x2CB, new Tuple<string, string>("Nirili", "Power 59, Shinjiro") },
            { 0x2CC, new Tuple<string, string>("Crushing Club", "Power 75, Shinjiro") },
            { 0x2CD, new Tuple<string, string>("Spiked Club", "Power 90, Shinjiro") },
            { 0x2CE, new Tuple<string, string>("Hex Club", "Power 106, Shinjiro") },
            { 0x2CF, new Tuple<string, string>("Kanasaibou", "Power 122, Shinjiro") },
            { 0x2D0, new Tuple<string, string>("Morgenstern", "Power 138, Shinjiro") },
            { 0x2D1, new Tuple<string, string>("Jadagna", "Power 156, Shinjiro") },
            { 0x2D2, new Tuple<string, string>("Yagrush", "Power 176, Shinjiro") },
            { 0x2D3, new Tuple<string, string>("Gennou", "Power 200, Shinjiro") },
            { 0x2D5, new Tuple<string, string>("Oni ni Kanabou", "Power 78, Splash Effect x3, Shinjiro") },
            { 0x2D6, new Tuple<string, string>("Corpse Rod", "Power 250, Hit Rate x4, Panic x4, Shinjiro") },
            { 0x2D7, new Tuple<string, string>("Stun Hammer", "Power 1, Paralysis x10, Shinjiro") },
            { 0x2D8, new Tuple<string, string>("Heavy Hammer", "Power 1, Exp x2, All stats halved, Shinjiro") },
            { 0x2D9, new Tuple<string, string>("Aero Hammer", "Power 135, Wind x3, Shinjiro") },
            { 0x2DA, new Tuple<string, string>("Hammer of Repose", "Power 185, Sleep x3, Shinjiro") },
        };

        #endregion WeaponsShinjiro

        #region WeaponsTeddie

        internal static Dictionary<int, Tuple<string, string>> WeaponsTeddie = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x140, new Tuple<string, string>("Spikey Punch", "Power 29, Teddie") },
            { 0x141, new Tuple<string, string>("Mewling Claw", "Power 39, Teddie") },
            { 0x142, new Tuple<string, string>("Bear Claw", "Power 54, Teddie") },
            { 0x143, new Tuple<string, string>("Tiger Claw", "Power 70, Teddie") },
            { 0x144, new Tuple<string, string>("Breaker", "Power 85, Teddie") },
            { 0x145, new Tuple<string, string>("Gehenna Claw", "Power 101, Teddie") },
            { 0x146, new Tuple<string, string>("Seiryu Claw", "Power 117, Teddie") },
            { 0x147, new Tuple<string, string>("Sonic Claw", "Power 133, Teddie") },
            { 0x148, new Tuple<string, string>("Platinum Claw", "Power 151, Teddie") },
            { 0x149, new Tuple<string, string>("The Ripper", "Power 171, Teddie") },
            { 0x14A, new Tuple<string, string>("Shichisei Jakotsu", "Power 195, Teddie") },
            { 0x14B, new Tuple<string, string>("Crab Claw", "Power 46, Teddie") },
            { 0x14C, new Tuple<string, string>("Fuuma Bundou", "Power 62, Teddie") },
            { 0x14D, new Tuple<string, string>("Iron Claw", "Power 77, Teddie") },
            { 0x14E, new Tuple<string, string>("Hell Claw", "Power 94, Teddie") },
            { 0x14F, new Tuple<string, string>("Kazegiri", "Power 109, Teddie") },
            { 0x150, new Tuple<string, string>("Dangerous Claw", "Power 127, Teddie") },
            { 0x151, new Tuple<string, string>("Dragon Claw", "Power 141, Teddie") },
            { 0x152, new Tuple<string, string>("Black Claw", "Power 159, Teddie") },
            { 0x153, new Tuple<string, string>("Shadow Claw", "Power 185, Teddie") },
            { 0x155, new Tuple<string, string>("Byakko Reisou", "Power 176, Ag x3, Teddie") },
            { 0x156, new Tuple<string, string>("Claw of Revenge", "Power 245, SP x3, Curse x5, Teddie") },
            { 0x157, new Tuple<string, string>("Steeltrap Claw", "Power 1, [Ag] x10, Teddie") },
            { 0x158, new Tuple<string, string>("Heavy Claw", "Power 1, Exp x2, All stats halved, Teddie") },
            { 0x159, new Tuple<string, string>("Thunder Claw", "Power 130, Elec x3, Teddie") },
            { 0x15A, new Tuple<string, string>("Power Eater", "Power 105, [St] x3, Teddie") },
        };

        #endregion WeaponsTeddie

        #region WeaponsYosukeKoromaru

        internal static Dictionary<int, Tuple<string, string>> WeaponsYosukeKoromaru = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x40, new Tuple<string, string>("Monkey Wrench", "Power 29, Yosuke/Koromaru") },
            { 0x41, new Tuple<string, string>("Sylph Blade", "Power 29, Yosuke/Koromaru") },
            { 0x42, new Tuple<string, string>("Blitz Kunai", "Power 46, Yosuke/Koromaru") },
            { 0x43, new Tuple<string, string>("Kaiken", "Power 62, Yosuke/Koromaru") },
            { 0x44, new Tuple<string, string>("Yashima", "Power 77, Yosuke/Koromaru") },
            { 0x45, new Tuple<string, string>("Fearful Kunai", "Power 94, Yosuke/Koromaru") },
            { 0x46, new Tuple<string, string>("Jade Dagger", "Power 109, Yosuke/Koromaru") },
            { 0x47, new Tuple<string, string>("Basho", "Power 127, Yosuke/Koromaru") },
            { 0x48, new Tuple<string, string>("Militia Dagger", "Power 141, Yosuke/Koromaru") },
            { 0x49, new Tuple<string, string>("Rappa", "Power 159, Yosuke/Koromaru") },
            { 0x4A, new Tuple<string, string>("Kashin Koji", "Power 185, Yosuke/Koromaru") },
            { 0x4B, new Tuple<string, string>("Hunting Nata", "Power 39, Yosuke/Koromaru") },
            { 0x4C, new Tuple<string, string>("Machete", "Power 54, Yosuke/Koromaru") },
            { 0x4D, new Tuple<string, string>("Karasu-maru", "Power 70, Yosuke/Koromaru") },
            { 0x4E, new Tuple<string, string>("En-Giri", "Power 85, Yosuke/Koromaru") },
            { 0x4F, new Tuple<string, string>("Hoori", "Power 101, Yosuke/Koromaru") },
            { 0x50, new Tuple<string, string>("Raven Claw", "Power 117, Yosuke/Koromaru") },
            { 0x51, new Tuple<string, string>("Pesh Kabz", "Power 133, Yosuke/Koromaru") },
            { 0x52, new Tuple<string, string>("Shadowrend", "Power 151, Yosuke/Koromaru") },
            { 0x53, new Tuple<string, string>("Athame", "Power 171, Yosuke/Koromaru") },
            { 0x54, new Tuple<string, string>("Grand Slasher", "Power 195, Yosuke/Koromaru") },
            { 0x55, new Tuple<string, string>("Pixie Knife", "Power 72, Panic x3, Yosuke/Koromaru") },
            { 0x56, new Tuple<string, string>("Nandaka", "Power 245, St x4, Ma x4, Yosuke") },
            { 0x57, new Tuple<string, string>("Vajra", "Power 245, Lu x4, [Ma] x4, Koromaru") },
            { 0x58, new Tuple<string, string>("Jitte", "Power 1, [St] x10, Yosuke/Koromaru") },
            { 0x59, new Tuple<string, string>("Sleep Dagger", "Power 1, Sleep x10, Yosuke/Koromaru") },
            { 0x5A, new Tuple<string, string>("Heavy Knife", "Power 1, Exp x2, All stats halved, Yosuke/Koromaru") },
            { 0x5B, new Tuple<string, string>("Poison Knife", "Power 112, Poison x3, Yosuke/Koromaru") },
            { 0x5C, new Tuple<string, string>("Paralysis Knife", "Power 137, Paralysis x3, Yosuke/Koromaru") },
        };

        #endregion WeaponsYosukeKoromaru

        #region WeaponsYukari

        internal static Dictionary<int, Tuple<string, string>> WeaponsYukari = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x200, new Tuple<string, string>("Practice Bow", "Power 25, Yukari") },
            { 0x201, new Tuple<string, string>("Short Bow", "Power 35, Yukari") },
            { 0x202, new Tuple<string, string>("Shigetou-yumi", "Power 50, Yukari") },
            { 0x203, new Tuple<string, string>("Uta", "Power 66, Yukari") },
            { 0x204, new Tuple<string, string>("Kamatha", "Power 81, Yukari") },
            { 0x205, new Tuple<string, string>("Higo-yumi", "Power 97, Yukari") },
            { 0x206, new Tuple<string, string>("Composite Bow", "Power 113, Yukari") },
            { 0x207, new Tuple<string, string>("Great Bow", "Power 129, Yukari") },
            { 0x208, new Tuple<string, string>("Hero's Bow", "Power 147, Yukari") },
            { 0x209, new Tuple<string, string>("Lightning Bow", "Power 167, Yukari") },
            { 0x20A, new Tuple<string, string>("Yoichi's Bow", "Power 191, Yukari") },
            { 0x20B, new Tuple<string, string>("Quintet Bow", "Power 42, Yukari") },
            { 0x20C, new Tuple<string, string>("Long Bow", "Power 58, Yukari") },
            { 0x20D, new Tuple<string, string>("Self Bow", "Power 73, Yukari") },
            { 0x20E, new Tuple<string, string>("Hades Bow", "Power 90, Yukari") },
            { 0x20F, new Tuple<string, string>("Sylvan Bow", "Power 105, Yukari") },
            { 0x210, new Tuple<string, string>("Hunter Bow", "Power 123, Yukari") },
            { 0x211, new Tuple<string, string>("Ill Wind ", "Power 137, Yukari") },
            { 0x212, new Tuple<string, string>("Heaven Bow", "Power 155, Yukari") },
            { 0x213, new Tuple<string, string>("Gendawa", "Power 181, Yukari") },
            { 0x215, new Tuple<string, string>("Ichibal", "Power 143, Ice x3, Yukari") },
            { 0x216, new Tuple<string, string>("Quintessence Bow", "Power 241, SP x4, En x4, Yukari") },
            { 0x217, new Tuple<string, string>("Bat Bow", "Power 1, Panic x10, Yukari") },
            { 0x218, new Tuple<string, string>("Heavy Bow", "Power 1, Exp x2, All stats halved, Yukari") },
            { 0x219, new Tuple<string, string>("Spiral Bow", "Power 107, Piercing x3, Yukari") },
            { 0x21A, new Tuple<string, string>("Silence Bow", "Power 69, [Ma] x3, Yukari") },
        };

        #endregion WeaponsYukari

        #region WeaponsYukiko

        internal static Dictionary<int, Tuple<string, string>> WeaponsYukiko = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0xB0, new Tuple<string, string>("Kyo Sensu", "Power 24, Yukiko") },
            { 0xB1, new Tuple<string, string>("Noh Fan", "Power 41, Yukiko") },
            { 0xB2, new Tuple<string, string>("Silk Fan", "Power 57, Yukiko") },
            { 0xB3, new Tuple<string, string>("Hanachirusato", "Power 72, Yukiko") },
            { 0xB4, new Tuple<string, string>("Inversion Fan", "Power 89, Yukiko") },
            { 0xB5, new Tuple<string, string>("Fickle Madam", "Power 104, Yukiko") },
            { 0xB6, new Tuple<string, string>("Courtesia", "Power 122, Yukiko") },
            { 0xB7, new Tuple<string, string>("Suzumushi", "Power 136, Yukiko") },
            { 0xB8, new Tuple<string, string>("Secret Fan", "Power 154, Yukiko") },
            { 0xB9, new Tuple<string, string>("Pieta", "Power 180, Yukiko") },
            { 0xBA, new Tuple<string, string>("Santa Fan", "Power 34, Yukiko") },
            { 0xBB, new Tuple<string, string>("Festival Fan", "Power 49, Yukiko") },
            { 0xBC, new Tuple<string, string>("Tessen", "Power 65, Yukiko") },
            { 0xBD, new Tuple<string, string>("Masquerade", "Power 80, Yukiko") },
            { 0xBE, new Tuple<string, string>("Mogari-Bue", "Power 96, Yukiko") },
            { 0xBF, new Tuple<string, string>("Ganar", "Power 112, Yukiko") },
            { 0xC0, new Tuple<string, string>("Adoracion", "Power 128, Yukiko") },
            { 0xC1, new Tuple<string, string>("Hototogisu", "Power 146, Yukiko") },
            { 0xC2, new Tuple<string, string>("Kacho Fugetsu", "Power 166, Yukiko") },
            { 0xC3, new Tuple<string, string>("Yume no Ukihashi", "Power 190, Yukiko") },
            { 0xC5, new Tuple<string, string>("Suzaku Feather", "Power 132, Fire x3, Yukiko") },
            { 0xC6, new Tuple<string, string>("Peacock Tail", "Power 240, SP x3, [Ma] x5, Yukiko") },
            { 0xC7, new Tuple<string, string>("Jusen", "Power 1, Curse x10, Yukiko") },
            { 0xC8, new Tuple<string, string>("Heavy Sensu", "Power 1, Exp x2, All stats halved, Yukiko") },
            { 0xC9, new Tuple<string, string>("Lucky Rake", "Power 83, Lu x3, Yukiko") },
            { 0xCA, new Tuple<string, string>("Moth Fan", "Power 173, Poison x3, Yukiko") },
        };

        #endregion WeaponsYukiko

        #region WeaponsZenRei

        internal static Dictionary<int, Tuple<string, string>> WeaponsZenRei = new Dictionary<int, Tuple<string, string>>()
        {
            { 0, new Tuple<string, string>("EMPTY", "") },

            { 0x1A0, new Tuple<string, string>("Normal Bullet", "Power 41, " + Properties.Resources.ZenRei) },
            { 0x1A1, new Tuple<string, string>("Rubber Bullet", "Power 58, " + Properties.Resources.ZenRei) },
            { 0x1A2, new Tuple<string, string>("Iron Arrow", "Power 74, " + Properties.Resources.ZenRei) },
            { 0x1A3, new Tuple<string, string>("Softpoint", "Power 89, " + Properties.Resources.ZenRei) },
            { 0x1A4, new Tuple<string, string>("Copper Arrow", "Power 106, " + Properties.Resources.ZenRei) },
            { 0x1A5, new Tuple<string, string>("Frangible Bullet", "Power 121, " + Properties.Resources.ZenRei) },
            { 0x1A6, new Tuple<string, string>("Silver Arrow", "Power 139, " + Properties.Resources.ZenRei) },
            { 0x1A7, new Tuple<string, string>("Needle Shot", "Power 153, " + Properties.Resources.ZenRei) },
            { 0x1A8, new Tuple<string, string>("King´s Bullet", "Power 171, " + Properties.Resources.ZenRei) },
            { 0x1A9, new Tuple<string, string>("Golden Bullet", "Power 197, " + Properties.Resources.ZenRei) },
            { 0x1AA, new Tuple<string, string>("Splash Shot", "Power 66, Splash Effect x4, " + Properties.Resources.ZenRei) },
            { 0x1AB, new Tuple<string, string>("Theodore #3", "Power 74, Panic x2, " + Properties.Resources.ZenRei) },
            { 0x1AC, new Tuple<string, string>("Flame Shot", "Power 51, Fire x1, " + Properties.Resources.ZenRei) },
            { 0x1AD, new Tuple<string, string>("Wadcutter", "Power 82, " + Properties.Resources.ZenRei) },
            { 0x1AE, new Tuple<string, string>("Piercer", "Power 97, Piercing x2, " + Properties.Resources.ZenRei) },
            { 0x1AF, new Tuple<string, string>("Flechette", "Power 113, " + Properties.Resources.ZenRei) },
            { 0x1B0, new Tuple<string, string>("Exploder", "Power 129, " + Properties.Resources.ZenRei) },
            { 0x1B1, new Tuple<string, string>("Hollow Point", "Power 145, " + Properties.Resources.ZenRei) },
            { 0x1B2, new Tuple<string, string>("Silver Bullet", "Power 163, " + Properties.Resources.ZenRei) },
            { 0x1B3, new Tuple<string, string>("Gold Arrow", "Power 183, " + Properties.Resources.ZenRei) },
            { 0x1B4, new Tuple<string, string>("Infinite Shot", "Power 207, " + Properties.Resources.ZenRei) },
            { 0x1B5, new Tuple<string, string>("Clock Hand", "Power 190, Instakill x4, " + Properties.Resources.ZenRei) },
            { 0x1B6, new Tuple<string, string>("Pink Shot", "Power 108, Sleep x3, " + Properties.Resources.ZenRei) },
            { 0x1B7, new Tuple<string, string>("Pashupata", "Power 257, Ma x3, Fire x5, " + Properties.Resources.ZenRei) },
            { 0x1B8, new Tuple<string, string>("Gumball", "Power 1, [Ma] x10, " + Properties.Resources.ZenRei) },
            { 0x1B9, new Tuple<string, string>("Theodore #66", "Power 176, Piercing x3, HP x1, SP x1, Hit Rate x3, " + Properties.Resources.ZenRei) },
            { 0x1BA, new Tuple<string, string>("Heavy Bullet", "Power 1, Exp x2, All stats halved, " + Properties.Resources.ZenRei) },
            { 0x1BB, new Tuple<string, string>("Critical Bullet", "Power 190, Critical Rate x2, " + Properties.Resources.ZenRei) },
            { 0x1BC, new Tuple<string, string>("Speed Eater", "Power 148, [Ag] x3, " + Properties.Resources.ZenRei) },
            { 0x1BD, new Tuple<string, string>("Poison Bullet", "Power 149, Poison x3, " + Properties.Resources.ZenRei) },
        };

        #endregion WeaponsZenRei

        #endregion Weapons
    }
}
