﻿using System.ComponentModel;

namespace Persona_Q_Save_Editor.Models
{
    internal class KeyItem : INotifyPropertyChanged
    {
        private int bit;
        private string description, name;

        public int Bit
        {
            get { return bit; }

            set
            {
                if (bit == value) return;
                bit = value;
                NotifyPropertyChanged("Bit");
            }
        }

        public string Description { get { return description; } }

        public string Name { get { return name; } }

        internal KeyItem(int bit, string name, string description, bool mainCharIsP4)
        {
            this.bit = bit;
            this.name = name;
            this.description = description.Replace("/P34/", mainCharIsP4 ? "/P4/" : "/P3/").Replace("/P4/", Lists.Characters[0]).Replace("/P3/", Lists.Characters[1]);
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
