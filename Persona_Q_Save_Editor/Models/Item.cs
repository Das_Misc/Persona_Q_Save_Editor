﻿using System.ComponentModel;

namespace Persona_Q_Save_Editor.Models
{
    public class Item : INotifyPropertyChanged
    {
        private static Item[] list;
        private string description, name;
        private int id;

        public static Item[] List
        {
            get { return list; }
            set
            {
                list = value;
                foreach (var item in list)
                    item.UpdateData();
            }
        }

        public string Description { get { return description; } }

        public int ID
        {
            get { return id; }
            set
            {
                if (id == value) return;
                id = value;
                UpdateData();
            }
        }

        public string Name { get { return name; } }

        internal Item(int id, string name, string description)
        {
            this.id = id;
            this.name = name;
            this.description = description.Replace("/P4/", Lists.Characters[0]).Replace("/P3/", Lists.Characters[1]);
        }

        private void UpdateData()
        {
            var itemData = Lists.GetItemData(id);
            name = itemData.Item1;
            description = itemData.Item2.Replace("/P4/", Lists.Characters[0]).Replace("/P3/", Lists.Characters[1]);

            NotifyPropertyChanged("ID");
            NotifyPropertyChanged("Name");
            NotifyPropertyChanged("Description");
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
