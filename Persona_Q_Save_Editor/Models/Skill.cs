﻿using System.ComponentModel;

namespace Persona_Q_Save_Editor.Models
{
    public class Skill : INotifyPropertyChanged
    {
        private static Skill[] list;
        private string description, name;
        private short id;

        public static Skill[] List
        {
            get { return list; }
            set
            {
                list = value;
                foreach (var skill in list)
                    skill.UpdateData();
            }
        }

        public string Description { get { return description; } }

        public short ID
        {
            get { return id; }
            set
            {
                id = value;
                UpdateData();
            }
        }

        public string Name { get { return name; } }

        private void UpdateData()
        {
            var skillData = Lists.GetPersonaData(id);
            name = skillData != null ? skillData.Item1 : Properties.Resources.Unknown;
            description = skillData != null ? skillData.Item2 : Properties.Resources.Unknown;

            NotifyPropertyChanged("ID");
            NotifyPropertyChanged("Name");
            NotifyPropertyChanged("Description");
        }

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
